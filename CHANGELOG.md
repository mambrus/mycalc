# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- v0.10.x   System/program interfacing

### Added

- v0.10.1   Branch-off

### Changed

### Removed

## [0.9.6] - 2024-02-18

### Added

- Conditionals
  - A new 2-stage AST design enables both holding one condition and two cases,
    one for `true` the other for `false`
  - New AST-types: `CONDITIONAL`, `BRANCH`
- Loops
  - Utilizing same base-design as conditionals, except
     - Top-half iterates botton-half while condition is `true`
     - Condition still passed-on to botton-half but not currently used.
  - New AST-types: `WHILE-DO`, `BRANCH` (re-fined)
- Next-operator `;`
  - As `mycalc` is not a imperative but a strictly functional language, the
    concept of a `statement` is limited to declarations.
  - The next-operator works in a similar way however, allowing more than one
    expressions to be evaluated in a sequence from left to right.
  - For each evaluated expression the result-register is updated the
    same way as it would be if evaluated due to EOL. This construct gives
    interesting benefits over imperative execution.

### Changed

- Changelog to reflect this projects
- Expression evaluation triggered by EOL permitted to end with, what in other
  languages would be called "statement separator" (i.e. `;`).
  - This gives the illusion of `mycalc` being imperative. Yet it isn't, but it
    helps users used to Algol-derived languages such as `C` not to trigger
    syntax-errors as there's no syntax really to consider for signalling
    start-of-evaluation.

### Removed

## [0.8.12] - 2024-02-11

### Added

- Bitwise operations
  - A concession to the original idea and goal of `mycalc`, but very useful.
  - `mycalc` is still 100% `long double` (i.e. `mycalc Float`) based.
    Bitwise operations are categorized by the token-parser while AST buil-up
    and as such, enabling `l` & `r` for each bitwise-AST to treat both sides
    as `unsigned long long`. The type has no `mycalc` representation (closes
    would be `mycalc Int`)
  - Evaluation each bitwise-AST (even sub-AST) immediately returns to it's
    `mycalc Fload` whence executed.
  - I.e. mixing bitwise and "normal" operations both fully legal, possible
    and supported. As long as one keeps in mind that there are no fractions
    in integets.
  - Printing results as integers are not changed. It's still up to the
    output formatter to print a result as an integer or not (i.e. by using
    temporary output-settings like \x;n4 for example)
- Logical operations
   - Are no different from any normal `mycalc` operation, only that new
     tokens are recognised by the lexer, for example `<`, `>` etc. (And all
     that follows wrt parser and AST evaluation of course.)
   - Some logical operations are `bitwise` by nature, but from the
     perspective of operations themselves theres no difference. I.e. either
     side of an logic-AST (`l` or `r`) can be any other AST.
   - Some of these tokens represent themselves (`<`, `>`), but since some
     are conventionally multi-letter (worst case is for example 3-letter
     operators like `=<<`), the concept of `main-token` and `sub-opt` (i.e.
     variant) is heavilly deployed. This fit's the parser quite qell as all
     main-tokens have the same presedency and assosiativity which greatly
     simplifies the rest of the logic.
   - Note that despite some operators can be more than two letters, the
     two-stage categorization (`main-token` and `sub-opt`) is perfecably
     suitable but which of the letters a `sub-opt` is based upon is fuzzy.
  - The `main-token` is always based on all letters, but the `sub-opt` can
    be derived from the last, middle or even in some unusual cases, the
    first. I.e. the rules for building up a `multi-token` is case-by-case
    dependant (as long as lexer, parser and AST evauates on the same rules
    i.e. conceptually no different from any `single-token`.

### Fixed

### Changed

### Removed

## [0.6.7] - 2023-06-11

### Added

- lib `readline` enhanced for much improved line editing
- Built-in history

### Fixed

### Changed

### Removed

## [0.5.5] - 2022-11-19

### Added

- Much improved usability by introducing temporary output formatters
  - Utilized by another parsing technique using POSIX extended `regexp` from
    project `libregsub` - a lightweight but fully opreable alternative to Pearls
    `PRE` if there's no need for UTF/multi-lingual text substituting.
  - Temporary settings are not in human language but a relatively simple
    tokenized grammar which all-in-all did not motivate introducing the big
    monstrosity `PRE`, which is the only free regsubsts capable alternative.
- Support for all common integer types given as input in a `C` style syntax.
- Ditto for output
- Binary input/output format extending limits in libc's stdio (GNU extension
  ditto abandoned as too unstable or obscure).

### Fixed

### Changed

- Fully AST based

### Removed

- BIOF

## [0.4.7] - 2022-11-23

### Added

- First public release

### Fixed

### Changed

### Removed

## [0.0.0] - 2020-07-21

### Added

### Fixed

### Changed

### Removed
