include(CheckLibraryExists)
include(CheckSymbolExists)
#
# Dependant libraries which is actively contributed to within this project
# Note: These paths are to git-submodules. THEIR EXISTENCE IS NOT MANDATORY
#
set(LIB_LIBLOG_PATH         "${PROJECT_SOURCE_DIR}/lib/liblog"
    CACHE STRING "Source-path to library 'liblog'")
set(LIB_LIBMLIST_PATH       "${PROJECT_SOURCE_DIR}/lib/libmlist"
    CACHE STRING "Source-path to library 'libmlist'")
set(LIB_REGSUB_PATH         "${PROJECT_SOURCE_DIR}/lib/regsub"
    CACHE STRING "Source-path to library 'regsub'")

# Check if dependent libraries are installed. If so don't default to offer
# to build them
#
check_library_exists(liblog  log_set_verbosity "${CMAKE_LIBRARY_PATH}" HAVE_LIB_LIBLOG)

# Find library but prefer in-tree if existing"
find_library(LIB_REGSUB regsub
    PATHS "${LIB_REGSUB_PATH}/src"
    PATHS "${CMAKE_BINARY_DIR}/build/regsub/src"
    )
if(LIB_REGSUB STREQUAL "LIB_REGSUB-NOTFOUND")
    message("** WARNING: library regsub not found.")
else()
    set (HAVE_LIB_REGSUB "ON")
    message(STATUS "Library regsub used: ${LIB_REGSUB}")
endif()


# OFFER to build these libraries even if not needed
#
if (HAVE_LIB_LIBLOG)
    set (EXTRA_LIBS ${EXTRA_LIBS} liblog)
    option(MYCALC_BUILD_LIBLOG "Library liblog from source" OFF)
else (HAVE_LIB_LIBLOG)
    if(EXISTS "${LIB_LIBLOG_PATH}")
        message ("** WARNING: Library liblog not installed. Defaults to build from source")
        option(MYCALC_BUILD_LIBLOG "Library liblog from source" ON)
    else()
        message (SEND_ERROR
        "Dependent library liblog isn't installed"
        "Please build from: "
        "https://github.com/mambrus/liblog "
        "Or install: "
        "sudo apt install liblog ")
    endif()
endif (HAVE_LIB_LIBLOG)

if(EXISTS "${LIB_LIBMLIST_PATH}")
    option(MYCALC_BUILD_LIBMLIST "Library libmlist from source" ON)
else()
    option(MYCALC_BUILD_LIBMLIST "Library libmlist from source" OFF)
endif()

if (HAVE_LIB_REGSUB)
    set (EXTRA_LIBS ${EXTRA_LIBS} regsub)
    option(MYCALC_BUILD_REGSUB "Library regsub from source" OFF)
else (HAVE_LIB_REGSUB)
    if(EXISTS "${LIB_REGSUB_PATH}")
        message ("** WARNING: Library regsub not installed. Defaults to build from source")
        option(MYCALC_BUILD_REGSUB "Library regsub from source" ON)
    else()
        message (SEND_ERROR
        "Dependent library regsub isn't installed"
        "Please build from: "
        "https://gitlab.com/mambrus/regsub "
        "Or install: "
        "sudo apt install regsub ")
    endif()
endif (HAVE_LIB_REGSUB)

#
# BUILD selected libraries. With default-options suitable for mycalc
#
if (MYCALC_BUILD_LIBMLIST)
    option(LIBMLIST_BUILD_LIBLOG "Library liblog from source" OFF)
    mark_as_advanced(LIBMLIST_BUILD_LIBLOG)
    add_subdirectory(${LIB_LIBMLIST_PATH} "${PROJECT_BINARY_DIR}/build/libmlist")
    set(MYCALC_LIBS ${MYCALC_LIBS} libmlist_static)
    list(APPEND DFLT_MYCALC_BUILD_OPTIONS ${LIBMLIST_BUILD_OPTIONS})
endif()

if (MYCALC_BUILD_LIBLOG)
    option(LIBLOG_ENABLE_SYSLOG     "Logs to syslog" ON)
    option(LIBLOG_SYSLOG_LOG_PERROR "Log to also stderr if syslog" OFF)
    set(LIBLOG_DEFAULT_LOG_LEVEL
        ${LIBLOG_DEFAULT_LOG_LEVEL}INFO
        CACHE STRING
        "Set explicit default log-level: [0-6] | [VERBOSE|DEBUG|INFO|WARNING|ERROR|CRITICAL|SILENT|SYS]")
    add_subdirectory(${LIB_LIBLOG_PATH} "${PROJECT_BINARY_DIR}/build/liblog")
    set(MYCALC_LIBS ${MYCALC_LIBS} liblog_static)
    list(APPEND DFLT_MYCALC_BUILD_OPTIONS ${LIBLOG_BUILD_OPTIONS})
else ()
    list(APPEND DFLT_MYCALC_BUILD_OPTIONS "-Wl,--undefined=__liblog_init")
endif()

if (MYCALC_BUILD_REGSUB)
    option(REGSUB_BUILD_TESTS       "Buld regsub test programs" OFF)
    add_subdirectory(${LIB_REGSUB_PATH} "${PROJECT_BINARY_DIR}/build/regsub")
    message ("** System-include directory-path added: ${LIB_REGSUB_PATH}/src/include")
    include_directories("${LIB_REGSUB_PATH}/src/include")
    set(MYCALC_LIBS ${MYCALC_LIBS} regsub)
    list(APPEND DFLT_MYCALC_BUILD_OPTIONS ${REGSUB_BUILD_OPTIONS})
endif()

set (LOG_LEVEL
    ${LOG_LEVEL} LOG_LEVEL_INFO
    CACHE STRING
    "Default LOG_LEVEL (over-ride with env-var)")

set_property(CACHE LOG_LEVEL
    PROPERTY STRINGS
    "LOG_LEVEL_VERBOSE"
    "LOG_LEVEL_DEBUG"
    "LOG_LEVEL_INFO"
    "LOG_LEVEL_WARNING"
    "LOG_LEVEL_ERROR"
    "LOG_LEVEL_CRITICAL"
    "LOG_LEVEL_SILENT")


message(STATUS "DFLT_MYCALC_BUILD_OPTIONS: ${DFLT_MYCALC_BUILD_OPTIONS}")

# Actuate the inherited options, and offer tuning but as "advanced"
# ===========================================================================
#

set(MYCALC_BUILD_OPTIONS
    ${DFLT_MYCALC_BUILD_OPTIONS}
    CACHE STRING
    "Extra build options. Defaults iherited from sub-projects")
mark_as_advanced(MYCALC_BUILD_OPTIONS)

string(REPLACE ";" " " _MYCALC_BUILD_OPTIONS "${MYCALC_BUILD_OPTIONS}")
set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} ${_MYCALC_BUILD_OPTIONS}")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${_MYCALC_BUILD_OPTIONS}")

# Notes to self:
# ==========================================================
# * Consider using find_package(<packake> [version] [REQUIRED])
#
# * A more elegant solution for the flags:
#   add_compile_options(${MYCALC_BUILD_OPTIONS})
#   add_link_options(${MYCALC_BUILD_OPTIONS})    // CMake +3.15.5 needed

