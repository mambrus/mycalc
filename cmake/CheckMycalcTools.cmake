#
# Tools nessesary for build
#
find_program(EXE_LEX
    flex
)
if(EXE_LEX STREQUAL "EXE_LEX-NOTFOUND")
    message(FATAL_ERROR "Please install flex")
endif()

find_program(EXE_YACC
    bison
)
if(EXE_YACC STREQUAL "EXE_YACC-NOTFOUND")
    message(FATAL_ERROR "Please install bison >3.7!")
else()
    execute_process(COMMAND bison --version
                    OUTPUT_VARIABLE bison_VERSION_RAW_OUTPUT)

    string(REGEX MATCH
        "(.*)(\\(GNU Bison\\) )([0-9]+\.[0-9]+(\.[0-9]+\.)?)(.*)"
        dummy
        "${bison_VERSION_RAW_OUTPUT}"
    )
    set(bison_VERSION "${CMAKE_MATCH_3}")
    if (${bison_VERSION} VERSION_GREATER_EQUAL "3.7.0")
        message(STATUS "bison version OK: ${bison_VERSION}")
    else ()
        message(FATAL_ERROR "Bison version fail. Please install version >3.7")
    endif ()

endif()

find_program(EXE_ASCIIDOCTOR
    asciidoctor
)
if(EXE_ASCIIDOCTOR STREQUAL "EXE_ASCIIDOCTOR-NOTFOUND")
    message(FATAL_ERROR "Please install asciidoctor")
endif()

find_program(EXE_GROFFER
    groffer
)
if(EXE_GROFFER STREQUAL "EXE_GROFFER-NOTFOUND")
    message(FATAL_ERROR "Please install groffer (groff)")
endif()

find_program(EXE_HSUBST
    hsubst
)
if(EXE_HSUBST STREQUAL "EXE_HSUBST-NOTFOUND")
    message(FATAL_ERROR "Please install hsubst")
endif()

find_program(EXE_HELP2MAN
    help2man
)
if(EXE_HELP2MAN STREQUAL "EXE_HELP2MAN-NOTFOUND")
    message(FATAL_ERROR "Please install help2man")
endif()

#
# Local tools
#
find_program(EXE_DOCGEN
    docgen
    PATHS ${CMAKE_SOURCE_DIR}/bin
)
if(EXE_DOCGEN STREQUAL "EXE_DOCGEN-NOTFOUND")
    message(FATAL_ERROR "Please install docgen (roff -> c-file)")
endif()
find_program(EXE_HELP2ROFF
    help2roff
    PATHS ${CMAKE_SOURCE_DIR}/bin
)
if(EXE_HELP2ROFF STREQUAL "EXE_HELP2ROFF-NOTFOUND")
    message(FATAL_ERROR "local help2roff is missing")
endif()

find_program(EXE_SLOG
    slog
    PATHS ${CMAKE_SOURCE_DIR}/bin
)
if(EXE_SLOG STREQUAL "EXE_SLOG-NOTFOUND")
    message(FATAL_ERROR "Please install slog (sys-log pager)")
endif()

find_program(EXE_SIGGEN
    siggen
    PATHS ${CMAKE_SOURCE_DIR}/bin
)
if(EXE_SLOG STREQUAL "EXE_SIGGEN-NOTFOUND")
    message(FATAL_ERROR "Please install siggen (signal handler code-gen)")
endif()
