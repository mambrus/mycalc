include(CheckLibraryExists)

CHECK_LIBRARY_EXISTS(m sin "" HAVE_LIB_M)
CHECK_LIBRARY_EXISTS(readline add_history "" HAVE_LIB_READLINE)
CHECK_LIBRARY_EXISTS(dl dladdr "" HAVE_LIB_DL)
CHECK_LIBRARY_EXISTS(termcap tgetnum "" HAVE_LIB_TERMCAP)

if (HAVE_LIB_M)
    set(MYCALC_LIBS ${MYCALC_LIBS} m)
else ()
    message (FATAL_ERROR
        "Required system library m (GNU math) not found")
endif ()

if (HAVE_LIB_READLINE)
    option(MYCALC_USE_READLINE
        "Use GNU readline" ON)
    set(MYCALC_LIBS ${MYCALC_LIBS} readline)
else ()
    message (WARNING
        "System library readline-dev not installed. "
        "Can't build including realine support. "
        "Please consider installing!!!. "
        )
endif ()

if (HAVE_LIB_DL)
    option(MYCALC_USE_DL
        "Use GNU dl" ON)
    set(MYCALC_LIBS ${MYCALC_LIBS} dl)
else ()
    message (FATAL_ERROR
        "Required system library GNU dl not found")
endif ()

if (HAVE_LIB_TERMCAP)
    set(MYCALC_LIBS ${MYCALC_LIBS} termcap)
else ()
    message (FATAL_ERROR
        "Required system library termcap not found")
endif ()
