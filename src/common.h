#ifndef common_h
#define common_h

#define MAX(a, b)               \
    ({                          \
        __typeof__(a) _a = (a); \
        __typeof__(b) _b = (b); \
        _a > _b ? _a : _b;      \
    })

#define MIN(a, b)               \
    ({                          \
        __typeof__(a) _a = (a); \
        __typeof__(b) _b = (b); \
        _a < _b ? _a : _b;      \
    })

#define elements(A) (sizeof(A) / sizeof(A[0]))
#define ASZ elements

#define _STR(s) #s
#define STR(s) _STR(s)
#define UNUSED(x) (void)(x)

/* Bases */
enum base { BASE_BIN = 2, BASE_OCT = 8, BASE_DEC = 10, BASE_HEX = 16 };
enum base_id { BIN = 0, OCT, DEC, HEX, BASE_SENTINEL };

/* Number types, only that exists  */
typedef long long Int;
typedef long double Float;

/* Special unsigned Int used only for bitwise operations */
typedef unsigned long long Unsigned;

typedef Float (*f1)(Float);
typedef Float (*f2)(Float, Float);
typedef Float (*f3)(Float, Float, Float);
typedef Float (*f4)(Float, Float, Float, Float);
extern const char knownTypes[];

struct fun {
    union {
        void *(*f)(void *);
        f1 f1;
        f2 f2;
        f3 f3;
        f4 f4;
        void *p;
    };

    /* Function signature. One letter per argument, plus the return,
       representing type. It's up to the lexer to create correct signatures.

       Currently the following types are valid for signatures:
       F - Float
       I - Int
       V - Void
     */
    const char *kind;
};

#endif // common_h
