#define _GNU_SOURCE
#include "ast.h"
#include "userio/io.h"
#include "mycalc_yacc.h"
#include <math.h>
#include <assert.h>
#include <stddef.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <liblog/assure.h>
#include <liblog/log.h>
#include <mlist.h>
#include "config.h"
#include "ast_debug.h"
#include "misc.h"

const char knownTypes[] = "VIF";
static struct ast *lindex(struct ast *list, int indx);
static Float runFun(struct fun *fun, struct ast *ast);
static bool validArguments(const char *kind);
extern struct ast bad_ast;

extern int depth;
static char *errmsg = NULL;

/* Evaluate macro - Traverse LHS first, i.e. LTR. */
#define OPER_LR(A, OP) (EVAL(A, l) OP EVAL(A, r))
#define CASE_LR(A, OP, L)      \
    case L:                    \
        LOG_OP(OP);            \
        A->y = OPER_LR(A, OP); \
        break

/* Ditto evaluate macro but for bitwise operators */
#define OPER_LR_BIT(A, OP) ((Unsigned)(EVAL(A, l))OP(Unsigned)(EVAL(A, r)))
#define CASE_LR_BIT(A, OP, L)      \
    case L:                        \
        LOG_OP(OP);                \
        A->y = OPER_LR_BIT(A, OP); \
        break

/* Assignment (+= -= etc)  */
#define CASE_ASGN(A, OP, L)       \
    case L:                       \
        LOG_OP(OP);               \
        *A->l->mem OP EVAL(A, r); \
        break
/* Ditto assignment but for bitwise operators (<<= >>=)  */
#define CASE_ASGN_BIT(A, OP, L)                            \
    case L:                                                \
        LOG_OP(OP);                                        \
        *A->l->mem = (Int)(*A->l->mem)OP(Int)(EVAL(A, r)); \
        break

static Float _ast_eval(struct ast *ast)
{
#ifdef MYCALC_DEBUG_AST
    eval_depth++;
#endif

    switch (ast->op) {
        Float cval; /* Conditional value used by CONDITIONAL, WHILEDO etc */

        CASE_LR(ast, >, '>');
        CASE_LR(ast, <, '<');
        CASE_LR(ast, +, '+');
        CASE_LR(ast, -, '-');
        CASE_LR(ast, *, '*');
        CASE_LR(ast, /, '/');
        CASE_LR_BIT(ast, |, '|');
        CASE_LR_BIT(ast, &, '&');

    /* Multi letter COMPARISON, ie GE, LT*/
    case COMPARISON:
        switch (ast->subop) {
            CASE_LR(ast, >=, '>');
            CASE_LR(ast, <=, '<');
        default:
            ast_error("Bad sub-op LE/GE: %c", ast->subop);
        }
        break;

    /* Multi letter EQUALITY, ie GE, LT*/
    case EQUALITY:
        switch (ast->subop) {
            CASE_LR(ast, ==, '=');
            CASE_LR(ast, !=, '!');
        default:
            ast_error("Bad sub-op LE/GE: %c", ast->subop);
        }
        break;

    /* Multi letter LOGICAL, ie &&, ||*/
    case LOGICAL:
        switch (ast->subop) {
            CASE_LR(ast, &&, '&');
            CASE_LR(ast, ||, '|');
        default:
            ast_error("Bad sub-op AND/OR: %c", ast->subop);
        }
        break;

    case BITSHIFT:
        switch (ast->subop) {
            CASE_LR_BIT(ast, >>, '>');
            CASE_LR_BIT(ast, <<, '<');
        default:
            ast_error("Bad sub-op for bitwise shift: %c", ast->subop);
        }
        break;

    case '%':
        LOG_OP(%);
        ast->y = fmodl(EVAL(ast, l), EVAL(ast, r));
        break;

    case UNARY_NOT:
        LOG_OP(!);
        ast->y = !(bool)EVAL(ast, l);
        break;

    case UNARY_MINUS:
        LOG_OP(N);
        ast->y = -1.0L * EVAL(ast, l);
        break;

    case UNARY_ONECOMPL:
        LOG_OP(~);
        ast->y = ~(Int)(EVAL(ast, l));
        break;

    case '=':
        if (ast->l == NULL)
            ast_error("Unknown LHS for: %c", ast->op);

        if (!((ast->l->op == 'M') || (ast->l->op == 'R')))
            ast_error("Bad LHS for: %c", ast->op);

        switch (ast->subop) {
            CASE_ASGN(ast, +=, '+');
            CASE_ASGN(ast, -=, '-');
            CASE_ASGN(ast, *=, '*');
            CASE_ASGN(ast, /=, '/');
            /* NOTE: The operations in CASE_ASGN_BIT are "read-modify-wright".
             * Hence the untrue look of the operator actuating */
            CASE_ASGN_BIT(ast, <<, '<'); /* <<= (3 letter op) */
            CASE_ASGN_BIT(ast, >>, '>'); /* >>= */
            CASE_ASGN_BIT(ast, &, '&'); /*   &= (2 letter op)*/
            CASE_ASGN_BIT(ast, |, '|'); /*   |= */
        case '%':
            LOG_OP(%=);
            *ast->l->mem = fmodl(*ast->l->mem, EVAL(ast, r));
            break;
        default:
            LOG_OP(=);
            *ast->l->mem = EVAL(ast, r);
        }
        ast->y = EVAL(ast, l);
        break;

    case 's': /* This is a local scope symbol */
        LOG_OP(s);
        ASSERT(ast->r);
        if (ast->l) {
            LOGW("local-scope symbol has a global with the same name\n");
        }
        ILOGD(eval_depth, "Local scope symbol [%s] at ast->r to be evaluated\n",
              ast->sym_name);
        /* Peek ahead sanity check */
        ASSERT(ast->r->op == 'E');
        ASSERT(strcmp(ast->sym_name, ast->r->sym_name) == 0);
        ASSERT(ast->r->r != NULL);
        ast->y = EVAL(ast, r);
        ILOGD(eval_depth, "Local scope symbol [%s] evals to: %.2Lf\n",
              ast->r->sym_name, ast->y);
        break;

    case WHILEDO: /* This is a top-level AST of a WHILE-DO loop. */
        LOG_OP(W);
        ASSERT(ast->l); /* Left side is the condition */

        /* Right side is another AST with a true-side (l=left), and a non-true
         * side (r=right) */
        while (cval = EVAL(ast, l)) {
            if (ast->r) {
                ast->r->cond =
                    (bool)(cval); /* Tell the BRANCH AST that follows which turn to take*/
                ast->y = EVAL(ast, r);
                if (ast->y == NAN) {
                    LOGW("Incomplete BRANCH. Returning conditional value");
                    ast->y = cval;
                }
            } else {
                LOGE("Incomplete CONDITIONAL. Returning conditional value");
                ast->y = cval;
            }
        }
        break;
    case CONDITION: /* This is a top-level AST of a CONDITION */
        LOG_OP(C);
        ASSERT(ast->l); /* Left side is the condition */

        /* Right side is another AST with a true-side (l=left), and a non-true
         * side (r=right) */
        cval = EVAL(ast, l);
        if (ast->r) {
            ast->r->cond =
                (bool)(cval); /* Tell the BRANCH AST that follows which turn to take*/
            ast->y = EVAL(ast, r);
            if (ast->y == NAN) {
                LOGW("Incomplete BRANCH. Returning conditional value");
                ast->y = cval;
            }
        } else {
            LOGW("Incomplete CONDITIONAL. Returning conditional value");
            ast->y = cval;
        }
        break;
    case BRANCH: /* This is a bottom-half AST of a CONDITION */
        LOG_OP(BRANCH);

        if (ast->cond) {
            /* Condition previously evaluated TRUE and LHS exists, evaluate it */
            if (ast->l)
                ast->y = EVAL(ast, l);
            else
                ast->y = NAN; /* Rapport failure upwards */
        } else {
            /* Condition previously evaluated FALSE and RHS exists, evaluate it */
            if (ast->r)
                ast->y = EVAL(ast, r);
            else
                ast->y = NAN; /* Rapport failure upwards */
        }
        break;

    case ';':
        LOG_OP(X);
        ASSERT(ast->l);
        ASSERT(ast->r);
        set_result(EVAL(ast, l)); /* Silent set so NXT can use 'R' */
        ast->y = EVAL(ast, r);
        break;

    case 'F': /* This is a GLOBAL scope user function symbol */
        LOG_OP(F);
        ASSERT(ast->l);
        ast->y = ast_runfun(ast, ast->l);
        break;
    case 'S': /* This is a GLOBAL scope symbol */
        LOG_OP(S);
        ASSERT(ast->l);
        if (ast->r) {
            LOGE("We're evaluating the wrong symbol!!! (There's a local...)\n");
        }
        ast->y = EVAL(ast, l);
        break;
    case ',': /* This is a list element node. Value/expr is TTR */
        ast->y = EVAL(ast, r);
        break;

    case 'K': /* End leaf: already has value in y */
        LOG_OP(K);
        break;

    case 'B': {
        char msgbuf[127];
        int idx = 0;
        LOGE("Bad node in AST [%p]: %d\n", ast->l, ast->symcode);
        idx += sprintf(&msgbuf[idx], "Bad AST encountered: ");
        switch (ast->symcode) {
        case SYM_UNKNOWN:
            idx += sprintf(&msgbuf[idx], "Symbol not found [%s]",
                           ast->l->sym_name);
            break;
        default:
            idx += sprintf(&msgbuf[idx], "Bad AST encountered");
        }
        idx += sprintf(&msgbuf[idx], "\n");
        ast_error(msgbuf);
    } break;
    case 'E':
        /* Empty or place-holder symbol */
        LOG_OP(E);
        ASSERT(ast->r);
        ast->y = EVAL(ast, r);
        break;
    case 'M': /* End leaf: But as reference */
        LOG_OP(M);
        break;
    case 'R':
        LOG_OP(R);
        ast->y = *ast->mem;
        break;
    case 'f':
        LOG_OP(f);
        if (ast->l != NULL) { /* Arguments are an AST TTL */
            if (validArguments(ast->fun->kind)) { /* Worth continuing? */
                return runFun(ast->fun, ast->l);
            } else {
                ast_error("Invalid call to build in function: %c/0x%x", ast->op,
                          ast->op);
            }
        }
        break;

    default:
#ifdef MYCALC_DEBUG_AST
        asprintf(&errmsg, "BUG: AST-eval does not recognize token: (%li) %s",
                 ast->op, ast_tokname(ast->op));
#else
        asprintf(&errmsg, "BUG: AST-eval does not recognize token: %li",
                 ast->op);
#endif
        ast_error(errmsg);
        free(errmsg);
    }
#ifdef MYCALC_DEBUG_AST
    eval_depth--;
    prettyp_finish(eval_depth + 1, ast->y);
#endif
    return ast->y;
}

Float ast_eval(struct ast *ast)
{
    return _ast_eval(sym_resolve(ast));
}

/* Get the n'th AST in a list */
static struct ast *lindex(struct ast *ast, int idx)
{
    static int ldepth = 0;
    ASSERT(ast->op == ',');

    ldepth++;
    if (idx > 0) {
        if (ast->op != ',')
            ast_error("More list elements expected after the %d'th\n", idx);
        /* Next list element is TTL */
        ASSERT(ast->l);
        return lindex(ast->l, --idx);
    }

    ASSURE_E(ast->r != NULL, goto return_error);

    if (ast->r->op == 'K') {
        ILOGD(ldepth, "Arg %d from call-stack: Constant: %.2Lf\n", ldepth - 1,
              ast->r->y);
    } else {
        ILOGD(ldepth, "Arg %d from call-stack: AST: [%p]\n", ldepth - 1,
              ast->r);
    }

    ldepth = 0;
    return ast->r;

return_error:
    ILOGE(ldepth, "Argument missing for [%p]\n", ast);
    ldepth--;
    return ast;
}

#define ARG(N) (_ast_eval(lindex(argv, N)))
/* Execute a function of a certain signature */
static Float runFun(struct fun *fun, struct ast *argv)
{
    int argc;

    if (argv == NULL)
        ast_error("Internal error. "
                  "Trying to execute AST as function but it's not\n");
    if (argv->op != ',')
        ast_error("Internal error. "
                  "Trying to execute AST but argv is not a list \n");

    /* Only number of arguments needs to be cared for as compiler will if
       needed:

       - down-cast return (auto-convert type, but with same kind of
       loss, just more computational expensive)
       - type converts argument ("up-cast" i.e. loss-less)

       This neat feature makes it possible to handle many signature
       combinations without explicit casting. Just add more functions with
       more #args Number-type arguments and append the case-list here.

       All legal signatures are early returns. I.e. common error-handling is
       just after the switch-case.
     */
    argc = strnlen(fun->kind, MYCALC_FUNC_MAXARGS);
    argc--; /* Don't count the return signature letter */
    ASSERT((argc > 0) && (argc < MYCALC_FUNC_MAXARGS));
    switch (argc) {
    case 1:
        return fun->f1(ARG(0));
        break;
    case 2:
        return fun->f2(ARG(0), ARG(1));
        break;
    case 3:
        return fun->f3(ARG(0), ARG(1), ARG(3));
        break;
    case 4:
        return fun->f4(ARG(0), ARG(1), ARG(3), ARG(4));
        break;
    }
    ast_error("Unknown function signature %s", fun->kind);
    return 0;
}

/* Validate the arguments to build-in function by iterating the "kind"
 * string, which is a mangled signature of the faction describing argument
 * and return types using a simple letter convention */
static bool validArguments(const char *kind)
{
    char c;
    bool found;
    int i, k, l;

    i = strlen(kind);
    for (k = 0, found = true; k < i && found; k++) {
        c = kind[k];
        for (l = 0, found = false; l <= i && !found; l++) {
            found = (c == knownTypes[l]);
        }
    }
    return found;
};
