#include "ast.h"
#include "userio/io.h"
#include "mycalc_yacc.h"
#include <math.h>
#include <assert.h>
#include <stddef.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <liblog/assure.h>
#include <liblog/log.h>
#include <mlist.h>
#include "config.h"
#include "ast_debug.h"

extern struct ast bad_ast;

struct ast *ast_newufun(const char *name, struct ast *argv)
{
    struct ast *_ast = ast_new('F', argv, NULL);
    _ast->sym_name = strdup(name);
    return _ast;
}

#undef LDATA
#define LDATA struct sym

/* Run user-function
 * Look-up
 * arg-bind
 * return evaluated symbol
 * */
Float ast_runfun(struct ast *fun, struct ast *argv)
{
    int ilsc = 0;

    int arg_bind(handle_t scoped_syms, struct ast * argv)
    {
        static size_t depth = 0;
        int ilsc = 0;
        static int underflow = 0;
        struct sym *sym_p;

        if (argv == NULL) {
            underflow++;
            ILOGW(depth, "Local symbol for argument nr [%d] left dangling\n",
                  depth + 1);
        }

        depth++;
        ASSERT(argv->op == ',');

        sym_p = CREF(scoped_syms);
        if (sym_p != NULL) {
            /*NOTE: LHS AST the placeholder AST */
            ASSERT(sym_p->ast);
            ASSERT(sym_p->ast->op == 'E');

            sym_p->ast->r = argv->r; /* RIGHT = Value AST (Konstant etc) */
            ilsc++;
            ILOGD(depth, "Argument (%s) #[%d] bound to AST of type (%c)\n",
                  sym_p->name, depth, sym_p->ast->op);
            if (argv->l) { /* LEFT  = Next arg */
                mlist_next(scoped_syms);
                ilsc += arg_bind(scoped_syms, argv->l);
            }
        } else {
            ILOGE(depth, "Too many arguments for function\n", fun->sym_name);
            ast_error("Too many arguments for function\n");
            ilsc = ARG_OVERFLOW;
        }
        depth--;
        return ilsc;
    };

    struct sym *sym = sym_glookup((char *)fun->sym_name);
    if (sym == NULL) {
        ast_error("Symbol not found");
    }

    /* Symbol's local scoped variables are already resolved in it's AST.
     * Binding them now is as rigid as setting LHS, but servers not purpose
     * not to for the same reason. */
    mlist_head(sym->scoped_syms);
    ilsc = arg_bind(sym->scoped_syms, argv);
    PRINTF("Number of arguments bound for user-defined function %s(): %d\n",
           fun->sym_name, ilsc);

    if (ilsc >= 0) {
        return ast_eval(sym->ast);
    }

    LOGE("Running user-function %s failed, error-code: %d\n", fun->sym_name,
         ilsc);
    return bad_ast.y;
}

#undef LDATA
