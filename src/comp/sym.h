#ifndef sym_h
#define sym_h

#include <mlist.h>
#include <stdbool.h>

struct ast;

struct sym {
    /* Symbols name */
    char *name;

    /* Symbol is also an one leaf AST to be able to be part of an expression */
    struct ast *ast;

    /* List of locally scoped symbols */
    handle_t scoped_syms;
};

/*
 * Result codes for xref
 */
enum symcode {
    SYM_BUG = -127, /* Internal error */
    SYM_UNKNOWN, /* Symbol not found */
    FUN_UNKNOWN, /* Function not found */
    SYM_MIXUP, /* Attempting calling non-function as function or v.v. */
    ARG_OVERFLOW /* Too many arguments to user-function */
};

struct sym *sym_lookup(const char *name, bool create);
struct sym *sym_lookup_in(const char *name, handle_t *scoped_syms, bool create);
struct sym *sym_glookup(const char *name);
void sym_rhs_liberate(struct sym *sym);

int sym_rhs_xref(struct sym *sym, struct ast *ast);
struct ast *sym_resolve(struct ast *ast);

#endif //sym_h
