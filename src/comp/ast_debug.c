#include <stdlib.h>
#include <string.h>
#include <liblog/assure.h>
#include <liblog/log.h>
#include "config.h"
#include "ast.h"
#include "ast_debug.h"

#ifdef MYCALC_DEBUG_AST
static char *sprintnumber(char *buffert, Float number)
{
    sprintf(buffert, "%.Lf", number);
    return buffert;
}

int eval_depth = 0;
int free_depth = 0;
/* Debug variant of eval printing coming from left or right*/
Float debug_eval(struct ast *ast, char d)
{
    prettyp_enter(eval_depth + 1, ast, d);
    return ast_eval(ast);
}

void prettyp_enter(int depth, struct ast *ast, char d)
{
    char buffer[40] = { 0 };

    UNUSED(buffer);
    ILOGI(depth, "%c -> [%p]\n", d, ast);
}

void prettyp_finish(int depth, Float number)
{
    char buffer[40] = { 0 };

    ILOGI(depth, "%s\n", sprintnumber(buffer, number));
}
#endif
