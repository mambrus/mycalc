option(MYCALC_DEBUG_LEXER
    "Debug prints - lexer" OFF)

if (MYCALC_DEBUG_LEXER)
    list(APPEND MYCALC_LIBS dl)
endif ()

option(MYCALC_DEBUG_PARSER
    "Debug prints - parser" OFF)

option(MYCALC_DEBUG_AST
    "Debug prints - AST" OFF)

option(MYCALC_DEBUG_SYM
    "Debug prints - SYM" OFF)

set(MYCALC_FUNC_MAXARGS
    "10"
    CACHE STRING
    "Max possible number of arguments to built-in functions")
set(MYCALC_RES_REG
    "R"
    CACHE STRING
    "Register storing expression results")
