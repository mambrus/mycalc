/* Simple math-wrappers */

#ifndef mathwrap_h
#define mathwrap_h

#include "main.h"
#include "settings.h"
#include <math.h>

/* Trigonometric functions operating on degrees instead of radians */
#define DEG2RAD(A) (A * 2.0L * M_PI / 360.0L)
#define RAD2DEG(A) (A * 360.0L / (2.0L * M_PI))

/* clang-format off */
static inline double long _acosl(double long a) { return RAD2DEG(acosl(a)); };
static inline double long _acoshl(double long a) { return RAD2DEG(acoshl(a)); };
static inline double long _asinl(double long a) { return RAD2DEG(asinl(a)); };
static inline double long _asinhl(double long a) { return RAD2DEG(asinhl(a)); };
static inline double long _atanl(double long a) { return RAD2DEG(atanl(a)); };
static inline double long _atanhl(double long a) { return RAD2DEG(atanhl(a)); };
static inline double long _cosl(double long a) { return cosl(DEG2RAD(a)); };
static inline double long _coshl(double long a) { return coshl(DEG2RAD(a)); };
static inline double long _sinl(double long a) { return sinl(DEG2RAD(a)); };
static inline double long _sinhl(double long a) { return sinhl(DEG2RAD(a)); };
static inline double long _tanl(double long a) { return tanl(DEG2RAD(a)); };
static inline double long _tanhl(double long a) { return tanhl(DEG2RAD(a)); };

/* Extensions to libm */
inline static double long _lnl(double long x) { return logl(x)/M_LOG10E; };
/* clang-format on */

/* Conditional Converters. iow convert only if mode is not the same,
 * otherwise just pipe-through */
inline static double long _deg(double long a)
{
    return mycalc.settings->trig_mode != RAD ? RAD2DEG(a) : a;
};

inline static double long _rad(double long a)
{
    return mycalc.settings->trig_mode != DEG ? DEG2RAD(a) : a;
};

/* Conditional trigonometric function  */
#define FTRIGO(F) (mycalc.settings->trig_mode == DEG ? _##F : F)

#endif //mathwrap_h
