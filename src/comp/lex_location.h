/* Flex location */

#ifndef lex_location_h
#define lex_location_h

#define YY_USER_ACTION yylloc->last_column += yyleng;

/* Initialize LOC. */
#define LOCATION_RESET(Loc)                    \
    (Loc).first_column = (Loc).first_line = 1; \
    (Loc).last_column = (Loc).last_line = 1;

/* Advance of NUM lines. */
#define LOCATION_LINES(Loc, Num) \
    (Loc).last_column = 1;       \
    (Loc).last_line += Num;

/* Restart: move the first cursor to the last position. */
#define LOCATION_STEP(Loc)                  \
    (Loc).first_column = (Loc).last_column; \
    (Loc).first_line = (Loc).last_line;

/* Output LOC on the stream OUT. */
#define LOCATION_PRINT(Out, Loc)                                          \
    if ((Loc).first_line != (Loc).last_line)                              \
        fprintf(Out, "%d.%d-%d.%d", (Loc).first_line, (Loc).first_column, \
                (Loc).last_line, (Loc).last_column - 1);                  \
    else if ((Loc).first_column < (Loc).last_column - 1)                  \
        fprintf(Out, "%d.%d-%d", (Loc).first_line, (Loc).first_column,    \
                (Loc).last_column - 1);                                   \
    else                                                                  \
        fprintf(Out, "%d.%d", (Loc).first_line, (Loc).first_column)

#endif //lex_location_h
