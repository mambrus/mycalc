#ifndef ast_debug_h
#define ast_debug_h

#include "config.h"
#include "common.h"

#ifdef MYCALC_DEBUG_AST

struct ast;
struct sym;

Float debug_eval(struct ast *ast, char lr);
void prettyp_enter(int depth, struct ast *ast, char d);
void prettyp_finish(int depth, Float number);
/* The following is listed here because the parse has to be built with
  YYDEBUG  set*/
char *ast_tokname(int op);

extern int eval_depth;
extern int free_depth;

#    define PRINTF(...) LOGI(__VA_ARGS__)
#    define EVAL(A, R) debug_eval(A->R, STR(R)[0])
#    define LOG_OP(O) ILOGI(eval_depth, "%s\n", STR(O))
#    define FREE_DEPTH(OPER) free_depth OPER
#else //MYCALC_DEBUG_AST
#    define EVAL(A, R) _ast_eval(A->R)
#    define LOG_OP(...)
#    define prettyp_enter(...)
#    define FREE_DEPTH(...)

#    define PRINTF(...)
#    define DEPTH_INC(...)
#    define DEPTH_DEC(...)

/* Redefine whats defined in io.h to swallow the depth */
#    undef ILOGV
#    undef ILOGD
#    undef ILOGI
#    undef ILOGW
#    undef ILOGE
#    define ILOGV(N, ...) LOGV(__VA_ARGS__)
#    define ILOGD(N, ...) LOGD(__VA_ARGS__)
#    define ILOGI(N, ...) LOGI(__VA_ARGS__)
#    define ILOGW(N, ...) LOGW(__VA_ARGS__)
#    define ILOGE(N, ...) LOGE(__VA_ARGS__)
#endif //MYCALC_DEBUG_AST

#endif //ast_debug_h
