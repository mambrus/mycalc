%{
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stddef.h>
#include <liblog/assure.h>
#include <liblog/log.h>

#include "settings.h"

#include "config.h"

#ifdef MYCALC_DEBUG_PARSER
    #define YYDEBUG 1
#endif
#include "mycalc_yacc.h"

int yylex (YYSTYPE*, YYLTYPE*);

Float *_result;           /*Special register: Last result */

#define P_BINOP( OP, A, B, C ) { \
    PRINTF("%s( ", OP ); \
    PRINTF(_format_ne, A); \
    PRINTF(", "); \
    PRINTF(_format_ne, B); \
    PRINTF(")= "); \
    PRINTF(_format_ne, C); \
    PRINTF("\n"); \
}

#define P_OP( OP ) { \
    PRINTF("%s\n", OP ); \
}

#ifdef MYCALC_DEBUG_PARSER
#  define PRINTF(...) \
    LOGI(__VA_ARGS__)
#else
#  define PRINTF(...)
#  undef P_BINOP
#  define P_BINOP(...)
#  undef P_OP
#  define P_OP(...)
#endif

#include "misc.h"

void set_result_register(Float *reg)
{
    _result = reg;
}

char get_result_register()
{
    char rc = 0;

    for(char i='A'; i<'Z'; i++) {
        if (_result == regof(i)) {
            rc = i;
        }
    }
    return rc;
}

/* Silently set intermediate result register with value */
void set_result(Float value)
{
    *_result = value;
}
/* Silently get value from intermediate result register */
Float get_result()
{
    return *_result;
}

%}

%code requires {
    #include <stdlib.h>
    #include <mlist.h>
    #include "common.h"
    #include "comp/ast.h"
    #include "comp/sym.h"
    #include "userio/io.h"
}

%code provides {
    void yyerror(YYLTYPE *yylloc, const char *str, ...);
    const char *yyname (int yysymbol);
}

%locations
%define api.pure full

%token EOL
%token LET
%token SET
%token GET
%token IF
%token THEN
%token ELSE
%token WHILE
%token DO
%token BRANCH
%token CONDITION
%token WHILEDO
%token S_FORMAT
%token S_SCIMODE
%token S_SCI
%token S_NRM
%token S_TRIG
%token S_DEG
%token S_RAD
%token S_LOGLEVEL
%token S_RREG

%union {
    /*Possible types for terminals */
    Int         integer;
    Float       floating;
    struct fun  fun;
    Float       *memory;
    char        *string;
    char        letter;

    /*Types for non-terminals*/
    struct ast *ast;
    struct sym *sym;
    handle_t *list;
}

/* Type for terminals */
%token  <integer>   INTEGER
%token  <floating>  FLOAT
%token  <fun>       FUNC
%token  <memory>    REGISTER
%token  <letter>    ASSIGNBY
%token  <letter>    COMPARISON
%token  <letter>    EQUALITY
%token  <letter>    BITSHIFT
%token  <letter>    LOGICAL
%token  <string>    WORD

/* Type for non-terminals, i.e. wherever $$ is referred in Bison grammar (BNF) */
%type   <ast>       number
%type   <ast>       expr
%type   <ast>       unary_neg
%type   <ast>       unary_not
%type   <ast>       unary_bit
%type   <ast>       subexp
%type   <ast>       func
%type   <ast>       value
%type   <ast>       memory
%type   <ast>       list
%type   <ast>       conditional
%type   <sym>       symdef
%type   <sym>       symdecl
%type   <sym>       fundef
%type   <list>      symlist

/* Explicit presidence and grouping */
/* Presedence as close as possible to C&R: */
/* https://en.wikipedia.org/wiki/Order_of_operations */
/* Note: Some operators have different meaning vs. C&R's */

/* ============= */
/* BIG FAT NOTE: */
/* ============= */
/* Order of the following lines matters matter: earlier = less */

//%nonassoc

%left ',' ';'
%right '=' ASSIGNBY
%left '?' ':'
%left LOGICAL
%left '|'
%left '&'
%left '<' '>' COMPARISON
%left EQUALITY
%left BITSHIFT
%left '+' '-'
%left '*' '/' '%'
%left '^'
%right PRFX_INC PRFX_DEC UNARY_PLUS UNARY_MINUS UNARY_NOT UNARY_ONECOMPL
%left PST_INC PST_DEC

/* Root-rule */
%start execute

%%

/* List the current result */
execute:   /* Nothing */
        | execute EOL
        {
            result_out(_result, NULL);
            PRINTF("Execute complete\n");
        }
        | execute expr EOL
        {
            *_result=ast_eval($2);
            result_out(_result, NULL);
            PRINTF("Execute expression complete\n");
        }
        | execute expr statsep EOL
        {
            *_result=ast_eval($2);
            result_out(NULL, NULL);
            PRINTF("Silent execute expression complete\n");
        }
        | execute statement EOL
        {
            result_out(NULL, NULL);
            PRINTF("Execute statement complete\n");
        }
        | execute settings EOL
        {
            result_out(NULL,"\r");
            PRINTF("Setting handling complete\n");
        }
        ;

statsep: ';'
        | statsep ';'
        ;

settings: SET S_TRIG S_RAD {set_trig_rad(); PRINTF("RAD functions in use\n");}
        | SET S_TRIG S_DEG {set_trig_deg(); PRINTF("DEG functions in use\n");}
        | GET S_TRIG {print_trig();}
        | SET S_SCIMODE S_SCI
        {
            set_printf_post("SCI");
            PRINTF("Post formatter SCI in use\n");
        }
        | SET S_SCIMODE S_NRM
        {
            set_printf_post("NRM");
            PRINTF("Post formatter NRM in use\n");
        }
        | GET S_SCIMODE {print_scimode();}
        | SET S_FORMAT WORD
        {
            set_ofprintf_format($3);
            PRINTF("Number output-format: %s\n",get_ofprintf_format());
        }
        | GET S_FORMAT {printf("%s\n",get_ofprintf_format());}
        | SET S_RREG WORD
        {
            if (strlen($3) != 1 || (!($3[0] >= 'A' && $3[0] <= 'Z'))) {
                yyerror(&yyloc, "Invalid register letter [%s]\n" ,$3);
            } else {
                set_result_register(regof($3[0]));
                PRINTF("Results stored in %s\n", get_result_register());
            }
        }
        | GET S_RREG {printf("%c\n", get_result_register());}
        | SET S_LOGLEVEL WORD {set_loglevel($3);}
        | GET S_LOGLEVEL {get_loglevel();}
        ;

statement: ';'
        | statement ';'
        | symdef ';'
        | fundef ';'
        | symdecl ';'
        ;

/* A conditional is not a statement but a "value". It must have an end-marker
 * however.
 *
 * Re-use of statement's end-marker makes it just look like a statement, where
 * in fact it's just a end-marker. I.e. insted of "FI", "DONE", "END" etc for
 * FLOW operations that have the property of evaluate and return a value, the ";"
 * characte is re-used for all of them */
conditional: IF expr THEN expr ';'
        {
            $$ = ast_newflow(CONDITION, $2, $4, NULL);
        }
        | IF expr THEN expr ELSE expr ';'
        {
            $$ = ast_newflow(CONDITION, $2, $4, $6);
        }
        | expr '?' expr ';'
        {
            $$ = ast_newflow(CONDITION, $1, $3, NULL);
        }
        | expr '?' expr ':' expr ';'
        {
            $$ = ast_newflow(CONDITION, $1, $3, $5);
        }
        | WHILE expr DO expr ';'
        {
            $$ = ast_newflow(WHILEDO, $2, $4, NULL);
        }

/* Symbol and function declaration */
symdecl: symdef '=' expr
        {
            //sym_rhs_liberate($1);
            $1->ast = $3;
            $$ = $1;
        }
        | fundef '=' expr
        {
            int i;
            UNUSED(i);
            //sym_rhs_liberate($1);
            i = sym_rhs_xref($1, $3);
            PRINTF("Number of local symbols bound in AST: %i\n", i);
            $1->ast = $3;
            $$ = $1;
        }
        ;

/* symdef := LET WORD */
fundef: symdef '(' symlist ')'
        {
            PRINTF("Symbolic list processing\n");
            $1->scoped_syms=*$3;
            $1->ast->op='U';  /* Change symbol marker from S to U makes it
                                 easier to resolve (U=User function decl.)*/
            //free($3); TBD
            $$=$1;
        }
        ;

symlist: WORD
        {
            handle_t *scoped_syms = malloc(sizeof(handle_t));
            *scoped_syms = 0; /* Trigger creation of new list */
            sym_lookup_in($1, scoped_syms, true);
            PRINTF("Symlist init with [%s] - [%p]", $1, *scoped_syms);
            $$=scoped_syms;
        }
        | symlist ',' WORD
        {
            PRINTF("Symbol list appnd [%s] - [%p]\n",$3, *$1);
            struct sym* sym = sym_lookup_in($3, $1, false);
            if (sym != NULL) {
                yyerror(&yylloc, "Duplicate symbol in declaration list: %s\n", $3);
                $$=0;
            } else {
                sym = sym_lookup_in($3, $1, true);
            }
            $$=$1;
        }
        ;

symdef: LET WORD
        {
            struct sym* sym;
            sym = sym_lookup($2, true);
            if (sym->ast->op == 'E') {
                sym->ast->op = 'S';
                PRINTF("New global symbol created: %s\n", sym->name);
            } else {
                PRINTF("Symbol found: %s\n", sym->name);
            }
            $$=sym;
        }
        ;

/* R: Value/expr */
/* L: Next list-element or NULL*/
list: expr
        {
            $$=ast_new(',', NULL, $1);
        }
        | list ',' expr
        {
            $$=ast_list_append($1, $3);
        }
        ;

expr: value
        | memory '=' expr
        {
            $$=ast_new('=', $1, $3);
            $$->subop=0;
            PRINTF("R[%c]= \n", 'A' + (int)($1->mem - regof('A')));
        }
        | memory ASSIGNBY expr
        {
            $$=ast_newmultiop('=', $2, $1, $3);
            PRINTF("R[%c]%c= ", 'A' + (int)($1->mem - regof('A')), $2 );
        }
        | expr '<' expr { $$=ast_new('<', $1, $3); P_OP("LT"); }
        | expr '>' expr { $$=ast_new('>', $1, $3); P_OP("GT"); }
        | expr '*' expr { $$=ast_new('*', $1, $3); P_OP("MUL"); }
        | expr '/' expr { $$=ast_new('/', $1, $3); P_OP("DIV"); }
        | expr '%' expr { $$=ast_new('%', $1, $3); P_OP("MOD"); }
        | expr '+' expr { $$=ast_new('+', $1, $3); P_OP("ADD"); }
        | expr '-' expr { $$=ast_new('-', $1, $3); P_OP("SUB"); }
        | expr '&' expr { $$=ast_new('&', $1, $3); P_OP("BND"); }
        | expr '|' expr { $$=ast_new('|', $1, $3); P_OP("BOR"); }
        | expr ';' expr { $$=ast_new(';', $1, $3); P_OP("NXT"); }
        | expr unary_neg { $$=ast_new('+', $1, $2); P_OP("ADD"); }
        | expr '^' expr {
            struct fun fun = {
                .f = (void *)powl,
                .kind = "FFF"
            };
            struct ast *argv=ast_new(',', NULL, $1);
            argv->l=ast_new(',', NULL, $3);
            $$=ast_newfunction(fun, argv); PRINTF("f(...)\n");
        }
        | expr BITSHIFT expr {
            $$=ast_newmultiop(BITSHIFT, $2, $1, $3);
            P_OP("BSH");
        }
        | expr COMPARISON expr {
            $$=ast_newmultiop(COMPARISON, $2, $1, $3);
            P_OP("CPE");
        }
        | expr EQUALITY expr {
            $$=ast_newmultiop(EQUALITY, $2, $1, $3);
            P_OP("EQ");
        }
        | expr LOGICAL expr {
            $$=ast_newmultiop(LOGICAL, $2, $1, $3);
            P_OP("LGC");
        }
        ;

memory: REGISTER  { $$=ast_newregister($1); PRINTF("R:\n"); }

unary_neg: UNARY_MINUS expr
        {
            $$=ast_new(UNARY_MINUS, $2, NULL); P_OP("NEG");
        }
        ;
unary_not: UNARY_NOT expr
        {
            $$=ast_new(UNARY_NOT, $2, NULL); P_OP("NOT");
        }
        ;
unary_bit: UNARY_ONECOMPL expr
        {
            $$=ast_new(UNARY_ONECOMPL, $2, NULL); P_OP("BITINVERSION");
        }
        ;

value: number
        | subexp
        | func
        | memory
        | unary_neg
        | unary_not
        | unary_bit
        | conditional
        | WORD {$$=ast_newsymbol($1);}
        ;

subexp: '(' expr ')' { $$ = $2; PRINTF("( )\n"); };

func:   FUNC '(' list ')'
        { $$=ast_newfunction($1, $3); PRINTF("f(...)\n"); }
        | FUNC '(' ')'
        { $$=ast_newfunction($1, NULL); PRINTF("f( )\n"); }
        | WORD '(' list ')'
        { $$=ast_newufun($1, $3); PRINTF("F(...)\n"); }
        | WORD '(' ')'
        { $$=ast_newufun($1, NULL); PRINTF("F( )\n"); }
        ;

number:   INTEGER   { $$=ast_newnumber($1); PRINTF("I:\n"); }
        | FLOAT     { $$=ast_newnumber($1); PRINTF("F:\n"); }
        ;

%%

#if YYDEBUG || 0
const char *yyname(int yysymbol) {
    return yysymbol_name(yysymbol);
}
#endif
