/* Lexer debug macros for mycalc */

#ifndef lex_debug_h
#define lex_debug_h

#ifdef MYCALC_DEBUG_LEXER
#    define PRINTF(...) LOGI(__VA_ARGS__)

#    define RETURN(R)                                                     \
        if (R == YYEMPTY) {                                               \
            PRINTF("Lex[YYEMPTY]\n");                                     \
        } else if (R == YYEOF) {                                          \
            PRINTF("Lex[YYEOF]\n");                                       \
        } else if (R == YYerror) {                                        \
            PRINTF("Lex[YYerror]\n");                                     \
        } else if (R == YYUNDEF) {                                        \
            PRINTF("Lex[YYUNDEF]\n");                                     \
        } else if (R == EOL) {                                            \
            PRINTF("Lex[EOL]\n");                                         \
        }                                                                 \
                                                                          \
        else if (R == LET) {                                              \
            PRINTF("Lex[LET]\n");                                         \
        } else if (R == IF) {                                             \
            PRINTF("Lex[IF]\n");                                          \
        } else if (R == THEN) {                                           \
            PRINTF("Lex[THEN]\n");                                        \
        } else if (R == ELSE) {                                           \
            PRINTF("Lex[ELSE]\n");                                        \
        } else if (R == WHILE) {                                          \
            PRINTF("Lex[WHILE]\n");                                       \
        } else if (R == DO) {                                             \
            PRINTF("Lex[DO]\n");                                          \
        }                                                                 \
                                                                          \
        else if (R == INTEGER) {                                          \
            PRINTF("Lex[INTEGER]: %Ld\n", yylval->integer);               \
        } else if (R == FLOAT) {                                          \
            PRINTF("Lex[FLOAT]: %Lf\n", yylval->floating);                \
        } else if (R == FUNC) {                                           \
            PRINTF("Lex[FUNC]: %s %p [%s]\n", dl_info_fun(yylval->fun.f), \
                   *yylval->fun.f, yylval->fun.kind);                     \
        } else if (R == REGISTER) {                                       \
            PRINTF("Lex[REGISTER]: %c\n", yylval->letter);                \
        } else if (R == ASSIGNBY) {                                       \
            PRINTF("Lex[ASSIGNBY]: %c\n", yylval->letter);                \
        } else if (R == COMPARISON) {                                     \
            PRINTF("Lex[COMP-OR-EQ]: %c\n", yylval->letter);              \
        } else if (R == LOGICAL) {                                        \
            PRINTF("Lex[LOGICAL]: %c\n", yylval->letter);                 \
        } else if (R == EQUALITY) {                                       \
            PRINTF("Lex[IS_EQUAL]: %c\n", yylval->letter);                \
        } else if (R == WORD) {                                           \
            PRINTF("Lex[WORD]: %s\n", yylval->string);                    \
        } else if (R == BITSHIFT) {                                       \
            PRINTF("Lex[BITSHIFT]: %c\n", yylval->letter);                \
        } else if (R == UNARY_NOT) {                                      \
            PRINTF("Lex[UNARY_NOT]\n");                                   \
        } else if (R == UNARY_ONECOMPL) {                                 \
            PRINTF("Lex[UNARY_ONECOMPL]\n");                              \
        }                                                                 \
                                                                          \
        else if (R == PRFX_INC) {                                         \
            PRINTF("Lex[PRFX_INC]\n");                                    \
        } else if (R == PRFX_DEC) {                                       \
            PRINTF("Lex[PRFX_DEC]\n");                                    \
        } else if (R == UNARY_PLUS) {                                     \
            PRINTF("Lex[UNARY_PLUS]\n");                                  \
        } else if (R == UNARY_MINUS) {                                    \
            PRINTF("Lex[UNARY_MINUS]\n");                                 \
        } else if (R == PST_INC) {                                        \
            PRINTF("Lex[PST_INC]\n");                                     \
        } else if (R == PST_DEC) {                                        \
            PRINTF("Lex[PST_DEC]\n");                                     \
        } else if (R == SET) {                                            \
            PRINTF("Lex[SET]\n");                                         \
        } else if (R == GET) {                                            \
            PRINTF("Lex[GET]\n");                                         \
        } else if (R == S_FORMAT) {                                       \
            PRINTF("Lex[S_FORMAT]\n");                                    \
        } else if (R == S_TRIG) {                                         \
            PRINTF("Lex[S_TRIG]\n");                                      \
        } else if (R == S_DEG) {                                          \
            PRINTF("Lex[S_DEG]\n");                                       \
        } else if (R == S_RAD) {                                          \
            PRINTF("Lex[S_RAD]\n");                                       \
        } else if (R == S_SCIMODE) {                                      \
            PRINTF("Lex[S_SCIMODE]\n");                                   \
        } else if (R == S_SCI) {                                          \
            PRINTF("Lex[S_SCI]\n");                                       \
        } else if (R == S_NRM) {                                          \
            PRINTF("Lex[S_NRM]\n");                                       \
        } else if (R == S_LOGLEVEL) {                                     \
            PRINTF("Lex[S_LOGLEVEL]\n");                                  \
        } else if (R < 256) {                                             \
            PRINTF("Lex[%d]: %c\n", R, R);                                \
        } else {                                                          \
            PRINTF("Lex[UNKNOWN-OP]: %d\n", R);                           \
        }                                                                 \
        return R
#else
#    define PRINTF(...)
#    define RETURN(R) return R
#endif

#endif //lex_debug_h
