#include "sym.h"
#include "ast.h"
#include <liblog/assure.h>
#include <liblog/log.h>
#include <mlist.h>
#include <stdbool.h>
#include "config.h"

#ifdef MYCALC_DEBUG_SYM
#    define PRINTF(...) LOGI(__VA_ARGS__)
int depth = 0;
#    define DEPTH_INC() (++depth)
#    define DEPTH_DEC() (depth--)
#else //MYCALC_DEBUG_SYM
#    define PRINTF(...)
#    define DEPTH_INC(...)
#    define DEPTH_DEC(...)

#    undef ILOGV
#    undef ILOGD
#    undef ILOGI
#    undef ILOGW
#    undef ILOGE
#    define ILOGV(N, ...) LOGV(__VA_ARGS__)
#    define ILOGD(N, ...) LOGD(__VA_ARGS__)
#    define ILOGI(N, ...) LOGI(__VA_ARGS__)
#    define ILOGW(N, ...) LOGW(__VA_ARGS__)
#    define ILOGE(N, ...) LOGE(__VA_ARGS__)

#endif //MYCALC_DEBUG_SYM

/* AST to reply with when failure
 * Failed ast is in either r or l and reason in symcode */
struct ast bad_ast = { .y = 0.0L,
                       .op = 'B',
                       .symcode = SYM_UNKNOWN,
                       .l = NULL,
                       .r = NULL };

/* Assigned asts, i.e. using "let" */
handle_t global_syms = 0;

static int lsc_bind(char *name, struct sym *sym, struct ast *ast);

/* Look name up and return it's ast
 * If it's missing, add it*/
struct sym *sym_lookup(const char *name, bool create)
{
    struct sym *sym_p;

#undef LDATA
#define LDATA struct sym
    sym_p = sym_lookup_in(name, &global_syms, create);
    return sym_p;
#undef LDATA
}

struct sym *sym_lookup_in(const char *name, handle_t *scoped_syms, bool create)
{
    struct ast *ast;
    struct sym sym;
    struct sym *sym_p;

    if (!*scoped_syms) {
        if (create) {
            ASSURE(mlist_opencreate(sizeof(struct sym), NULL, scoped_syms) ==
                   0);
            ILOGD(depth, "New symlist [%p]\n", *scoped_syms);
        } else {
            return NULL;
        }
    }
#undef LDATA
#define LDATA struct sym
    ITERATE(*scoped_syms)
    {
        sym_p = CREF(*scoped_syms);
        if (strcmp(sym_p->name, name) == 0) {
            ILOGD(depth, "Symbol [%s] found in symlist [%p]\n", name,
                  *scoped_syms);
            return sym_p;
        }
    }
    if (!create) {
        ILOGD(depth, "Symbol [%s] not found in symlist [%p]\n", name,
              *scoped_syms);
        return NULL;
    }

    /* Not found, create new */
    ILOGD(depth, "Creating new sym [%s] in symlist [%p]\n", name, *scoped_syms);
    ast = ast_new('E', NULL, NULL);
    sym.ast = ast;
    sym.name = strdup(name);
    mlist_add_last(*scoped_syms, &sym);
    sym_p = CREF(*scoped_syms);
    return sym_p;
#undef LDATA
}

struct sym *sym_glookup(const char *name)
{
    return sym_lookup_in(name, &global_syms, false);
}

/*
 * Liberate AST from a symbol, i.e. any existing AST is symbol-name is
 * reused.
 *
 * I.e. AST exist only for as long as there's a symbol owning it
 */
void sym_rhs_liberate(struct sym *sym)
{
    if (sym->ast != NULL) {
        if (sym->ast->op == 'E' || sym->ast->op == 'S') {
            ILOGI(depth, "Symdecl: clean AST value bound\n");
            free(sym->ast);
        } else {
            ILOGI(depth, "Symdecl: Reusing symbol. Old AST is GC\n");
            ast_free(sym->ast);
        };
    } else {
        ILOGE(depth, "Symdecl: Completely free AST. How is this possible?\n");
    }
}

/*
 * Cross-bind a function's symbol-table into the RHS AST
 *
 * Function returns:
 * Positive number: How many symbols where bound
 */
int sym_rhs_xref(struct sym *sym, struct ast *ast)
{
    int ilsc = 0;
    struct sym *sym_p;

    ILOGD(depth, "Symbol [%s]'s list [%p] xref with AST [%p]\n", sym->name,
          sym->scoped_syms, sym->ast);

#undef LDATA
#define LDATA struct sym
    ITERATE(sym->scoped_syms)
    {
        sym_p = CREF(sym->scoped_syms);
        ILOGD(depth, "Searching for local-scope symbol [%s:%p] in AST [%p]\n",
              sym_p->name, sym_p, ast);
        ilsc += lsc_bind(sym_p->name, sym_p, ast);
    }
#undef LDATA

    return ilsc;
}

/*
 * Local scope bind
 * For each name, replace place-holder AST with functions symbol-ast.
 */
int lsc_bind(char *name, struct sym *sym, struct ast *ast)
{
    int tilsc = 0;
    int ilsc = 0;

    ILOGD(depth, "l> [%p]\n", ast);
    ;
    DEPTH_INC();

    if (ast->r) {
        tilsc = lsc_bind(name, sym, ast->r);
        if (tilsc < 0)
            return tilsc;
        ilsc += tilsc;
    }
    if (ast->l) {
        tilsc = lsc_bind(name, sym, ast->l);
        if (tilsc < 0)
            return tilsc;
        ilsc += tilsc;
    }

    if ((ast->op == 'S') && (strcmp(name, ast->sym_name) == 0)) {
        ILOGD(depth, "Local-scope symbol [%s] binds at AST->r [%p]\n", name,
              ast);
        ast->op = 's';
        ASSERT(sym->ast->op == 'E'); /* Empty symbol */
        sym->ast->sym_name =
            name; /* Symbol already has a name, but connections in AST is henceforth lost
                                       This will indicate what it was resolved to (used for debugging) */
        ast->r = sym->ast;
        return 1;
    }

    DEPTH_DEC();
    ILOGD(depth, "r< [%p]\n", ast);
    ;
    return ilsc;
}
