#ifndef ast_h
#define ast_h

#include "common.h"
#include "comp/mycalc_yacc.h"
#include "comp/sym.h"
#include <stdint.h>

struct YYLTYPE;

struct ast {
    Float y;
    union {
        intptr_t op; /* If Symbol: S=Global, s=scoped */
        char opc; /* Ditto as character. Debugger can show */
    };
    union {
        char subop;
        bool cond; /* Conditional l/r used by BRANCH */
        struct fun *fun;
        Float *mem;
        char *sym_name;
        enum symcode symcode;
    };
    struct ast *l;
    struct ast *r;
};

void ast_setloc(struct YYLTYPE *_yylloc);
void ast_error(const char *str, ...);
struct ast *ast_new(int op, struct ast *l, struct ast *r);
struct ast *ast_newmultiop(int op, char subop, struct ast *l, struct ast *r);
struct ast *ast_newflow(int op, struct ast *l, struct ast *r1, struct ast *r2);
struct ast *ast_newnumber(Float number);
struct ast *ast_newregister(Float *number);
struct ast *ast_newreference(Float *number);
struct ast *ast_newfunction(struct fun fun, struct ast *list);
struct ast *ast_newufun(const char *name, struct ast *argv);
struct ast *ast_newsymbol(const char *name);
struct ast *ast_list_append(struct ast *ast, struct ast *r);
Float ast_eval(struct ast *a);
Float ast_runfun(struct ast *fun, struct ast *argv);
void ast_free(struct ast *ast);

#endif //ast_h
