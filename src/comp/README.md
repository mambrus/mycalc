# Module `COMP`

This is the core of `MYCALC`. IOW everything that matters, i.e. except

* project glue
* `userio`

## The content

* Parser (lex-file)
* Grammar (yacc-file)
* AST implementation
* Symbol management
* Misc
    * helpers
    * math-wrappers
    * debug-aid
