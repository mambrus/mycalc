#include "sym.h"
#include "ast.h"
#include <liblog/assure.h>
#include <liblog/log.h>
#include <mlist.h>
#include <stdbool.h>
#include "config.h"

#ifdef MYCALC_DEBUG_SYM
#    define PRINTF(...) LOGI(__VA_ARGS__)
extern int depth;
#    define DEPTH_INC() (++depth)
#    define DEPTH_DEC() (depth--)
#else //MYCALC_DEBUG_SYM
#    define PRINTF(...)
#    define DEPTH_INC(...)
#    define DEPTH_DEC(...)

#    undef ILOGV
#    undef ILOGD
#    undef ILOGI
#    undef ILOGW
#    undef ILOGE
#    define ILOGV(N, ...) LOGV(__VA_ARGS__)
#    define ILOGD(N, ...) LOGD(__VA_ARGS__)
#    define ILOGI(N, ...) LOGI(__VA_ARGS__)
#    define ILOGW(N, ...) LOGW(__VA_ARGS__)
#    define ILOGE(N, ...) LOGE(__VA_ARGS__)

#endif //MYCALC_DEBUG_SYM

extern struct ast bad_ast;

/*
 *  Global scope bind. For each name still un-bound, search in global
 *  symbol-table. If not found, return bad-AST in bast
 */
int gsc_bind(struct ast *ast, struct ast **bast)
{
    int tigsc = 0;
    int igsc = 0;

    int sym_xref()
    {
        struct sym *sym;
        ASSURE(ast->l == NULL);

        ILOGD(depth, "Looking for g:symbol [%s]\n", ast->sym_name);
        sym = sym_glookup(ast->sym_name);

        if (sym == NULL) {
            ILOGW(depth, "Symbol [%s] not found\n", ast->sym_name);
            *bast = ast;
            return SYM_UNKNOWN;
        }

        if (sym->ast->op == 'F') {
            ILOGE(depth, "Symbol (S) [%s] mixup: %c\n", sym->ast->sym_name,
                  sym->ast->op);
            *bast = ast;
            return SYM_MIXUP;
        }

        ILOGD(depth, "Bind (S) [%s]: [%p] <--> [%p]\n", ast->sym_name, ast,
              sym->ast);
        ast->l = sym->ast; /* Global THL, local THR */

        return 1; /* Found and bound at this level */
    }

    /* Note: function AST-symbols differ from non-func symbols in that they
     * have one more levels where LHS is the full definition including
     * locally scoped sub-symbols */
#ifdef NOTNOW
    int fun_xref()
    {
        struct sym *sym;
        ASSURE(ast->l == NULL);

        ILOGD(depth, "Looking for g:function [%s]\n", ast->sym_name);
        sym = sym_glookup(ast->sym_name);

        if (sym == NULL) {
            ILOGW(depth, "Function [%s] not found [%p]\n", ast->sym_name);
            *bast = ast;
            return SYM_UNKNOWN;
        }

        if (sym->ast->op != 'F') {
            ILOGE(depth, "Symbol (F) [%s] mixup: %c\n", sym->ast->sym_name,
                  sym->ast->op);
            *bast = ast;
            return SYM_MIXUP;
        }

        ILOGD(depth, "Bind (F) [%s]: [%p] <--> [%p]\n", ast->sym_name, ast,
              sym->ast);
        ast->l = sym->ast; /* Global THL, local THR */

        return 1; /* Found and bound at this level */
    }
#endif

    ILOGD(depth, "R> [%p]\n", ast);
    ;
    DEPTH_INC();

    if (ast->r) {
        tigsc = gsc_bind(ast->r, bast);
        if (tigsc < 0)
            return tigsc;
        igsc += tigsc;
    }
    if (ast->l) {
        tigsc = gsc_bind(ast->l, bast);
        if (tigsc < 0)
            return tigsc;
        igsc += tigsc;
    }

    switch (ast->op) {
    case 'S':
        igsc = sym_xref();
        break;
        //        case 'F':
        //            igsc = fun_xref();
        //            break;
    default:
        igsc = 0;
    }

    DEPTH_DEC();
    ILOGD(depth, "R< [%p]\n", ast);
    ;
    return igsc;
}

/*
 *  Resolve symbols in AST against global symbols
 *
 *  Return bad_ast on first symbol not resolved otherwise return the
 *  resolved AST.
 */
struct ast *sym_resolve(struct ast *ast)
{
    int igsc = 0;
    struct ast *bast = NULL;

    ILOGD(depth, "Symbols resolve is ast [%p]\n", ast);
    igsc = gsc_bind(ast, &bast);
    ILOGD(depth, "%d symbols resolved in ast [%p]\n", igsc, ast);

    if (igsc < 0) {
        bad_ast.l = bast;
        bad_ast.symcode = igsc;
        return &bad_ast;
    }
    return ast;
}
