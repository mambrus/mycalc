%x GR_CMD
%x HELP
%x QUIT

%{
#include "config.h"
#include "mycalc_yacc.h"
#include "comp/mathwrap.h"
#include "lex_location.h"
#include "lex_debug.h"
#include "userio/io.h"
#include "common.h"
#include "misc.h"
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>
#include <liblog/log.h>
#include <sys/types.h>
#include <signal.h>
#include <regstring.h>

#ifdef MYCALC_USE_READLINE
#  define YY_INPUT(buf,result,max_size) result = getinput(buf, max_size);
#endif

#ifdef MYCALC_DEBUG_LEXER
static int scan_cntr = 0;
#define _GNU_SOURCE         /* See feature_test_macros(7) */
#define __USE_GNU
#include <dlfcn.h>
static Dl_info dl_info;     /* Last lookup DL_info */
#endif

/* Bison compatibility */
extern YYSTYPE yylval;
/* Registers */
Float reg['Z' - 'A'] = {0.0L};
Float *regof(char letter);
/* Modality for trigonometric functions */
static char peek_char;
static char *opt_str = NULL;

/* Set yylval and return agreed token */
#define FUN_F_F( F )  \
    yylval->fun.f1=F; \
    yylval->fun.kind="FF"; \
    RETURN(FUNC);

#define FUN_F_FF( F ) \
    yylval->fun.f2=F; \
    yylval->fun.kind="FFF"; \
    RETURN(FUNC);

#define FUN_F_FFF( F ) \
    yylval->fun.f3=F; \
    yylval->fun.kind="FFFF"; \
    RETURN(FUNC);

#define FUN_F_FFFF( F ) \
    yylval->fun.f4=F; \
    yylval->fun.kind="FFFFF"; \
    RETURN(FUNC);

#define FUN_I_F( F ) \
    yylval->fun.p=(void*)F; /* We need only the address */ \
    yylval->fun.kind="IF"; \
    RETURN(FUNC);

/* Select trigo function and return token
   Note: This macro applies ONLY for "Float fun(Float)" functions
   */
#define FTRIGO_R( F ) \
    FUN_F_F(FTRIGO( F ));

#define CONSTANT_R( F ) yylval->floating=F; return FLOAT
#define READ_FLOAT_R( F ) sscanf(yytext, F, &yylval->floating); RETURN(FLOAT);
#define READ_INT_R( F )   sscanf(yytext, F, &yylval->integer); RETURN( INTEGER);

static long double max3(long double a, long double b, long double c);
static long double max4(long double a, long double b, long double c, long double d);
static void start_lex(void);
const char *dl_info_fun(void *f); /* Run once for each lex:ed function */

%}

%option yylineno
%option noyywrap
%option bison-bridge
%option bison-locations
%option stack

%%
%{
  /* At each yylex invocation, execute mycalc's start_lex() */
  start_lex();
%}

 /* Handling the two possible minuses */

-                                   {
                                        PRINTF("MINUS\n");
                                        peek_char=input();
                                        unput(peek_char);
                                        if (peek_char==' ' || peek_char=='\t') {
                                            RETURN('-');
                                        }
                                        PRINTF("Returning unary minus\n");
                                        RETURN(UNARY_MINUS);
                                    }

 /* Well-known constants  (scan before numbers as symbols may contain digits) */

SQRT1_2D|1\/sqrt\(2\)               CONSTANT_R(M_SQRT1_2);    /* 1/sqr(2) */
SQRT2D|sqrt\(2\)                    CONSTANT_R(M_SQRT2);      /* sqr(2) */
2_SQRTPID|2\/sqr\(pi\)              CONSTANT_R(M_2_SQRTPI);   /* 2/sqr(pi)  */
2_PI|2\/p                           CONSTANT_R(M_2_PI);       /* 2/pi */
1_PI|1\/pi                          CONSTANT_R(M_1_PI);       /* 1/pi */
PI_4|pi\/4                          CONSTANT_R(M_PI_4);       /* pi/4 */
PI_2|pi\/2                          CONSTANT_R(M_PI_2);       /* pi/2 */
PI|pi                               CONSTANT_R(M_PI);         /*  pi */
ln10                                CONSTANT_R(M_LN10);       /* loge10 */
ln2                                 CONSTANT_R(M_LN2);        /* loge2 */
log10e                              CONSTANT_R(M_LOG10E);     /* log10e */
log2e                               CONSTANT_R(M_LOG2E);      /* log2e */
e                                   CONSTANT_R(M_E);          /* e */
(true|false)                        {                         /* boolean */
                                        yylval->integer =  yytext[0] == 't' ? true : false;
                                        RETURN(INTEGER);
                                    };

 /* Numbers */
[01]+.[01]+[B]-?[01]+               {
                                        yylval->floating = read_binary_f(yytext);
                                        RETURN(FLOAT);
                                    }
[01]+[B]-?[01]+                     {
                                        yylval->floating = read_binary_i(yytext);
                                        RETURN(FLOAT);
                                    }
[0-7]+.[0-7]+[Oo]-?[0-7]+           {
                                        yylval->floating = read_octal_f(yytext);
                                        RETURN(FLOAT);
                                    }
[0-7]+[Oo]-?[0-7]+                  {
                                        yylval->floating = read_octal_i(yytext);
                                        RETURN(FLOAT);
                                    }
[[:xdigit:]]+.[[:xdigit:]]+[Hh][-+]?[[:xdigit:]]+                           {
                                        yylval->floating = read_hex_f(yytext);
                                        RETURN(FLOAT);
                                    }
[[:xdigit:]]+[Hh][-+]?[[:xdigit:]]+                                         {
                                        yylval->floating = read_hex_i(yytext);
                                        RETURN(FLOAT);
                                    }
0c[0-7]+                            READ_INT_R("0c%Lo");      /* Integer OCT */
0C[0-7]+                            READ_INT_R("0C%Lo");      /*    --"--    */
[[:digit:]]+                        READ_INT_R("%Ld");        /* Indeger DEC */
0x[[:xdigit:]]+                     READ_INT_R("0x%Lx");      /* Integer HEX */
0X[[:xdigit:]]+                     READ_INT_R("0X%Lx");      /*    --"--    */
0[bB][01]+                          {                         /* Integer BIN */
                                        yylval->integer = bins2int(&yytext[2]);
                                        RETURN(INTEGER);
                                    };
(?ix:
    /* Full feathered float */
   [[:digit:]]+\.[[:digit:]]+
   ([eE]-?[[:digit:]]+)?)           READ_FLOAT_R("%Lf");
[[:digit:]]+[eE]-?[[:digit:]]+      READ_FLOAT_R("%Lf");

(?ix:
    /* Full feathered hex-float */
   0x[[:xdigit:]]+\.[[:xdigit:]]+
   ([pP]-?[[:xdigit:]]+)?)          READ_FLOAT_R("%La");
0x[[:xdigit:]]+[pP]-?[[:xdigit:]]+  READ_FLOAT_R("%La");

 /* Math functions */
<INITIAL>deg                        FUN_F_F(_deg);
<INITIAL>rad                        FUN_F_F(_rad);

 /* -- Test-functions */
max3                                FUN_F_FFF(max3);
max4                                FUN_F_FFFF(max4);

 /* -- Trigonometric functions (trigo-mode obediant) */
acos                                FTRIGO_R(acosl);
acosh                               FTRIGO_R(acoshl);
asin                                FTRIGO_R(asinl);
asinh                               FTRIGO_R(asinhl);
atan                                FTRIGO_R(atanl);
atanh                               FTRIGO_R(atanhl);
cos                                 FTRIGO_R(cosl);
cosh                                FTRIGO_R(coshl);
sin                                 FTRIGO_R(sinl);
sinh                                FTRIGO_R(sinhl);
tan                                 FTRIGO_R(tanl);
tanh                                FTRIGO_R(tanhl);

cbrt                                FUN_F_F(cbrtl);
ceil                                FUN_F_F(ceill);
erfc                                FUN_F_F(erfcl);
erf                                 FUN_F_F(erfl);
exp2                                FUN_F_F(exp2l);
exp                                 FUN_F_F(expl);
expm1                               FUN_F_F(expm1l);
fabs|abs                            FUN_F_F(fabsl);
floor                               FUN_F_F(floorl);
lgamma                              FUN_F_F(lgammal);
log10                               FUN_F_F(log10l);
log1p                               FUN_F_F(log1pl);
log2                                FUN_F_F(log2l);
logb                                FUN_F_F(logbl);
log                                 FUN_F_F(logl);
nearbyint                           FUN_F_F(nearbyintl);
sqrt                                FUN_F_F(sqrtl);
tgamma                              FUN_F_F(tgammal);
trunc                               FUN_F_F(truncl);
   /* Type: Int_Float */
rint                                FUN_I_F(llrintl);
round                               FUN_I_F(llroundl);

   /* Convenience */
ln                                  FUN_F_F(_lnl);
 /* Type Float_Float_Float */
 /*atan2                             FUN_F_FF(atan2l; RETURN(FUNC)); */
copysign                            FUN_F_FF(copysignl);
dim                                 FUN_F_FF(fdiml);
max                                 FUN_F_FF(fmaxl);
min                                 FUN_F_FF(fminl);
mod                                 FUN_F_FF(fmodl);
hypot                               FUN_F_FF(hypotl);
nextafter                           FUN_F_FF(nextafterl);
nexttoward                          FUN_F_FF(nexttowardl);
pow                                 FUN_F_FF(powl);
remainder                           FUN_F_FF(remainderl);

 /* -- Multi letter operators -- */

 /* Assign-by: ASSIGNBY as implicit token, variant as "subop" from 1:st letter */
[+\-*/%]=                           {
                                        yylval->letter=yytext[0];
                                        RETURN(ASSIGNBY);
                                    }
 /* LE, GE multi character (2x) where implicit token (variant) as "subop" from 1:st letter */
[><]=                               {
                                        yylval->letter=yytext[0];
                                        RETURN(COMPARISON);
                                    }
 /* Multi character (2x) EQUALITY where implicit token (variant) as "subop" from 1:st letter */
[=!]=                               {
                                        yylval->letter=yytext[0];
                                        RETURN(EQUALITY);
                                    }
 /* Multi character (2x) EQUALITY where implicit token (variant) as "subop" from 1:st letter */
([&][&]|[|][|])                     {
                                        yylval->letter=yytext[0];
                                        RETURN(LOGICAL);
                                    }
 /* Bitwise ditto, i.e. <<= >>= */
 /* variant as "subop" from 1:st letter which is enough to make it distinct */
([<][<]|[>][>]|[|]|[&])=            {
                                        yylval->letter=yytext[0];
                                        RETURN(ASSIGNBY);
                                    }
 /* Bitwise multi-letter operators: BITSHIFT as implicit token, */
 /* variant as "subop" from 2:st letter*/
([<][<]|[>][>])                     {
                                        yylval->letter=yytext[1];
                                        RETURN(BITSHIFT);
                                    }
~                                   {
                                        RETURN(UNARY_ONECOMPL);
                                    }
!                                   {
                                        RETURN(UNARY_NOT);
                                    }

 /* NON-grammar temporary setting */
 /*-------------------------------*/
^\\[^[:space:]]+                    {
                                        during_line_begin(yytext);
                                    }
 /* Non-grammar commands: */
 /*-----------------------*/
^help                               {
                                        yy_push_state(HELP);
                                        STRASGN(opt_str) = strdup("");
                                    }
<HELP>[^[:space:]]+                 {
                                        if (strlen(opt_str) != 0)
                                            strappnd(&opt_str," ");
                                        strappnd(&opt_str,yytext);
                                    }
<HELP>\n                            {
                                        yy_pop_state();
                                        help(opt_str);
                                    }
^quit                               {
                                        yy_push_state(QUIT);
                                    }
<QUIT>\n                            {
                                        yy_pop_state();
                                        /* Let ^C signal handler quit */
                                        kill(0, SIGINT);
                                    }
 /* Grammar commands. Some of these really belong to, if not grammar, then
  * at least AST-eval , like trigonometry modality (perhaps not even that).
  * But most others don't, like the old print formatters.

  * TODO: Find a nicer way that doesn't involve tokenisation for the BNF to
  * handle 
  */
^set                                {
                                        yy_push_state(GR_CMD);
                                        RETURN(SET);
                                    }
^get                                {
                                        yy_push_state(GR_CMD);
                                        RETURN(GET);
                                    }
<GR_CMD>trig                        {RETURN(S_TRIG);}
<GR_CMD>(deg|DEG)                   {RETURN(S_DEG);}
<GR_CMD>(rad|RAD)                   {RETURN(S_RAD);}
<GR_CMD>format                      {RETURN(S_FORMAT);}
<GR_CMD>scimode                     {RETURN(S_SCIMODE);}
<GR_CMD>(sci|SCI)                   {RETURN(S_SCI);}
<GR_CMD>(nrm|NRM)                   {RETURN(S_NRM);}
<GR_CMD>register                    {RETURN(S_RREG);}
<GR_CMD>loglevel                    {RETURN(S_LOGLEVEL);}
 /* Any word i.e. symbol-name, setting-name etc */
<GR_CMD>[[:alnum:].]+               {
                                        yylval->string=strdup(yytext);
                                        RETURN(WORD);
                                    }
<GR_CMD>\n                               {
                                        yy_pop_state();
                                        RETURN(EOL);
                                    }
 /* Registers */
[A-Z]                               {
                                        yylval->memory=regof(yytext[0]);
                                        RETURN(REGISTER);
                                    }
 /* keywords */
let                                 RETURN(LET);
if                                  RETURN(IF);
then                                RETURN(THEN);
else                                RETURN(ELSE);
while                               RETURN(WHILE);
do                                  RETURN(DO);

<INITIAL>[a-zA-Z][[:alnum:]_]*      {
                                        yylval->string=strdup(yytext);
                                        RETURN(WORD);
                                    }
<INITIAL>\n                         {
                                        #ifdef MYCALC_DEBUG_LEXER
                                        scan_cntr=0;
                                        #endif
                                        RETURN(EOL);
                                    }
[ \t]+                              {;}
.                                   RETURN(yytext[0]);

%%

/* Scanner modality */
#include "misc.h"
#include "userio/io.h"
int set_trig_deg()
{
    mycalc.settings->trig_mode = DEG;
    return print_trig();
}

int set_trig_rad()
{
    mycalc.settings->trig_mode = RAD;
    return print_trig();
}

int print_trig()
{
    fprintf(ofrmt->file, "\r");

    if (mycalc.settings->trig_mode == DEG)
        fprintf(ofrmt->file, "DEG\n");
    else
        fprintf(ofrmt->file, "RAD\n");
    return 1;
}

int print_scimode()
{
    fprintf(ofrmt->file, "\r");

    if (ofrmt->scimode == SCI)
        fprintf(ofrmt->file, "SCI\n");
    else
        fprintf(ofrmt->file, "NRM\n");
    return 1;
}

/* Register reference */
Float *regof(char letter)
{
    assert((letter >= 'A') && (letter <= 'Z'));
    return &reg[letter - 'A'];
    yy_top_state();             // <-- Just to silence warnings
}

static char *progname;
void set_myprogname(const char *name)
{
    progname = strdup(name);
}

void yyerror(YYLTYPE * yylloc, const char *str, ...)
{
    FILE *out = stderr;
    va_list args;

    va_start(args, str);

    fprintf(out, "[%s] at ", progname);
    LOCATION_PRINT(out, *yylloc);
    fprintf(out, ": ");
    vfprintf(out, str, args);
    fprintf(out, "\n");
}

/* Test functions for validating argument passing */
long double max3(long double a, long double b, long double c)
{
    LOGD("max3 a:%.2Lf b:%.2Lf c:%.2Lf\n", a, b, c);

    return fmaxl(fmaxl(a, b), c);
}

long double max4(long double a, long double b, long double c, long double d)
{
    LOGD("max4 a:%.2Lf b:%.2Lf c:%.2Lf d:%.2Lf\n", a, b, c, d);

    return fmaxl(fmaxl(fmaxl(a, b), c), d);
}

/* Debug support */

/* Executed once each lex-start. Used as a hook. */
void start_lex()
{
    /* At each yylex invocation, mark the current position as the
       start of the next token.  */
    //LOCATION_STEP (yylloc_param);
    PRINTF("                          <lex: %d>\n", scan_cntr++);
}
#define SYMTAB( S ) \
    { &S , #S }

struct {
    void *addr;
    char *name;
} symtab[] = {
    SYMTAB(_acosl),
    SYMTAB(_acoshl),
    SYMTAB(_asinl),
    SYMTAB(_asinhl),
    SYMTAB(_atanl),
    SYMTAB(_atanhl),
    SYMTAB(_cosl),
    SYMTAB(_coshl),
    SYMTAB(_sinl),
    SYMTAB(_sinhl),
    SYMTAB(_tanl),
    SYMTAB(_tanhl),
    SYMTAB(_lnl),
    SYMTAB(_deg),
    SYMTAB(_rad),
    SYMTAB(max3),
    SYMTAB(max4),
};

#ifdef MYCALC_DEBUG_LEXER
/* Run each time a function if tokenized */
const char *dl_info_fun(void *f)
{
#ifdef MYCALC_USE_DL
    if (dladdr(f, &dl_info) == 0) {
        LOGW("Address %p dladdr() failure\n", f);
    }
    /* Is a GNU-built in. */
    if (dl_info.dli_sname) {
        return dl_info.dli_sname;
    }
#endif
    /* Not a GNU-built in. Try mycalc built-in extensions */
    for (int i = 0; elements(symtab); i++) {
        if (symtab[i].addr == f)
            return symtab[i].name;
    }
    LOGW("Address %p has no known symbol\n", f);
    return NULL;
}
#endif
