#define _GNU_SOURCE
#include "ast.h"
#include "userio/io.h"
#include "mycalc_yacc.h"
#include <math.h>
#include <assert.h>
#include <stddef.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <liblog/assure.h>
#include <liblog/log.h>
#include <mlist.h>
#include "config.h"
#include "ast_debug.h"

static YYLTYPE *yylloc = NULL;

static struct YYLTYPE no_parser_yylloc = { .first_line = 0,
                                           .first_column = 1,
                                           .last_line = 1,
                                           .last_column = 0xC0DEDEAD };

void ast_setloc(struct YYLTYPE *_yylloc)
{
    yylloc = _yylloc;
}

void ast_error(const char *str, ...)
{
    struct YYLTYPE *loc = yylloc ? yylloc : &no_parser_yylloc;
    va_list args;

    va_start(args, str);

    yyerror(loc, str, args);
}

#ifdef MYCALC_DEBUG_AST
char *ast_tokname(int op)
{
    static char *name = NULL;

    if (name)
        free(name);

    if (op > 255) {
        int i;
        i = op - 255;
        name = strdup((char *)yyname(i));
    } else {
        asprintf(&name, "%c", (char)op);
    }
    return name;
}
#else
char *ast_tokname(int op)
{
    static char *name = NULL;

    if (name)
        free(name);
    asprintf(&name, "%c", (char)op);

    return name;
}
#endif

struct ast *ast_new(int op, struct ast *l, struct ast *r)
{
    ILOGI(eval_depth, "AST: ast_new(%d, ...) [%s]", op, ast_tokname(op));

    struct ast *_ast = NULL;
    _ast = calloc(1, sizeof(struct ast));
    if (_ast == NULL) {
        ast_error("Out of memory");
    }
    _ast->l = l;
    _ast->r = r;
    _ast->op = op;
    return _ast;
}

struct ast *ast_newmultiop(int op, char subop, struct ast *l, struct ast *r)
{
    ILOGI(eval_depth, "AST: ast_newmultiop(%d, %c, ...) [%s, %d]", op, subop,
          ast_tokname(op), subop);
    struct ast *_ast = ast_new(op, l, r);
    if (_ast)
        _ast->subop = subop;
    return _ast;
}

/*
 * 2x AST generator for "conditionals" and "flow"
 *
 * Two AST:s always created, one top level and one containing execution
 * branching path. Top-level AST returned
 *
 * Second-level AST:s "left" is assosiated with truth, the right with
 * non-truth (if existing). I.e. non-truth AST can be left dangling but if
 * existing, 1 lvl AST's subop will be maked 'E' (as in "else").
 */
struct ast *ast_newflow(int op, struct ast *l, struct ast *r2l, struct ast *r2r)
{
    ILOGI(eval_depth, "1:st of 2x AST: ast_newflow(%d, %c, ...) [%s, %d]", op,
          r2r ? "E" : "", ast_tokname(op), r2r ? 1 : 0);
    ILOGI(eval_depth, "2:nd lvl of 2x AST: ast_newflow(%d, %p, ...) [%s, %p]",
          BRANCH, r2r, ast_tokname(BRANCH), r2r);

    struct ast *_ast2 = ast_new(BRANCH, r2l, NULL);
    if (!_ast2) {
        return NULL;
    }
    /* Create top-level AST chained to the second-level at RHS */
    struct ast *_ast1 = ast_new(op, l, _ast2);
    if (!_ast1) {
        free(_ast2);
        return NULL;
    }

    /* If "else-capable", tag 1:st lvl AST and ser RHS of 2:nd-lvl AST*/
    if (r2r) {
        _ast1->subop = 'E';
        _ast2->r = r2r;
    }
    return _ast1;
}

struct ast *ast_list_append(struct ast *ast, struct ast *r)
{
    struct ast *_ast =
        ast_new(',', NULL, r); /* (->l == NULL) is a list sentinel */

    struct ast *leaf(struct ast * a)
    {
        ASSERT(a->op == ',');
        if (a->l)
            return leaf(a->l);

        return a;
    }

    /* Traverse the tree until leaf is found and append new node there */
    leaf(ast)->l = _ast;

    /* Return same, now appended, list-ast */
    return ast;
}

/* AST as end-point (leaf) */
struct ast *ast_newnumber(Float number)
{
    struct ast *_ast = ast_new('K', NULL, NULL); /* K as in "Konstant" */
    _ast->y = number;
    return _ast;
}

struct ast *ast_newreference(Float *number)
{
    struct ast *_ast = ast_new('M', NULL, NULL); /* M as in "Memory" */
    _ast->mem = number;
    return _ast;
}

struct ast *ast_newregister(Float *number)
{
    struct ast *_ast =
        ast_new('R', NULL, NULL); /* R as in "Register" (no not free) */
    _ast->mem = number;
    return _ast;
}

/*
 * Creates a new place-holder symbol
 */
struct ast *ast_newsymbol(const char *name)
{
    struct ast *_ast = ast_new('S', NULL, NULL); /* S as in "Symbol",
                                                       Capital: unbound or Global */
    _ast->sym_name = strdup(name);
    return _ast;
}

struct ast *ast_newfunction(struct fun fun, struct ast *argv)
{
    struct fun *_fun = NULL;
    struct ast *_ast = ast_new('f', argv, NULL);

    _fun = malloc(sizeof(struct fun));
    if (_fun == NULL) {
        ast_error("Out of memory");
    }
    memcpy(_fun, &fun, sizeof(struct fun));
    _ast->fun = _fun;

    return _ast;
}

/* Walk the tree TTR & TTL and free it recursively */
void ast_free(struct ast *ast)
{
    prettyp_enter(free_depth, ast, 'N');
    FREE_DEPTH(++);

    ASSURE(ast);
    if (ast->r) {
        prettyp_enter(free_depth, ast->r, 'R');
        ast_free(ast->r);
        free(ast->r);
    }
    if (ast->l) {
        prettyp_enter(free_depth, ast->l, 'L');
        ast_free(ast->l);
        free(ast->r);
    }
    if ((ast->op == 'S') || (ast->op == 's')) {
        if (ast->sym_name)
            free(ast->sym_name);
    }
    free(ast);

    FREE_DEPTH(--);
    prettyp_enter(free_depth, ast, 'N');
}
