/* misc.h
 *
 * Settings functions. Implementation may be in different files due to
 * hard-coded variables/functions they need to access*/
#ifndef misc_h
#define misc_h

#include "common.h"
#include <stddef.h>

struct YYLTYPE;

int set_trig_deg(void);
int set_trig_rad(void);
int print_trig(void);
int print_scimode(void);
void set_myprogname(const char *);
void set_result_register(Float *);
char get_result_register();
void set_result(Float);
Float get_result();
Float *regof(char letter);
void help(char *topics);
void yyerror(struct YYLTYPE *yylloc, const char *str, ...);

#endif //misc_h
