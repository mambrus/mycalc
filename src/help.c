/***************************************************************************
 *   Copyright (C) 2022 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* Interactive help */

#include "misc.h"
#include <liblog/log.h>
#include <liblog/assure.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>

/*
 * Convert wait-status into a mask where each bit is defined.
 */
static unsigned wait_mask(int stat_val)
{
    unsigned rc = 0;

    rc |= (WIFEXITED(stat_val) << 0);
    rc |= (WEXITSTATUS(stat_val) << 1);
    rc |= (WIFSIGNALED(stat_val) << 2);
    rc |= (WTERMSIG(stat_val) << 3);
    rc |= (WIFSTOPPED(stat_val) << 4);
    rc |= (WSTOPSIG(stat_val) << 5);
    rc |= (WIFCONTINUED(stat_val) << 6);

    return rc;
}

/*
 *  Open manpage in a forked process
 */
static void help_man()
{
    int childpid, wpid, status;
    char *const argv[] = { "/usr/bin/man", "mycalc", NULL };
    ASSURE((childpid = fork()) >= 0);

    if (childpid == 0) {
        /* Child excutes this */
        LOGD("CHILD: isatty detect part 1 {0:%d} {1:%d} {2:%d}\n", isatty(0),
             isatty(1), isatty(2));
        LOGD("CHILD: Will execute:\n");
        for (int i = 0; argv[i]; i++) {
            LOGI("   [%s]\n", argv[i]);
        }

        execv("/usr/bin/man", argv);
        /* Should never execute */
        LOGE("exec error:" __FILE__ " +" STR(__LINE__) " %s", strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* Parent executes this */
    LOGD("PARENT: Will wait for child %d to exit\n", childpid);

    do {
        //wpid=waitpid( /*childpid*/ /*0*/ -1, &status, WUNTRACED );
        wpid = waitpid(childpid, &status, WUNTRACED);
        LOGI("CHILD: [%d] Has new status: %d, wait-mask: 0x%02x\n", wpid,
             status, wait_mask(status));
        ASSURE(wpid >= 0);
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    LOGD("man_help finished waiting for child %d and is exiting\n", childpid);
}

void help(char *args)
{
    UNUSED(args);
    LOGD("Interactive help invoked with argument-string: %s", args);

    if (strlen(args) == 0) {
        LOGD("Forking man-page");
        help_man();
    } else {
        LOGE("No topic-help yet (TBD)");
    }
}
