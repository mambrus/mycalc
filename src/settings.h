/***************************************************************************
 *   Copyright (C) 2020 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef settings_h
#define settings_h

#include <stdbool.h>
#include <stdio.h>

/***************************************************************************
 * Numerical output formatter settings
 ***************************************************************************/
#define OF_MAX_GROUPS 64
struct grouping;

struct oformat {
    FILE *file; /* Where to send output */

    char *format; /* Format-string for native printf */
    enum { NRM, SCI } scimode; /* Exponential base 10 in SI-units */
    struct grouping *grouping; /* Post-formatter group settings. */
};

struct settings {
    struct oformat *oformat;
    enum { DEG, RAD } trig_mode;
};

struct settings *settings_p(void);
int set_ofprintf_format(char *format);
int set_printf_post(char *formatter);
char *get_ofprintf_format(void);

/***************************************************************************
 *
 ***************************************************************************/

#endif /* settings_h */
