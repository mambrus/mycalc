/***************************************************************************
 *   Copyright (C) 2020 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/*
 * Parse single options for mycalc
 */
#include <stdio.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <getopt.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <errno.h>

#include <liblog/assure.h>
#include <liblog/log.h>
#include "opts.h"
#include "doc.h"
#include "settings.h"
#include "userio/io.h"

#define not_req 0
#define mandatory 1
#define at_least 0
#define precisely -1

static struct req_opt *_req_opt(int val);

extern struct mycalc mycalc;

__attribute__((weak)) void print_doc()
{
    printf("Error: This text not supposed to be printed\n");
}

/* Parse a single option. */
static int opts_parse_opt(const char *cmd, int key, char *arg,
                          struct opts *opts)
{
    switch (key) {
    case 'l':
        _req_opt('v')->cnt++;
        if (arg[0] >= '0' && arg[0] <= '9')
            *opts->loglevel = arg ? atoi(arg) : 0;
        else {
            int ok;
            *opts->loglevel = str2loglevel(arg, &ok);
            if (!ok)
                LOGW("loglevel [%s] invalid. Falling back to default\n", arg);
        }
        break;
    case 'p':
        _req_opt('p')->cnt++;
        if (set_ofprintf_format(arg)) {
            fprintf(stderr, "mycalc: Bad option argument for -p: %s\n", arg);
            opts_help(stderr, HELP_TRY | HELP_EXIT_ERR);
        }
        break;
    case 'z':
        _req_opt('z')->cnt++;
        opts->daemon = 1;
        break;
    case 'u':
        _req_opt('u')->cnt++;
        opts_help(stdout, HELP_USAGE | HELP_EXIT);
        break;
    case 'h':
        _req_opt('h')->cnt++;
        opts_help(stdout, HELP_LONG | HELP_EXIT);
        break;
    case 'D':
        _req_opt('D')->cnt++;
        print_doc();
        mycalc_exit(0);
        break;
    case 'H':
        _req_opt('H')->cnt++;
        if (history_file(arg) == NULL) {
            fprintf(stderr, "mycalc: Bad filename for history-file -H: %s\n",
                    arg);
            opts_help(stderr, HELP_TRY | HELP_EXIT_ERR);
        }
        break;
    case '?':
        /* getopt_long already printed an error message. */
        opts_help(stderr, HELP_TRY | HELP_EXIT_ERR);
        break;
    case ':':
        /* getopt_long already printed an error message. */
        fprintf(stderr, "%s: option `-%c' requires an argument\n", cmd, optopt);
        opts_help(stderr, HELP_TRY | HELP_EXIT_ERR);
        break;
    case 'v':
        _req_opt('v')->cnt++;
        opts_help(stdout, HELP_VERSION | HELP_EXIT);
        break;
    default:
        fprintf(stderr, "mycalc: unrecognized option '-%c'\n", (char)key);
        opts_help(stderr, HELP_TRY | HELP_EXIT_ERR);
        break;
    }
    return OPT_OK;
}

/* GNU log-opt structure. For more info, see:
 * http://www.gnu.org/software/libc/manual/html_node/Getopt-Long-Options.html
 * */
static struct option long_options[] = {
    /* clang-format off */
    {"loglevel",       required_argument,  0,  'l'},
    {"format",         required_argument,  0,  'p'},

    {"daemon",         no_argument,        0,  'z'},
    {"history",        required_argument,  0,  'H'},
    //{"device",         required_argument,  0,  'd'},
    {"documentation",  no_argument,        0,  'D'},
    {"help",           no_argument,        0,  'h'},
    {"usage",          no_argument,        0,  'u'},
    {"version",        no_argument,        0,  'v'},
    {0,                0,                  0,  0}
    /* clang-format on */
};

/* Additional structure to keep track of mandatory options. Each entry-index
 * should correspond exactly to long_options. Therefore keep tables close.
 */
static struct req_opt req_opts[] = {
    /* clang-format off */
    {'l',  not_req,    at_least,   0},
    {'p',  not_req,    precisely,  0},

    {'z',  not_req,    precisely,  0},
    {'H',  not_req,    precisely,  0},
    //{'d',  mandatory,  at_least,   0},
    {'D',  not_req,    at_least,   0},
    {'h',  not_req,    at_least,   0},
    {'u',  not_req,    at_least,   0},
    {'v',  not_req,    at_least,   0},
    {0,    0,          0,          0}
    /* clang-format on */
};

/* Access by key to the table above */
struct req_opt *_req_opt(int val)
{
    struct req_opt *ropt = req_opts;

    for (ropt = req_opts; ropt->val != 0; ropt++) {
        if (ropt->val == val)
            return ropt;
    }
    assert("req_opt reached sentinel" == 0);
    return ropt;
}

/* Access by key to a req_opt-list, generalized form */
struct req_opt *req_opt(int val, struct req_opt *ropt)
{
    for (ropt = req_opts; ropt->val != 0; ropt++) {
        if (ropt->val == val)
            return ropt;
    }
    assert("req_opt reached sentinel" == 0);
    return ropt;
}

/* Returns 0 on success, -1 on error */
static int become_daemon()
{
    switch (fork()) {
    case -1:
        return -1;
    case 0:
        break;
    default:
        _exit(EXIT_SUCCESS);
    }

    if (setsid() == -1)
        return -1;

    switch (fork()) { /* Ensure we are not session leader */
    case -1:
        return -1;
    case 0:
        break;
    default:
        _exit(EXIT_SUCCESS);
    }

    umask(0); /* Clear file mode creation mask */
    ASSERT(chdir("/") == 0); /* Change to root directory */

    return 0;
}

/*Initializes (resets) whatever is state-dependent here */
void opts_init()
{
    struct req_opt *ropt = req_opts;
    struct option *opt = long_options;

    for (ropt = req_opts, opt = long_options; opt->name != NULL;
         ropt++, opt++) {
        ropt->cnt = 0;
    }
}

/* Checks options to fulfill extended criteria. If successful appends a
 * deep-copy of the validation-structure. */
int opts_check(struct opts *opts)
{
    int resok = OPT_OK;
    struct option *opt = long_options;
    struct req_opt *ropt = req_opts;

    LOGD("Checking if mandatory options are set\n");

    for (ropt = req_opts, opt = long_options; opt->name != NULL;
         ropt++, opt++) {
        assert(opt->val == ropt->val);
        if (opt->flag)
            LOGV("%-15s %d %4d %c %2d %2d %2d\n", opt->name, opt->has_arg,
                 &opt->flag, opt->val, ropt->req, ropt->cnt, ropt->max);
        else
            LOGV("%-15s %d NULL %c %2d %2d %2d\n", opt->name, opt->has_arg,
                 opt->val, ropt->req, ropt->cnt, ropt->max);

        if (ropt->cnt < ropt->req) {
            LOGE("Mandatory option [\"%s\",'%c'] requirement failed. "
                 "Seen [%d] times, required [%d]",
                 opt->name, opt->val, ropt->cnt, ropt->req, ropt->max);
            resok = E_OPT_REQ;
        }
        if (ropt->max > 0 && ropt->cnt > ropt->max) {
            LOGE("Count of option [\"%s\",'%c'] requirement failed. "
                 "Seen [%d] times, permitted [%d]",
                 opt->name, opt->val, ropt->cnt, ropt->req, ropt->max);
            resok = E_OPT_REQ;
        }
        if (ropt->max == precisely && ropt->cnt != ropt->max && ropt->req > 0) {
            LOGE("Count of option [\"%s\",'%c'] requirement failed. "
                 "Seen [%d] times, expected [%d]",
                 opt->name, opt->val, ropt->cnt, ropt->req, ropt->max);
            resok = E_OPT_REQ;
        }
    }

    /* If all passed OK, make a deep-copy of check-struct  and attach it to
     * the opts-struct */
    {
        void *p;

        opts->req_opts = ((p = malloc(sizeof(req_opts))) ?
                              memcpy(p, &req_opts, sizeof(req_opts)) :
                              NULL);
    }

    return resok;
}

/* Parse options
 *
 * Returns number of options parsed.
 * Negative return-value indicate error.
 *
 * Affects input arguments pargc and argv so that they can be used for
 * cascading further options after a '--' marker, which by convention is
 * used and recognized by GNU opt-parsers as an end-of-arguments marker.
 * This way severer option parsers can be cascades after each other, as
 * long as they don't use the same options.
 *
 * You can use &argc and &argv as input arguments. But if these for some
 * reason need to be preserved, copies must be used instead.
 *
 * */
int opts_parse(int *pargc, char ***pargv, struct opts *opts)
{
    int rc, parsed_options = 0;
    int old_optind = optind;

    while (1) {
        int option_index = 0;
        int c = getopt_long(*pargc, *pargv, "g:b:p:d:l:zd:DuhvH:", long_options,
                            &option_index);
        /* Detect the end of the options. */
        if (c == -1)
            break;
        ASSURE_E((rc = opts_parse_opt((*pargv)[0], c, optarg, opts)) == OPT_OK,
                 return rc);
        parsed_options++;
    }

    /* Handle any remaining command line arguments (not options). */
    if (optind < *pargc) {
        if (strcmp("--", (*pargv)[optind - 1]) != 0) {
            perror(
                "mycalc: Too many arguments, \"mycalc\" takes only options.\n");
            fflush(stderr);
            opts_help(stderr, HELP_TRY | HELP_EXIT_ERR);
            return -1;
        }
    }
    /* Side-effect handling */
    *pargc = *pargc - optind + 1;
    *pargv = &(*pargv)[optind - 1];

    /* Restore parser global variable(s) */
    optind = old_optind;

    if (opts->daemon) {
        become_daemon();
    }

    return parsed_options;
}
