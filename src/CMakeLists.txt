
set(MYCALC_BASE_SRC
    settings.c
    main.c
    sig.c
    opts.c
    opts_help.c
    help.c
)
set(MYCALC_ALL_SRC
    ${MYCALC_BASE_SRC}
    doc.c
)
add_executable(${PROJECT_NAME}-base ${MYCALC_BASE_SRC})
add_executable(${PROJECT_NAME} ${MYCALC_ALL_SRC})

include_directories("${CMAKE_CURRENT_SOURCE_DIR}")
include_directories("${CMAKE_CURRENT_BINARY_DIR}")

# reuseable cmake macro for ASCIIDOCTOR
MACRO(ASCIIDOCTOR_FILE _filename)
    GET_FILENAME_COMPONENT(_basename ${_filename} NAME_WE)
    ADD_CUSTOM_COMMAND(
        OUTPUT  ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}-options
        COMMAND mkdir -p ${CMAKE_CURRENT_BINARY_DIR}/man
        COMMAND ${EXE_HELP2ROFF}
                ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}-options
        DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}-base
    )
    ADD_CUSTOM_COMMAND(
        OUTPUT  ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}.7.adoc
        COMMAND ${EXE_HSUBST} -s
                -e VERSION="${PROJECT_VERSION}"
                -e MYCALC_LOG_LEVEL="INFO"
                -e MYCALC_PRINTF_FORMAT="${MYCALC_PRINTF_FORMAT}"
                -e MYCALC_PRINTF_SCI_MODE="${MYCALC_PRINTF_SCI_MODE}"
                -e MYCALC_GROUP_HEX_DIGITS="${MYCALC_GROUP_HEX_DIGITS}"
                -e MYCALC_GROUP_HEX_SEP="${MYCALC_GROUP_HEX_SEP}"
                -e MYCALC_GROUP_HEX_PFX="${MYCALC_GROUP_HEX_PFX}"
                -e MYCALC_GROUP_HEX_ZPAD="${MYCALC_GROUP_HEX_ZPAD}"
                -e MYCALC_GROUP_DEC_DIGITS="${MYCALC_GROUP_DEC_DIGITS}"
                -e MYCALC_GROUP_DEC_SEP="${MYCALC_GROUP_DEC_SEP}"
                -e MYCALC_GROUP_DEC_PFX="${MYCALC_GROUP_DEC_PFX}"
                -e MYCALC_GROUP_DEC_ZPAD="${MYCALC_GROUP_DEC_ZPAD}"
                -e MYCALC_GROUP_OCT_DIGITS="${MYCALC_GROUP_OCT_DIGITS}"
                -e MYCALC_GROUP_OCT_SEP="${MYCALC_GROUP_OCT_SEP}"
                -e MYCALC_GROUP_OCT_PFX="${MYCALC_GROUP_OCT_PFX}"
                -e MYCALC_GROUP_OCT_ZPAD="${MYCALC_GROUP_OCT_ZPAD}"
                -e MYCALC_GROUP_BIN_DIGITS="${MYCALC_GROUP_BIN_DIGITS}"
                -e MYCALC_GROUP_BIN_SEP="${MYCALC_GROUP_BIN_SEP}"
                -e MYCALC_GROUP_BIN_PFX="${MYCALC_GROUP_BIN_PFX}"
                -e MYCALC_GROUP_BIN_ZPAD="${MYCALC_GROUP_BIN_ZPAD}"
                -i ${CMAKE_CURRENT_SOURCE_DIR}/${_filename}
                -o ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}.7.adoc
        DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}-options
        DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${_filename}
    )
    ADD_CUSTOM_COMMAND(
        OUTPUT  ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}.7
        COMMAND ${EXE_ASCIIDOCTOR}
            -b manpage -a stem -o${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}.7
            ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}.7.adoc
        DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}-options
        DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}.7.adoc
    )
    ADD_CUSTOM_COMMAND(
        OUTPUT  ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}.7.gz
        COMMAND gzip -kf ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}.7
        DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}.7
    )
    ADD_CUSTOM_COMMAND(
        OUTPUT  ${CMAKE_CURRENT_BINARY_DIR}/doc.c
        COMMAND ${EXE_DOCGEN}
            ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}.7
            ${CMAKE_CURRENT_BINARY_DIR}/doc.c
        DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}.7.gz
        DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/man/${_basename}.7
    )
ENDMACRO(ASCIIDOCTOR_FILE)

string(TIMESTAMP TODAY "%Y%m%d")
# reuseable cmake macro for siggen
MACRO(HSUBST_FILE _filename)
    GET_FILENAME_COMPONENT(_basename ${_filename} NAME_WE)
    ADD_CUSTOM_COMMAND(
        OUTPUT  ${CMAKE_CURRENT_BINARY_DIR}/${_basename}.c
        COMMAND ${EXE_HSUBST} -sw
                -e DATE="${TODAY}"
                -e PATH="${CMAKE_SOURCE_DIR}/bin:$ENV{PATH}"
                -i ${CMAKE_CURRENT_SOURCE_DIR}/${_filename}
                -o ${CMAKE_CURRENT_BINARY_DIR}/${_basename}.c
        DEPENDS ${_filename}
    )
ENDMACRO(HSUBST_FILE)

ASCIIDOCTOR_FILE(man/${PROJECT_NAME}.7.adoc.in)
HSUBST_FILE(sig.c.in)

#-------------------------------------------------------------------------------
# Defaults for undefined
#-------------------------------------------------------------------------------
if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release")
endif()

option(MYCALC_EXPORT_COMPILE_COMMANDS
    "Generate compile_commands.json" ON)
mark_as_advanced(MYCALC_EXPORT_COMPILE_COMMANDS)

set (CMAKE_EXPORT_COMPILE_COMMANDS
    ${MYCALC_EXPORT_COMPILE_COMMANDS}
    CACHE BOOL
    "Overloaded cmake global cached CMAKE_EXPORT_COMPILE_COMMANDS"
    FORCE
    )
string(TOUPPER "${CMAKE_BUILD_TYPE}" MYCALC_BUILD_TYPE)

option(MYCALC_C_ANAL
    "Nit-pick build warnings" ON)

set(MYCALC_FUNC_MAXARGS
    "10"
    CACHE STRING
    "Max possible number of arguments to built-in functions")

# Instead of -Wunused, specify exacly which ones
set(MYCALC_ANAL_UNUSED
    -Wunused-function
    -Wunused-label
    -Wunused-local-typedefs
    #-Wunused-macros
    -Wunused-parameter
    -Wno-unused-result
    -Wunused-value
    -Wunused-variable
    -Wunused-const-variable
    -Wunused-but-set-parameter
    -Wunused-but-set-variable
)
if (MYCALC_C_ANAL)
    set(MY_COMPILE_OPTS
        ${MYCALC_ANAL_UNUSED}
        -Wreturn-type
        -Wextra
        -Werror
    )
else()
    set(MY_COMPILE_OPTS
        -Werror
    )
endif()

add_subdirectory(userio)
list(APPEND MYCALC_LIBS user-io)

add_subdirectory(comp)
list(APPEND MYCALC_LIBS comp)

configure_file (
    "${CMAKE_CURRENT_SOURCE_DIR}/config.h.in"
    "${CMAKE_CURRENT_BINARY_DIR}/config.h"
)

target_link_libraries(${PROJECT_NAME} PUBLIC ${MYCALC_LIBS})
target_link_libraries(${PROJECT_NAME}-base PUBLIC ${MYCALC_LIBS})
target_compile_options(${PROJECT_NAME} PRIVATE ${MY_COMPILE_OPTS})
target_compile_options(${PROJECT_NAME}-base PRIVATE ${MY_COMPILE_OPTS})

#-------------------------------------------------------------------------------
# Defaults for undefined
#-------------------------------------------------------------------------------

# Note: Defaulted twice, still not showing that default is set with ccmake
#       Likely because cache for build-ins doesn't work as other variables
#       These lines should have the variable set however regardless.
if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release")
endif()
set(CMAKE_BUILD_TYPE
    ${CMAKE_BUILD_TYPE}Release
    CACHE STRING
    "Build-type")
set_property(CACHE CMAKE_BUILD_TYPE
    PROPERTY STRINGS
    "None" "Debug" "Release" "RelWithDebInfo" "MinSizeRel")

# Installation
# ------------
include(GNUInstallDirs)

# Shells (ignore for now)
#install(PROGRAMS ${PROJECT_BINARY_DIR}/blaha.sh DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)

# Files:
install(FILES
        ${CMAKE_CURRENT_BINARY_DIR}/man/mycalc.7.gz
            DESTINATION ${CMAKE_INSTALL_MANDIR}/man7)

# Targets:
install(TARGETS ${PROJECT_NAME}
        RUNTIME       DESTINATION ${CMAKE_INSTALL_PREFIX}/bin
        LIBRARY       DESTINATION ${CMAKE_INSTALL_PREFIX}/lib
        ARCHIVE       DESTINATION ${CMAKE_INSTALL_PREFIX}/lib
        PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_PREFIX}/include/${PROJECT_NAME})

# Install package generation
# --------------------------
SET(CPACK_PACKAGE_CONTACT "Michael Ambrus")
SET(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
SET(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
SET(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})
SET(CPACK_PACKAGE_VERSION_TWEAK ${PROJECT_VERSION_TWEAK})
set(CPACK_PACKAGE_DESCRIPTION_FILE
    ${PROJECT_SOURCE_DIR}/DESCRIPTION)
SET(CPACK_RESOURCE_FILE_LICENSE
    ${PROJECT_SOURCE_DIR}/LICENSE)

# Append or change if/when needed
list(APPEND PROJ_DEPENDS "libc6 (>= 2.31)")
list(APPEND PROJ_DEPENDS "liblog (>= 0.3.12)")
list(APPEND PROJ_DEPENDS "libregsub (>= 0.2.5)")
# Buit with, therefor required
if (MYCALC_USE_READLINE)
    list(APPEND PROJ_DEPENDS "libreadline8 (>= 8.0)")
endif()

list(JOIN PROJ_DEPENDS ", " PROJ_DEPENDS_STR)
message(STATUS "Dependencies: ${PROJ_DEPENDS_STR}")
set(CPACK_DEBIAN_PACKAGE_DEPENDS "${PROJ_DEPENDS_STR}")

set(SPECIFIED_CPACK
    ${SPECIFIED_CPACK}DEB
    CACHE STRING
    "Specify cpack generator (press enter to toggle choices)")

set_property(CACHE SPECIFIED_CPACK
    PROPERTY STRINGS
    "NSIS ZIP" "DEB" "TGZ" "RPM")

if (NOT SPECIFIED_CPACK STREQUAL "")
    SET(CPACK_GENERATOR ${SPECIFIED_CPACK})
endif ()

INCLUDE(CPack)

