/***************************************************************************
 *   Copyright (C) 2020 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* This file contains mycalc's entry-point, argument and environments
 * handling */

#define _GNU_SOURCE
#if __GNUC__ < 10
#    undef _FORTIFY_SOURCE
#    define _FORTIFY_SOURCE 0
#endif
#include "comp/mycalc_yacc.h"
#include "misc.h"
#include "userio/io.h"
#include "sig.h"
#include "config.h"
#include "main.h"
#include "opts.h"
#include "settings.h"
#include <liblog/log.h>
#include <liblog/assure.h>

#define _GNU_SOURCE
#include <errno.h>
#include <error.h>
#include <gensub.h>
#include <stdio.h>
#include <stdlib.h>
#include <termcap.h>

extern log_level log_filter_level; /* Short-cut to liblog filter variable */

/* Environment variable names */
#define ENV_FORMAT "MYCALC_FORMAT"
#define ENV_SCIMODE "MYCALC_SCIMODE"
#define ENV_HIST "MYCALC_HISTORY_FILE"

/* Syslog includes stderr or not */
#define INCLUDE_STDERR 1
#define NO_STDERR 0

struct opts opts = {
    /* clang-format off */
    .loglevel = &log_filter_level,
    .daemon = 0
    /* clang-format on */
};

/* Static initialization (fields possible to initialize) */
struct mycalc mycalc = {
    /* clang-format off */
    .opts = &opts
    /* clang-format on */
};

/*
 * Comply to environment variables is set. Destination is opts struct. I.e.
 * it is populated in three passes where the last one has highest
 * precedence:
 *
 *  1  - Static initialization
 *  2  - Environment variable
 *  3  - Option
 * (4) - Same option later on command-line
 */
static void environment(void)
{
    char *str;

    str = getenv(ENV_FORMAT);
    if (str != NULL) {
        set_ofprintf_format(str);
    }

    str = getenv(ENV_SCIMODE);
    if (str != NULL) {
        set_printf_post(str);
    }

    str = getenv(ENV_HIST);
    if (str != NULL) {
        history_file(str);
    }
}

/* A simple info-/status-line at top
 */
static void print_header()
{
    char *str;
    char *term = getenv("TERM");
    char termbuf[2048];
    int col;
    int len;
    asprintf(&str, "Expression results are stored in register '%c'",
             get_result_register());
    len = strlen(str);
    fprintf(stderr, "%s", str);
    free(str);

    if (!term)
        goto end;
    if (tgetent(termbuf, term) < 0) {
        error(EXIT_FAILURE, 0, "Could not access the termcap data base.\n");
    }

    col = tgetnum("co");

    for (int i = 0; i < (col - len - 15); i++)
        fprintf(stderr, " ");
    if (mycalc.settings->trig_mode == DEG)
        fprintf(stderr, "[DEG] ");
    else
        fprintf(stderr, "[RAD] ");
    if (mycalc.settings->oformat->scimode == SCI)
        fprintf(stderr, "[SCI] ");
    else
        fprintf(stderr, "[NRM] ");
end:
    fprintf(stderr, "\n");
}

int main(int argc, char **argv)
{
    int rc, new_argc = argc;
    char **new_argv = argv;
    int log_level = MYCALC_LOG_LEVEL;
    char *pname;

    if (getenv("LOGLEVEL") != NULL) {
        log_level = log_getenv_loglevel();
    }
    log_syslog_config(NO_STDERR);
    LOGE("log_level: %d\n", log_level);
    log_set_verbosity(log_level);

    log_syslog_config(INCLUDE_STDERR);
    pname = gensub(".*\\/", "", 1, argv[0]);
    log_set_process_name(pname);

    ASSURE(sizeof(void *) >= sizeof(int));
    log_syslog_config(NO_STDERR);
    log_set_verbosity(log_level);

    LOGI("\"%s\" version v%s \n", pname, VERSION);

    mycalc.settings = settings_p();
    mycalc.settings->oformat->file = stdout;
    set_ofprintf_format(DFLT_PRINTF_FORMAT);

    environment();

    opts_init();
    /* /begin/ zeroing of opt vars */
    /* /end/   zeroing of opt vars */

    ASSURE_E((rc = opts_parse(&new_argc, &new_argv, &opts)) >= 0,
             mycalc_exit(254));
    LOGI("Parsed %d options.\n", rc);
    ASSURE_E(opts_check(&opts) == OPT_OK, mycalc_exit(255));
    LOGI("Option passed rule-check OK\n", rc);

    set_myprogname(argv[0]);
    //set_ofprintf_format(opts.printf_format);

    set_result_register(regof(DFLT_RESULT_REG));
    ioinit();
    siginit();

    fprintf(stderr, "Welcome to %s version " VERSION "\n", pname);
    print_header();

    rc = yyparse();
    iofini();
    return rc;
}

void mycalc_exit(int status)
{
    LOGD("mycalc_exit initiated\n");

    free(opts.req_opts);
    exit(status);
}
