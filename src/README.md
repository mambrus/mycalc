# MyCalc

## Math functions

### Basic functions

Funtions that have native types, in and out, of the same type as
`MyCalc` has:

_One argument funtions:_

* [acosh](https://pubs.opengroup.org/onlinepubs/009695399/functions/acosh.html)
* [acos](https://pubs.opengroup.org/onlinepubs/009695399/functions/acos.html)
* [asinh](https://pubs.opengroup.org/onlinepubs/009695399/functions/asinh.html)
* [asin](https://pubs.opengroup.org/onlinepubs/009695399/functions/asin.html)
* [atanh](https://pubs.opengroup.org/onlinepubs/009695399/functions/atanh.html)
* [atan](https://pubs.opengroup.org/onlinepubs/009695399/functions/atan.html)
* [cbrt](https://pubs.opengroup.org/onlinepubs/009695399/functions/cbrt.html)
* [ceil](https://pubs.opengroup.org/onlinepubs/009695399/functions/ceil.html)
* [cosh](https://pubs.opengroup.org/onlinepubs/009695399/functions/cosh.html)
* [cos](https://pubs.opengroup.org/onlinepubs/009695399/functions/cos.html)
* [erfc](https://pubs.opengroup.org/onlinepubs/009695399/functions/erfc.html)
* [erf](https://pubs.opengroup.org/onlinepubs/009695399/functions/erf.html)
* [exp2](https://pubs.opengroup.org/onlinepubs/009695399/functions/exp2.html)
* [exp](https://pubs.opengroup.org/onlinepubs/009695399/functions/exp.html)
* [expm1](https://pubs.opengroup.org/onlinepubs/009695399/functions/expm1.html)
* [fabs](https://pubs.opengroup.org/onlinepubs/009695399/functions/fabs.html)
* [floor](https://pubs.opengroup.org/onlinepubs/009695399/functions/floor.html)
* [lgamma](https://pubs.opengroup.org/onlinepubs/009695399/functions/lgamma.html)
* [log10](https://pubs.opengroup.org/onlinepubs/009695399/functions/log10.html)
* [log1p](https://pubs.opengroup.org/onlinepubs/009695399/functions/log1p.html)
* [log2](https://pubs.opengroup.org/onlinepubs/009695399/functions/log2.html)
* [logb](https://pubs.opengroup.org/onlinepubs/009695399/functions/logb.html)
* [log](https://pubs.opengroup.org/onlinepubs/009695399/functions/log.html)
* [nearbyint](https://pubs.opengroup.org/onlinepubs/009695399/functions/nearbyint.html)
* [rint](https://pubs.opengroup.org/onlinepubs/009695399/functions/rint.html)
* [round](https://pubs.opengroup.org/onlinepubs/009695399/functions/round.html)
* [sinh](https://pubs.opengroup.org/onlinepubs/009695399/functions/sinh.html)
* [sin](https://pubs.opengroup.org/onlinepubs/009695399/functions/sin.html)
* [sqrt](https://pubs.opengroup.org/onlinepubs/009695399/functions/sqrt.html)
* [tanh](https://pubs.opengroup.org/onlinepubs/009695399/functions/tanh.html)
* [tan](https://pubs.opengroup.org/onlinepubs/009695399/functions/tan.html)
* [tgamma](https://pubs.opengroup.org/onlinepubs/009695399/functions/tgamma.html)
* [trunc](https://pubs.opengroup.org/onlinepubs/009695399/functions/trunc.html)

_One argument funtions returning integer:_

* [rint](https://pubs.opengroup.org/onlinepubs/009695399/functions/rint.html)
* [round](https://pubs.opengroup.org/onlinepubs/009695399/functions/round.html)

_Two argument funtions:_

* _TBD [atan2](https://pubs.opengroup.org/onlinepubs/009695399/functions/atan2.html)_
* [copysign](https://pubs.opengroup.org/onlinepubs/009695399/functions/copysign.html)
* [fdim](https://pubs.opengroup.org/onlinepubs/009695399/functions/fdim.html)
* [fmax](https://pubs.opengroup.org/onlinepubs/009695399/functions/fmax.html)
* [fmin](https://pubs.opengroup.org/onlinepubs/009695399/functions/fmin.html)
* [fmod](https://pubs.opengroup.org/onlinepubs/009695399/functions/fmod.html)
* [hypot](https://pubs.opengroup.org/onlinepubs/009695399/functions/hypot.html)
* [nextafter](https://pubs.opengroup.org/onlinepubs/009695399/functions/nextafter.html)
* [nexttoward](https://pubs.opengroup.org/onlinepubs/009695399/functions/nexttoward.html)
* [pow](https://pubs.opengroup.org/onlinepubs/009695399/functions/pow.html)
* [remainder](https://pubs.opengroup.org/onlinepubs/009695399/functions/remainder.html)


### `libm` reference
```
double      acos(double);
long double acosl(long double);
float       acosf(float);

double      acosh(double);
float       acoshf(float);
long double acoshl(long double);

double      asin(double);
float       asinf(float);
long double asinl(long double);

double      asinh(double);
float       asinhf(float);
long double asinhl(long double);

double      atan(double);
float       atanf(float);
long double atanl(long double);
double      atan2(double, double);
float       atan2f(float, float);
long double atan2l(long double, long double);

double      atanh(double);
float       atanhf(float);
long double atanhl(long double);

double      cbrt(double);
float       cbrtf(float);
long double cbrtl(long double);

double      ceil(double);
float       ceilf(float);
long double ceill(long double);

double      copysign(double, double);
float       copysignf(float, float);
long double copysignl(long double, long double);

double      cos(double);
float       cosf(float);
double      cosh(double);
float       coshf(float);
long double coshl(long double);
long double cosl(long double);

double      erf(double);
double      erfc(double);
float       erfcf(float);
long double erfcl(long double);
float       erff(float);
long double erfl(long double);

double      exp(double);
double      exp2(double);
float       exp2f(float);
long double exp2l(long double);
float       expf(float);

long double expl(long double);
double      expm1(double);
float       expm1f(float);
long double expm1l(long double);

double      fabs(double);
float       fabsf(float);
long double fabsl(long double);

double      fdim(double, double);
float       fdimf(float, float);
long double fdiml(long double, long double);

double      floor(double);
float       floorf(float);
long double floorl(long double);

double      fma(double, double, double);
float       fmaf(float, float, float);
long double fmal(long double, long double, long double);

double      fmax(double, double);
float       fmaxf(float, float);
long double fmaxl(long double, long double);

double      fmin(double, double);
float       fminf(float, float);
long double fminl(long double, long double);

double      fmod(double, double);
float       fmodf(float, float);
long double fmodl(long double, long double);

double      frexp(double, int *);
float       frexpf(float value, int *);
long double frexpl(long double value, int *);

double      hypot(double, double);
float       hypotf(float, float);
long double hypotl(long double, long double);

int         ilogb(double);
int         ilogbf(float);
int         ilogbl(long double);

double      j0(double);
double      j1(double);
double      jn(int, double);

double      ldexp(double, int);
float       ldexpf(float, int);
long double ldexpl(long double, int);
double      lgamma(double);

float       lgammaf(float);
long double lgammal(long double);

long long   llrint(double);
long long   llrintf(float);
long long   llrintl(long double);

long long   llround(double);
long long   llroundf(float);
long long   llroundl(long double);

double      log(double);
double      log10(double);
float       log10f(float);
long double log10l(long double);
double      log1p(double);
float       log1pf(float);
long double log1pl(long double);
double      log2(double);
float       log2f(float);
long double log2l(long double);
double      logb(double);
float       logbf(float);
long double logbl(long double);
float       logf(float);
long double logl(long double);

long        lrint(double);
long        lrintf(float);
long        lrintl(long double);

long        lround(double);
long        lroundf(float);
long        lroundl(long double);

double      modf(double, double *);
float       modff(float, float *);
long double modfl(long double, long double *);

double      nan(const char *);
float       nanf(const char *);
long double nanl(const char *);

double      nearbyint(double);
float       nearbyintf(float);
long double nearbyintl(long double);

double      nextafter(double, double);
float       nextafterf(float, float);
long double nextafterl(long double, long double);
double      nexttoward(double, long double);
float       nexttowardf(float, long double);
long double nexttowardl(long double, long double);

double      pow(double, double);
float       powf(float, float);
long double powl(long double, long double);

double      remainder(double, double);
float       remainderf(float, float);
long double remainderl(long double, long double);

double      remquo(double, double, int *);
float       remquof(float, float, int *);
long double remquol(long double, long double, int *);

double      rint(double);
float       rintf(float);
long double rintl(long double);

double      round(double);
float       roundf(float);
long double roundl(long double);

double      scalb(double, double);
double      scalbln(double, long);
float       scalblnf(float, long);
long double scalblnl(long double, long);
double      scalbn(double, int);
float       scalbnf(float, int);
long double scalbnl(long double, int);

double      sin(double);
float       sinf(float);
double      sinh(double);
float       sinhf(float);
long double sinhl(long double);
long double sinl(long double);

double      sqrt(double);
float       sqrtf(float);
long double sqrtl(long double);

double      tan(double);
float       tanf(float);
double      tanh(double);
float       tanhf(float);
long double tanhl(long double);
long double tanl(long double);

double      tgamma(double);
float       tgammaf(float);
long double tgammal(long double);

double      trunc(double);
float       truncf(float);
long double truncl(long double);

double      y0(double);
double      y1(double);
double      yn(int, double);
```
