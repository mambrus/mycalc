/***************************************************************************
 *   Copyright (C) 2020 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* Handle Settings and logic thereof. I.e. settings from environment,
 * CLI-options and set/get from running operation.
 */

#include "settings.h"
#include <liblog/log.h>
#include <liblog/assure.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include "config.h"

/***************************************************************************
 * Numerical output formatter
 ***************************************************************************/
static struct oformat oformat = {
    .format = NULL,
    .scimode = DFLT_PRINTF_SCI_MODE,
    .grouping = NULL,

};

static struct settings settings = {
    .oformat = &oformat,
};

struct settings *settings_p(void)
{
    return &settings;
};

/* Set printf output format */
int set_ofprintf_format(char *format)
{
    LOGI("New format request: %s\n", format);

    free(oformat.format);
    oformat.format = NULL;

    oformat.format = malloc(strlen(format) + 1);
    sprintf(oformat.format, "%%%s", format);
    LOGI("Format: %s\n", oformat.format);

    /* Sanity can't be tested, always return success */
    /* Yes it can (TODO) */
    return 0;
}

/* Set printf post formatter */
int set_printf_post(char *formatter)
{
    LOGI("New formatter request: %s\n", formatter);
    if (strcasecmp(formatter, "sci") == 0) {
        oformat.scimode = SCI;
        return 0;
    }
    if (strcasecmp(formatter, "nrm") == 0) {
        oformat.scimode = NRM;
        return 0;
    }

    LOGE("Bad formatter: %s\n", formatter);
    return 1;
}

char *get_ofprintf_format()
{
    return oformat.format;
}

char *get_printf_post()
{
    return oformat.scimode == SCI ? "SCI" : "NRM";
}
