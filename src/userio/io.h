#ifndef io_h
#define io_h
#include "common.h"
#include <stdio.h>
#include <stdbool.h>

/*
 * Post-formatter grouping
 */
enum pfx { PFX_NONE, PFX_ALL, PFX_FIRST, PFX_SUFFIX, PFX_SENTINEL };
struct grouping {
    const enum base_id id;
    int digits;
    char sep;
    enum pfx pfx;
    bool zpad;
};
extern struct grouping grouping[];
long long bins2int(char *str);

/**
 * @brief Output format shortcut
 */
extern struct oformat *ofrmt;

/* Main-functions */
int ioinit(void);
int iofini(void);
const char *history_file(const char *name);
int getinput(char *buf, size_t size);
void result_out(Float *res, char *bottonline);
void during_line_begin(const char *text);
void during_line_end();

/*Number format and printing */
void printnumber(Float number);
char format_letter();
enum base_id printf_base();
int snprindent(size_t depth, char *str, size_t size, const char *format, ...);
int sindent(size_t depth, char *str, size_t size);
int ilogx(char c, size_t depth, const char *format, ...);
int set_loglevel(char *level);
int get_loglevel();

/* Printf extension - binary integer formatter */
struct printf_info;
int printf_binary(FILE *stream, const struct printf_info *info,
                  const void *const *args);
int printf_binary_arginfo_size(const struct printf_info *info, size_t n,
                               int *argtypes, int *size);
int asprintf_binary(char **numstr, const struct printf_info *info,
                    const void *const *args);

Float read_binary_f(const char *);
Float read_binary_i(const char *);

Float read_octal_f(const char *);
Float read_octal_i(const char *);

Float read_hex_f(const char *);
Float read_hex_i(const char *);

#define ILOGV(N, ...) ilogx('V', N, __VA_ARGS__);
#define ILOGD(N, ...) ilogx('D', N, __VA_ARGS__);
#define ILOGI(N, ...) ilogx('I', N, __VA_ARGS__);
#define ILOGW(N, ...) ilogx('W', N, __VA_ARGS__);
#define ILOGE(N, ...) ilogx('E', N, __VA_ARGS__);

/* Produced by auto-generation in c-file without any public declaration */
#define YY_NULL 0
extern FILE *yyin, *yyout;

#endif //io_h
