#define _GNU_SOURCE
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <printf.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <liblog/log.h>
#include <liblog/assure.h>
#include <stdarg.h>
#include <gensub.h>
#include "main.h"
#include "io.h"
#include "common.h"
#include "misc.h"
#include "settings.h"
#include "config.h"

#define MAX_TSTR 128
#define DEF_HISTORY_NAME ".mycalc_history"

struct oformat *ofrmt;

struct {
    const char *history;
    int nelements;
    char *history_ts;
    char *line_read;
} io = {
    .history = NULL,
    .nelements = 0,
    .line_read = NULL,
};

/* Grouping settings storage initialized with configure defaults
 */
struct grouping grouping[] = {
    [HEX] = {
        .id= HEX,
        .digits = DFLT_GROUP_HEX_DIGITS,
        .sep = DFLT_GROUP_HEX_SEP,
        .pfx = DFLT_GROUP_HEX_PFX,
        .zpad = DFLT_GROUP_HEX_ZPAD,
    },
    [DEC] = {
        .id= DEC,
        .digits = DFLT_GROUP_DEC_DIGITS,
        .sep = DFLT_GROUP_DEC_SEP,
        .pfx = DFLT_GROUP_DEC_PFX,
        .zpad = DFLT_GROUP_DEC_ZPAD,
    },
    [OCT] = {
        .id= OCT,
        .digits = DFLT_GROUP_OCT_DIGITS,
        .sep = DFLT_GROUP_OCT_SEP,
        .pfx = DFLT_GROUP_OCT_PFX,
        .zpad = DFLT_GROUP_BIN_ZPAD,
    },
    [BIN] = {
        .id= BIN,
        .digits = DFLT_GROUP_BIN_DIGITS,
        .sep = DFLT_GROUP_BIN_SEP,
        .pfx = DFLT_GROUP_BIN_PFX,
        .zpad = DFLT_GROUP_BIN_ZPAD,
    },
};
/* Environment variable names */
#define ENV_GROUP_HEX_DIGITS "MYCALC_GROUP_HEX_DIGITS"
#define ENV_GROUP_HEX_SEP "MYCALC_GROUP_HEX_SEP"
#define ENV_GROUP_HEX_PFX "MYCALC_GROUP_HEX_PFX"
#define ENV_GROUP_HEX_ZPAD "MYCALC_GROUP_HEX_ZPAD"

#define ENV_GROUP_DEC_DIGITS "MYCALC_GROUP_DEC_DIGITS"
#define ENV_GROUP_DEC_SEP "MYCALC_GROUP_DEC_SEP"
#define ENV_GROUP_DEC_PFX "MYCALC_GROUP_DEC_PFX"
#define ENV_GROUP_DEC_ZPAD "MYCALC_GROUP_DEC_ZPAD"

#define ENV_GROUP_OCT_DIGITS "MYCALC_GROUP_OCT_DIGITS"
#define ENV_GROUP_OCT_SEP "MYCALC_GROUP_OCT_SEP"
#define ENV_GROUP_OCT_PFX "MYCALC_GROUP_OCT_PFX"
#define ENV_GROUP_OCT_ZPAD "MYCALC_GROUP_OCT_ZPAD"

#define ENV_GROUP_BIN_DIGITS "MYCALC_GROUP_BIN_DIGITS"
#define ENV_GROUP_BIN_SEP "MYCALC_GROUP_BIN_SEP"
#define ENV_GROUP_BIN_PFX "MYCALC_GROUP_BIN_PFX"
#define ENV_GROUP_BIN_ZPAD "MYCALC_GROUP_BIN_ZPAD"
/*
 * Environment handling of io-relevant variables
 */
static void io_environment(void)
{
    char *str;
    struct {
        const enum pfx id;
        const char *name;
    } const pfxtype[] = {
        [PFX_NONE] = {.name="NONE", .id=PFX_NONE,},
        [PFX_ALL] = {.name="ALL", .id=PFX_ALL,},
        [PFX_FIRST] = {.name="FIRST", .id=PFX_FIRST,},
        [PFX_SUFFIX] = {.name="UFFIX", .id=PFX_SUFFIX,},
    };
    struct {
        const enum base_id id;
        const char *digits;
        const char *sep;
        const char *pfx;
        const bool zpad;
    } const env_name[] = {
        [BIN] = { .id = BIN,
                  .digits = ENV_GROUP_BIN_DIGITS,
                  .sep = ENV_GROUP_BIN_SEP,
                  .pfx = ENV_GROUP_BIN_PFX,
                  .zpad = ENV_GROUP_BIN_ZPAD,
        },
        [OCT] = { .id = OCT,
                  .digits = ENV_GROUP_OCT_DIGITS,
                  .sep = ENV_GROUP_OCT_SEP,
                  .pfx = ENV_GROUP_OCT_PFX,
                  .zpad = ENV_GROUP_OCT_ZPAD,
        },
        [DEC] = { .id = DEC,
                  .digits = ENV_GROUP_DEC_DIGITS,
                  .sep = ENV_GROUP_DEC_SEP,
                  .pfx = ENV_GROUP_DEC_PFX,
                  .zpad = ENV_GROUP_DEC_ZPAD,
        },
        [HEX] = { .id = HEX,
                  .digits = ENV_GROUP_HEX_DIGITS,
                  .sep = ENV_GROUP_HEX_SEP,
                  .pfx = ENV_GROUP_HEX_PFX,
                  .zpad = ENV_GROUP_HEX_ZPAD,
        },
    };

    /* Deduct environment-variable existence and set appropriately number of
       characters in group */
    for (enum base_id i = BIN; i <= HEX; i++) {
        str = getenv(env_name[i].digits);
        if (str != NULL) {
            grouping[i].digits = atoi(str);
        }
    }
    /* Deduct environment-variable existence and set appropriately separator
       character */
    for (enum base_id i = BIN; i <= HEX; i++) {
        str = getenv(env_name[i].sep);
        if (str != NULL) {
            grouping[i].sep = str[0];
        }
    }
    /* Deduct environment-variable existence and set appropriately pfx-type*/
    for (enum base_id i = BIN; i <= HEX; i++) {
        str = getenv(env_name[i].pfx);
        if (str != NULL) {
            for (enum pfx j = PFX_NONE; j <= PFX_SUFFIX; j++) {
                if (strcasecmp(pfxtype[j].name, str) == 0)
                    grouping[i].pfx = j;
            }
        }
    }
    /* Deduct environment-variable existence and set appropriately Zero-padding*/
    for (enum base_id i = BIN; i <= HEX; i++) {
        str = getenv(env_name[i].pfx);
        if (str != NULL) {
            if (strcasecmp(str, "ON") == 0)
                grouping[i].zpad = true;
            else
                grouping[i].zpad = false;
        }
    }
}

char format_letter()
{
    char *format = get_ofprintf_format();
    size_t flen = strlen(format);
    return format[flen - 1];
}

enum base_id printf_base()
{
    char fc = format_letter();
    enum base_id base = BASE_SENTINEL;

    switch (fc) {
        /* Base 10 (DEC) */
    case 'd':
    case 'i':
    case 'u':
    case 'e':
    case 'E':
    case 'g':
    case 'G':
    case 'f':
        base = DEC;
        break;
        /* Base 16 (HEX) */
    case 'x':
    case 'X':
    case 'a':
    case 'A':
        base = HEX;
        break;
        /* Base 8 (OCT) */
    case 'o':
        base = OCT;
        break;
        /* Base 2 (BIN) */
    case 'b':
        base = BIN;
        break;
    default:
        LOGE("Unknown base in format-string %s: \"%c\"\n", get_ofprintf_format,
             fc);
    }
    return base;
}

#ifdef MYCALC_USE_READLINE
#    include <readline/readline.h>
#    include <readline/history.h>

/* Entry point for reading one line_read (Lexer invokes) */
int getinput(char *buf, size_t size)
{
    struct timeval tv;
    time_t t;
    struct tm *info;

    if (feof(yyin))
        return YY_NULL;
    io.line_read = readline("> ");
    if (!io.line_read)
        return YY_NULL;
    if (strlen(io.line_read) > size - 2) {
        fprintf(stderr, "input line_read too long\n");
        return YY_NULL;
    }
    sprintf(buf, "%s\n", io.line_read);
    gettimeofday(&tv, NULL);
    t = tv.tv_sec;

    info = localtime(&t);
    io.history_ts = asctime(info); /* Returns a string composed of
                                       constant's. Do not free */
    return strlen(buf);
}

static void line_read_fini()
{
    HIST_ENTRY **list = history_list();
    UNUSED(list);
    if (io.line_read && *io.line_read) {
        //strstr( Clean-up line (TODO)
        add_history(io.line_read);
        /* add_history_time(io.history_ts); Done auto magically */
        io.nelements++;
        free(io.line_read);
    }
    io.line_read = NULL;
}
#else
#    define line_read_fini(...) ((void)0)
#endif //MYCALC_USE_READLINE

int ioinit()
{
    io_environment();
#ifdef MYCALC_USE_READLINE
    using_history();
    history_write_timestamps = 1;
    history_comment_char = '#';
#endif
    /* Register the printf function for binary */
#ifdef MYCALC_PRINTF_REGISTER
    register_printf_specifier('b', printf_binary, printf_binary_arginfo_size);
#endif

    ofrmt = mycalc.settings->oformat;
    if (!io.history) {
        /* Test current directory */
        if (access(DEF_HISTORY_NAME, R_OK | W_OK) == 0) {
            ASSERT(history_file(DEF_HISTORY_NAME) != NULL);
        } else {
            char *str;
            static char fname[PATH_MAX];

            str = getenv("HOME");
            if (str != NULL) {
                snprintf(fname, PATH_MAX, "%s/%s", str, DEF_HISTORY_NAME);
                history_file(fname);
            }
        }
    }
#ifdef MYCALC_USE_READLINE
    /* Don't crash, just log if fail */
    ASSURE_E(read_history(io.history) == 0, goto fail);
#endif
    return 0;
#ifdef MYCALC_USE_READLINE
fail:
    return 1;
#endif
}

int iofini()
{
#ifdef MYCALC_USE_READLINE
    LOGD("Appending history: %s\n", io.history);
    ASSURE(append_history(io.nelements, io.history) == 0);
#endif
    return 0;
}

const char *history_file(const char *fname)
{
    int fd;
    mode_t mode = S_IWUSR | S_IRUSR;

    if (access(fname, F_OK) == 0) {
        io.history = fname;
    } else {
        if ((fd = creat(fname, mode)) < 0)
            LOGE("File creation error: %s", fname);
        else {
            close(fd);
            io.history = fname;
        }
    }
    return io.history;
}

/* Exit-point (grammar invokes) */
void result_out(Float *res, char *bottonline)
{
    /* If managed reaching this point it means grammar is happy. Line is iow
     * good. Save it for posterity */
    line_read_fini();

    /* If there's no terminal and reaching this point after
     * a statement or declaration, don't output (TODO) */
    {
        if (res) {
            printnumber(*res);
            printf("\n");
        }

        if (bottonline)
            printf("%s", bottonline);
    }
    during_line_end();
}

/*
 * Sprintf indented to string
 *
 */
#define MYCALC_INDENT_BY 3
int sindent(size_t depth, char *str, size_t size)
{
    size_t i, j = 0, k, left;
    char snip[MYCALC_INDENT_BY] = { [0 ... MYCALC_INDENT_BY - 1] = ' ' };

    memset(str, 0, size);

    left = size;
    for (k = MYCALC_INDENT_BY, i = 0;
         (j < size) && (k == MYCALC_INDENT_BY) && (i < depth); i++, left -= k) {
        k = snprintf(&str[j], left, "%s", snip);
        j += k;
    }
    return j;
}

int snprindent(size_t depth, char *str, size_t size, const char *format, ...)
{
    va_list ap;
    size_t i;
    size_t left = size;
    va_start(ap, format);

    i = sindent(depth, str, size);
    if (i >= size)
        return i;

    left -= i;

    i += vsnprintf(&str[i], left, format, ap);
    va_end(ap);
    return i;
}

/*
 * Indented log by depth
 */
#define MYCALC_STRBUF_SZ 512

int ilogx(char c, size_t depth, const char *format, ...)
{
    va_list ap;
    char str[MYCALC_STRBUF_SZ];
    size_t i;
    size_t size = MYCALC_STRBUF_SZ - 1;
    size_t left = size;
    va_start(ap, format);

    i = sindent(depth, str, size);
    if (i >= size)
        return i;

    left -= i;

    i += vsnprintf(&str[i], left, format, ap);
    va_end(ap);

    switch (c) {
    case 'V':
        LOGV("%s", str);
        break;
    case 'D':
        LOGD("%s", str);
        break;
    case 'I':
        LOGI("%s", str);
        break;
    case 'W':
        LOGW("%s", str);
        break;
    case 'E':
        LOGE("%s", str);
        break;
    default:
        ASSERT("Illegal option" == NULL);
    }

    return i;
}

int set_loglevel(char *lstr)
{
    int valid;

    printf("\r");

    int level = str2loglevel(lstr, &valid);
    if (valid) {
        log_set_verbosity(level);
    }
    get_loglevel();
    return valid;
}

int get_loglevel()
{
    int level = log_get_verbosity();

    printf("\r");

    switch (level) {
    case LOG_LEVEL_VERBOSE:
        printf("VERBOSE\n");
        break;
    case LOG_LEVEL_DEBUG:
        printf("DEBUG\n");
        break;
    case LOG_LEVEL_INFO:
        printf("INFO\n");
        break;
    case LOG_LEVEL_WARNING:
        printf("WARNING\n");
        break;
    case LOG_LEVEL_ERROR:
        printf("ERROR\n");
        break;
    case LOG_LEVEL_CRITICAL:
        printf("CRITICAL\n");
        break;
    case LOG_LEVEL_SILENT:
        printf("SILENT\n");
        break;
    default:
        printf("Log-level unknown\n");
    };

    return level;
}
