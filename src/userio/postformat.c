#define _GNU_SOURCE
#if __GNUC__ < 10
#    undef _FORTIFY_SOURCE
#    define _FORTIFY_SOURCE 0
#endif
#include <gensub.h>
#include <regsub.h>
#include <regextra.h>
#include <regstring.h>
#include <math.h>
#include <liblog/log.h>
#include <liblog/assure.h>
#include "postformat.h"
#include "io.h"
#include "main.h"
#include "settings.h"
#include "common.h"

/* Group formatter constants and types */
#define MAX_MATCH 15
#define DEF_CFLAGS (REG_EXTENDED | REG_NEWLINE)
#define DEF_EFLAGS (0)
enum numparts { SIGN = 0, INT, FRAC, EXP, MAX_NUMPARTS };

/*
 * Scientific formatter
 */
static char *format_sci(char *numstr, const char *estr)
{
    char echar = estr[0];
    int exponent = atoi(estr + 1);
    numstr[estr - numstr] = 0;
    int significants = strlen(numstr);
    double base = atof(numstr);
    int newexponent;
    int precision;

    if (numstr[0] == '-')
        significants--;
    if (index(numstr, '.'))
        significants--;
    LOGI("N=%d, B=%f E=%d\n", significants, base, exponent);

    free(numstr);

    /* Calculate base and exponent for the new number format */
    if (exponent >= 0) {
        newexponent = (exponent / 3) * 3;
    } else {
        newexponent = (exponent % 3) ? ((exponent - 3) / 3) * 3 : exponent;
    }
    base = base * pow(10, exponent - newexponent);

    /* Calculate precision for result to maintain the same number of
          significant figures */
    precision = MAX(0, significants - (exponent - newexponent) - 1);
    LOGI("N=%d, Eo=%d, En=%d, Pr=%d\n", significants, exponent, newexponent,
         precision);

    /* Construct new format string */
    char *format;
    (void)(asprintf(&format, "%%.%df%%c%%c%%d", precision) + 1);

    (void)(asprintf(&numstr, format, base, echar, exponent < 0 ? '-' : '+',
                    abs(newexponent)) +
           1);
    free(format);
    return (numstr);
}

static int deduct_base_class(enum base_id base, const char **re_numclass,
                             char **str_base)
{
    switch (base) {
    case HEX:
        *re_numclass = "[:xdigit:]";
        if ((ofrmt->grouping->pfx == PFX_ALL) ||
            (ofrmt->grouping->pfx == PFX_FIRST))
            *str_base = strdup("0x");
        if (ofrmt->grouping->pfx == PFX_SUFFIX)
            *str_base = strdup("(HEX)");
        break;
    case DEC:
        *re_numclass = "[:digit:]";
        if ((ofrmt->grouping->pfx == PFX_ALL) ||
            (ofrmt->grouping->pfx == PFX_FIRST))
            *str_base = strdup("0d");
        if (ofrmt->grouping->pfx == PFX_SUFFIX)
            *str_base = strdup("(DEC)");
        break;
    case OCT:
        *re_numclass = "0-7";
        if ((ofrmt->grouping->pfx == PFX_ALL) ||
            (ofrmt->grouping->pfx == PFX_FIRST))
            *str_base = strdup("0c");
        if (ofrmt->grouping->pfx == PFX_SUFFIX)
            *str_base = strdup("(OCT)");
        break;
    case BIN:
        *re_numclass = "01";
        if ((ofrmt->grouping->pfx == PFX_ALL) ||
            (ofrmt->grouping->pfx == PFX_FIRST))
            *str_base = strdup("0b");
        if (ofrmt->grouping->pfx == PFX_SUFFIX)
            *str_base = strdup("(BIN)");
        break;
    default:
        fprintf(stderr, "Grouping not supported\n");
        return 0;
    }
    return 1;
}

/*
 * Split a number-string into strings of it's components:
 * 1. Sign                      : if any
 * 2. Integer                   : always
 * 3. Fraction                  : if any
 * 4. Exponential expression    : if any
 */
static int split_nums(char *numstr, char *spart[], const char *re_numclass)
{
    char *re = NULL; /* Composed search */
    regmatch_t pmatch[2] = { [0 ... 1] = { -1, -1 } };
    char *ps = numstr;
    int rc = 1;

    if (ps[0]) {
        if ((ps[0] == '+') || (ps[0] == '-')) {
            spart[SIGN] = strndup(ps, 1);
            ps++;
        }
    }

    if (ps[0]) {
        asprintf(&re, "^[%s]+", re_numclass);
        pmatch[0] = regfind(re, ps, 0);
        strfree(&re);
        if (regmatches(pmatch) == 1) {
            spart[INT] = strndup(ps, pmatch[0].rm_eo);
            ps += pmatch[0].rm_eo;
        }
    } else {
        rc = 0;
        LOGE("%s: NaN detected: %s (%s)\n", re, __func__, numstr);
        /* NaN. No point in continuing */
        goto done;
    }

    if (ps[0]) {
        asprintf(&re, "^\\.[%s]+", re_numclass);
        pmatch[0] = regfind(re, ps, 0);
        strfree(&re);
        if (regmatches(pmatch) == 1) {
            spart[FRAC] = strndup(&ps[1], pmatch[0].rm_eo - 1);
            ps += pmatch[0].rm_eo;
        }
    }

    if (ps[0]) {
        asprintf(&re, "^[EePp][+-]?[[:digit:]]+");
        pmatch[0] = regfind(re, ps, 0);
        strfree(&re);
        if (regmatches(pmatch) == 1) {
            spart[EXP] = strndup(ps, pmatch[0].rm_eo);
            ps += pmatch[0].rm_eo;
        }
    }

    for (enum numparts part = SIGN; part < MAX_NUMPARTS; part++) {
        if (!spart[part])
            spart[part] = strdup("");
    }

done:
    return rc;
}

/*
 * Group formatter
 */
static char *format_group(char *numstr)
{
    char *spart[MAX_NUMPARTS] = { NULL };
    /* Default regmatch indexes. Note thatfor each sub-expression
       missing, consequtive indexes need to be decremented */
    const char *re_numclass = ""; /* Num re for each base (asgn silence warn)*/
    char *str_base = NULL;
    char *re = NULL; /* Composed search */
    char *sb = NULL; /* Composed replace */
    char *z_str, *tmps = NULL, *tmps2 = NULL;
    enum base_id base = printf_base();
    int pad_zeros;
    z_str = strdup("");

    /* pre-formatter g/G formats a string starting with 0x 0X and
       with/without sign. Future pre-formatters may follow the same pattern
     */
    char *numstr_ptr = numstr; /* Helper pointer. Never free this */
    if (numstr[0] == '0')
        numstr_ptr = &numstr[2];
    if ((numstr[0] == '-') && (numstr[1] == '0'))
        numstr_ptr = &numstr[3];

    deduct_base_class(base, &re_numclass, &str_base);
    if (!split_nums(numstr_ptr, spart, re_numclass)) {
        /* Failure but error is notified. Return the same sting but free any
           allocated resources */
        goto done;
    }

    /* split_nums fails with sign for this case */
    if ((numstr[0] == '-') && (numstr[1] == '0'))
        STRASGN(spart[SIGN]) = strdup("-");

    strfree(&numstr); /* Now spent so dispose */

    /* -- The actual grouping -- */
    /* Compose the substitution string */
    asprintf(&sb, "([%s]{%d})", re_numclass, ofrmt->grouping->digits);
    STRASGN(re) = strdup("\\1 ");
    if (ofrmt->grouping->pfx == PFX_ALL) {
        strprepend(str_base, &re);
    }

    /* -- INT -- */
    if (strlen(spart[INT]) > 0) {
        /* Pad integer-part with zeros up until even modulo */
        pad_zeros = strlen(spart[INT]) % ofrmt->grouping->digits;
        if (pad_zeros) {
            pad_zeros = ofrmt->grouping->digits - pad_zeros;
        }
        for (int i = 0; i < pad_zeros; i++) {
            strappnd(&z_str, "0");
        }
        strprepend(z_str, &spart[INT]);

        tmps = gensub(sb, re, 'g', spart[INT]);
        if (tmps[strlen(tmps) - 1] == ' ')
            tmps[strlen(tmps) - 1] = 0;

        /* Remove padded zeros again if not requested */
        if (!ofrmt->grouping->zpad) {
            tmps2 = tmps;
            tmps = gensub("^0+", "", '1', tmps2);
        }
        STRASGN(spart[INT]) = tmps;
    }

    /* -- FRAC -- */
    if (strlen(spart[FRAC]) > 0) {
        tmps = gensub(sb, re, 'g', spart[FRAC]);
        if (tmps[strlen(tmps) - 1] == ' ')
            tmps[strlen(tmps) - 1] = 0;
        strprepend(".", &tmps);

        STRASGN(spart[FRAC]) = tmps;
    }

    /* Finalize */
    switch (ofrmt->grouping->pfx) {
    case PFX_FIRST:
        asprintf(&numstr, "%s %s%s%s%s", str_base, spart[SIGN], spart[INT],
                 spart[FRAC], spart[EXP]);
        break;
    case PFX_SUFFIX:
        asprintf(&numstr, "%s%s%s%s %s", spart[SIGN], spart[INT], spart[FRAC],
                 spart[EXP], str_base);
        break;
    default:
        asprintf(&numstr, "%s%s%s%s", spart[SIGN], spart[INT], spart[FRAC],
                 spart[EXP]);
        break;
    }

done:
    strfree(&str_base);
    strfree(&z_str);
    strfree(&re);
    strfree(&sb);
    for (enum numparts part = SIGN; part < MAX_NUMPARTS; part++) {
        strfree(&spart[part]);
    }

    return (numstr);
}

/*
 * Determine if exponental decimal form
 */
static const char *isEform(char *numstr, char format_letter)
{
    switch (format_letter) {
    /* Only these formats can possibly produce a decimal E-form
               number */
    case 'e':
    case 'E':
    case 'g':
    case 'G':
        break;
    default:
        return NULL;
    }
    /* Determine if numstr is in scientific E-form. Return is either
         * NULL or the substring where ether exponent-string starts */
    char *epos;

    epos = strchr(numstr, 'e');
    if (epos)
        return epos;
    epos = strchr(numstr, 'E');
    if (epos)
        return epos;

    return NULL;
}

/*
 * Reformat the current printed number with post-decoration if applicable
 */
char *postformat(char *nums)
{
    struct oformat *format = ofrmt;

    char fc = format_letter();
    const char *estr = isEform(nums, fc);

    if (estr && format->scimode == SCI) {
        nums = format_sci(nums, estr);
    }
    if (format->grouping) {
        nums = format_group(nums);
    }
    return nums;
}
