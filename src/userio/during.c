/***************************************************************************
 *   Copyright (C) 2020 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
 * Temporary settings during processing of one "line" (one "evaluation"):
 *    * Settings
 *    * Output formats
 *
 * Arguments:
 *     text       - Command(s), arguments and values embedded in one
 *                  continuous string without any white-spaces. First level
 *                  command is the first letter. What comes after is either
 *                  sub-commands, arguments, lists or ranges in a C-like
 *                  syntax.
 *
 * More than one command can be given. Multiple commands are separated using
 * a special delimiter character (";")
 *
 * Settings are actuated temporarily and restored to last ones after
 * evaluation is done.
 *
 * Note that this feature is for interactive IO. I.e. "during" settings
 * will be part of an AST (i.e. function, sub-expression etc)
 *
 * This file contains the outer logic and temp-handling.
 */

/********************** INCLUDE FILES SECTION ******************************/
#include "main.h"
#include "settings.h"
#include "io.h"
#include <liblog/log.h>
#include <liblog/assure.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <gensub.h>
#include "misc.h"
#include "config.h"

/********************** CONSTANT AND MACRO SECTION *************************/
#define MAX_DIRECTIVES 100 /* Maximum number of concatenated directives */

/********************** TYPE DEFINITION SECTION ****************************/

/********************** LOCAL FUNCTION DECLARATION SECTION *****************/

/**
 * @brief Set trigonometric mode
 *
 * @param text          Trigonometric type "R"=RAD, "D"=DEG
 *
 * @return void
 */
static void trig_mode(const char *text);

/**
 * @brief Snap-shot of current settings
 *
 * @return current settings
 */
static struct settings *settings_backup(void);

/**
 * @brief Restore settings from backup
 *
 * @param settings_r    Last settings reference. If not NULL, t will be freed
 *                      whence settings are restored.
 */
static void settings_restore(struct settings **settings_r);

/**
 * @brief Split multiple "during"-directives into an arg-array
 *
 * @param text          Input with possible multiple directives
 * @param sep           A character used as delimiter
 * @param argc          Out: reference to number of argv's (optional)
 *
 * @return NULL-terminated arg-array of strings
 */
static char **argv_create(const char *text, char sep, int *argc);

/**
 * @brief Destroy argv, free allocated strings and argv itself
 *
 * @param argv          Vector of strings
 * @param argc          Number of strings in array (optional). If given it
 *                      will be used to confirm that the number of strings
 *                      in argv is correct before destroying it.
 *
 * @return NULL on success. Otherwise pass-trough of input argv.
 */
static char **argv_destroy(char **argv, const int *argc);

/**
 * @brief Dispatch argv to corresponding settings parser
 *
 * @param argv          Vector of "during"-directives
 */
static void argv_dispatch(char *const *argv);

/********************** GLOBAL VARIABLE DEFINITION SECTION *****************/

/********************** LOCAL VARIABLE DEFINITION SECTION ******************/

/**
 * @brief Holds settings
 *
 * If not NULL, this pointer holds the latest snap-shot of all various
 * settings
 */
static struct settings *tsettings = NULL;

/**
 * @brief Look-up table for output format
 *
 * Output format to be used per letter when format is NOT given as
 * supplementary command argument.
 *
 * Note: these formats are shortened printf format. The length
 * specifier 'L' is implicit as is the '%' character.
 */
static const struct {
    char letter;
    char *format;
} o_fmt[] = {
    { 'b', DFLT_DURING_b }, { 'd', DFLT_DURING_d }, { 'i', DFLT_DURING_i },
    { 'o', DFLT_DURING_o }, { 'u', DFLT_DURING_u }, { 'x', DFLT_DURING_x },
    { 'X', DFLT_DURING_X }, { 'e', DFLT_DURING_e }, { 'E', DFLT_DURING_E },
    { 'f', DFLT_DURING_f }, { 'F', DFLT_DURING_F }, { 'g', DFLT_DURING_g },
    { 'G', DFLT_DURING_G }, { 'a', DFLT_DURING_a }, { 'A', DFLT_DURING_A },
};

/********************** FUNCTION DEFINITION SECTION ************************/

/*
 * Snap-shot of current settings. Deep-copy in newly-allocated memory
 */
static struct settings *settings_backup(void)
{
    struct settings *settings = malloc(sizeof(struct settings));
    struct oformat *oformat = malloc(sizeof(struct oformat));

    ASSURE(settings);
    ASSURE(oformat);

    /* Shallow copy - i.e. lazy-copy to cover coarse data */
    memcpy(settings, mycalc.settings, sizeof(struct settings));

    /* Deep copy - details not covered by coarse copy */
    memcpy(oformat, ofrmt, sizeof(struct oformat));
    settings->oformat = oformat;
    /* Extra deep */
    settings->oformat->format = strdup(settings->oformat->format);

    return settings;
};

/*
 * Restore settings from backup, free the backup reference argument and set
 * the argument reference to NULL.
 */
static void settings_restore(struct settings **settings_r)
{
    char *true_format;
    ASSURE(settings_r);
    struct settings *settings = *settings_r;
    if (settings->oformat->grouping)
        free(settings->oformat->grouping);
    memcpy(ofrmt, settings->oformat, sizeof(struct oformat));

    /* Restore printf format-string properly */
    true_format = strdup(&ofrmt->format[1]);
    ofrmt->format = NULL;
    set_ofprintf_format(true_format);
    free(true_format);

    mycalc.settings->trig_mode = settings->trig_mode;

    free(settings->oformat);
    free(settings);
    /* Set also the reference */
    *settings_r = NULL;
};

/***************************************************************************
 * Setting-specific (pre-)parsers
 ***************************************************************************/

/* Set printf output-format */
static void output_format(const char *text)
{
    size_t slen = strlen(text);
    char *true_format = NULL;

    if (slen == 1) {
        size_t i;

        /* Look-up default format */
        for (i = 0; (i < elements(o_fmt)) && (o_fmt[i].letter != text[0]); i++)
            ;
        if (i == elements(o_fmt)) {
            LOGE("Aborting: unknown format command-letter: %c\n", text[0]);
            abort();
        };

        /* Compose the final format-string to use */
        true_format = malloc(strlen(o_fmt[i].format) + 4);
        sprintf(true_format, "%sL%c", o_fmt[i].format, text[0]);
        set_ofprintf_format(
            true_format); /* true_format will be freed upon next call to
                                             set_ofprintf_format() */
    } else {
        /* Compose the final format-string to use. Strip parenthesis and
         * quotes if present (FIXME) */
        //true_format = gensub("([?\"?)(.*)(]?\"?)", "\2L", 1, &text[1]);
        true_format = malloc(slen + 2);
        sprintf(true_format, "%sL%c", &text[1], text[0]);
        set_ofprintf_format(
            true_format); /* true_format will be freed upon next call to
                                             set_ofprintf_format() */
    }
}

/* Set trigonometric mode: "R|r"=RAD, "D|d"=DEG
 * If no sub-argument, toggle current mode-setting */
static void trig_mode(const char *text)
{
    if (strlen(text) == 1) {
        if (mycalc.settings->trig_mode == DEG)
            mycalc.settings->trig_mode = RAD;
        else
            mycalc.settings->trig_mode = DEG;
        return;
    }

    switch (text[1]) {
    case 'R':
    case 'r':
        mycalc.settings->trig_mode = RAD;
        break;
    case 'D':
    case 'd':
        mycalc.settings->trig_mode = DEG;
        break;
    default:
        fprintf(stderr, "Unknown trigonometric mode letter: \"%c\"\n", text[1]);
    }
}

/* Set specific mode: "1|S|s"=enable (SCI), "0|N|n"=disable (NRM)
 * If no sub-argument, toggle current mode-setting */
static void sci_mode(const char *text)
{
    if (strlen(text) == 1) {
        if (ofrmt->scimode == SCI)
            ofrmt->scimode = NRM;
        else
            ofrmt->scimode = SCI;
        return;
    }

    switch (text[1]) {
    case '1':
    case 's':
    case 'S':
        ofrmt->scimode = SCI;
        break;
    case '0':
    case 'n':
    case 'N':
        ofrmt->scimode = NRM;
        mycalc.settings->trig_mode = DEG;
        break;
    default:
        fprintf(stderr, "Unknown scientific mode letter: \"%c\"\n", text[1]);
    }
}

/* Manage grouping */
static void grouping_mgr(const char *text)
{
    enum base_id base = printf_base();
    ofrmt->grouping = calloc(1, sizeof(struct grouping));

    if (base < BASE_SENTINEL) {
        memcpy(ofrmt->grouping, &grouping[base], sizeof(struct grouping));
    } else {
        fprintf(stderr, "Unknown base in format-string\n");
        return;
    }

    if (strlen(text) == 1) {
        /* No arguments. Continue using defaults aready set */
        return;
    }

    /* Manage number of digits */
    char *tstr = strdup(&text[1]);
    char *pstr = tstr;
    int i;
    for (i = 0; ('0' <= tstr[i]) && (tstr[i] <= '9'); i++)
        ;
    tstr[i] = 0;
    ofrmt->grouping->digits = atoi(tstr);
    free(pstr);

    /* From here on, no need for any buffer. Use original as constant ptr*/
    pstr = (char *)&text[i + 1];
    if (strlen(pstr) == 0) {
        return;
    }

    /* Manage separator */
    ofrmt->grouping->sep = pstr[0];

    pstr++;
    if (strlen(pstr) == 0) {
        return;
    }

    /* Manage prefix-type */
    switch (pstr[0]) {
    case 'n':
    case 'N':
        ofrmt->grouping->pfx = PFX_NONE;
        break;
    case 'a':
    case 'A':
        ofrmt->grouping->pfx = PFX_ALL;
        break;
    case 'f':
    case 'F':
        ofrmt->grouping->pfx = PFX_FIRST;
        break;
    case 's':
    case 'S':
        ofrmt->grouping->pfx = PFX_SUFFIX;
        break;
    default:
        fprintf(stderr, "Unknown prefix letter: \"%c\"\n", tstr[0]);
    }
}

/***************************************************************************
 * Feature core
 ***************************************************************************/

static char **argv_create(const char *text, char sep, int *argc)
{
    if (!text)
        return NULL;
    size_t olen = strlen(text); /* Original length */
    if (!olen)
        return NULL;
    char **argv = calloc(MAX_DIRECTIVES + 1, sizeof(char *));
    if (!argv)
        return argv;

    size_t sep_strlen(char *s, char sep)
    {
        size_t i;
        for (i = 0; (s[i] != sep) && (s[i] != 0); i++)
            ;
        return i;
    }

    char *args = strdup(text); /* Allocate one buffer for all sub-args */
    char *sptr = args;
    size_t cur_len = 0, i;
    size_t j; /* Current end-index */
    for (i = 0, j = 0; j < olen;
         i++, j = (sptr - args) + cur_len, sptr += (cur_len + 1)) {
        cur_len = sep_strlen(sptr, sep);
        sptr[cur_len] = 0; /* Put an "end-of-sting" marker at sub-arg */
        argv[i] = sptr;
    }
    if (argc)
        *argc = i;
    return argv;
}

static char **argv_destroy(char **argv, const int *argc)
{
    int n = 0;

    if (!argv)
        return argv;

    for (n = 0; argv[n] && n < MAX_DIRECTIVES; n++)
        ; /* Determine  number of elements in argv */

    if (n >= MAX_DIRECTIVES) {
        LOGE("Argv is corrupted. Aborting");
        abort(); /* Crash this */
    }

    if (argc) {
        if (n != *argc) {
            LOGE("Argv is not the expected one. Refusing to destroy");
            return argv;
        }
    }
    free(argv[0]); /* Only the first element is the truly allocated memory */
    free(argv);
    return NULL;
}

/* Iterate through all argv and call corresponding settings-API */
static void argv_dispatch(char *const *argv)
{
    char *string;
    for (size_t i = 0; argv[i] != NULL; i++) {
        string = argv[i];
        switch (string[0]) {
        /* Trigonometric mode parser */
        case 't':
            trig_mode(string);
            break;
        /* Scientific mode parser */
        case 's':
            sci_mode(string);
            break;
        /* Manage grouping request */
        case 'n':
            grouping_mgr(string);
            break;
        /* Printf output-format commands are their own command letters */
        case 'b':
        case 'd':
        case 'i':
        case 'o':
        case 'u':
        case 'x':
        case 'X':
        case 'e':
        case 'E':
        case 'f':
        case 'F':
        case 'g':
        case 'G':
        case 'a':
        case 'A':
            output_format(string);
            break;
        default:
            fprintf(stderr, "Unknown \"during-line\" command-letter: \"%c\"\n",
                    string[0]);
            break;
        }
    }
}

/***************************************************************************
 * Public API
 ***************************************************************************/

void during_line_begin(const char *text)
{
    ASSURE(tsettings == NULL); /* If breaks here, there's a race */
    int argc = -1;
    char **argv = argv_create(&text[1], ';', &argc);

    tsettings = settings_backup(); /* Allocate a backup to be used
                                      during_line_end() */
    argv_dispatch(argv);

    ASSURE(argv_destroy(argv, &argc) == NULL);
}

void during_line_end()
{
    if (tsettings)
        settings_restore(&tsettings);
}
