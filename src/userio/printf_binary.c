#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <printf.h>
#include <string.h>
#include <liblog/log.h>
#include <regstring.h>
#include <gensub.h>
#include "common.h"

static const char *hex2bin(char nibble)
{
    switch (nibble) {
    case '0':
        return "0000";
    case '1':
        return "0001";
    case '2':
        return "0010";
    case '3':
        return "0011";

    case '4':
        return "0100";
    case '5':
        return "0101";
    case '6':
        return "0110";
    case '7':
        return "0111";

    case '8':
        return "1000";
    case '9':
        return "1001";
    case 'a':
        return "1010";
    case 'b':
        return "1011";

    case 'c':
        return "1100";
    case 'd':
        return "1101";
    case 'e':
        return "1110";
    case 'f':
        return "1111";

    default:
        LOGE("%s:%s failure for nibble: %c\n", __FILE__, __func__, nibble);
        return "NaN";
    }
}

int asprintf_binary(char **numstr, const struct printf_info *info,
                    const void *const *args)
{
    char *hbuffer = NULL;
    char *buffer = NULL;
    int len;
    long long unsigned num;

    /* Get argument as number. NOTE: layout is architecture specific */
    if (info->is_short) {
        num = *((short *)(args[0]));
    } else if (info->is_long) {
        num = *((long *)(args[0]));
    } else if (info->is_long_double) {
        num = *((long long *)(args[0]));
    } else {
        num = *((unsigned *)(args[0]));
    }
    /* Format the output into a string. */
    len = asprintf(&hbuffer, "%Lx", num);
    if (len == -1)
        return -1;

    buffer = strdup("");
    for (int i = 0; i < len; i++) {
        strappnd(&buffer, hex2bin(hbuffer[i]));
    }
    STRASGN(hbuffer) = gensub("^0+", "", '1', buffer);

    /* Pad to the minimum field width and print to the stream. */
    len = asprintf(numstr, "%*s", (info->left ? -info->width : info->width),
                   hbuffer);

    /* Clean up and return. */
    strfree(&buffer);
    strfree(&hbuffer);
    return len;
}

int printf_binary(FILE *stream, const struct printf_info *info,
                  const void *const *args)
{
    int len;
    char *numstr;

    len = asprintf_binary(&numstr, info, args);
    len = fprintf(stream, "%s", numstr);
    free(numstr);
    return len;
}

int printf_binary_arginfo_size(const struct printf_info *info, size_t n,
                               int *argtypes, int *size)
{
    UNUSED(size);
    int mask = 0; /* Supplementary information defines how big the number
                     when reaching printf_binary(args[0]) is */

    /* We always take exactly one argument */
    if (n > 0) {
        if (info->is_short)
            mask |= PA_FLAG_SHORT;
        if (info->is_long)
            mask |= PA_FLAG_LONG;
        if (info->is_long_double)
            mask |= PA_FLAG_LONG_LONG;
        argtypes[0] = PA_INT | mask;
    }
    return 1;
}
