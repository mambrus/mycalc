
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <liblog/log.h>
#include <liblog/assure.h>
#include <stdarg.h>
#include "io.h"
#include "common.h"
#include "config.h"
#include "main.h"

#ifdef MYCALC_DEBUG_LEXER
#    define PRINTF(...) LOGI(__VA_ARGS__)
#else
#    define PRINTF(...)
#endif

/*
 Return the first index in <str> of any of the characters in <letters>
 or a negative number on error.
 */
static int idxInStr(const char *letters, const char *str)
{
    int i, j, k, l;
    if ((letters == NULL) || (str == NULL)) {
        return -EINVAL;
    };
    k = strlen(str);
    j = strlen(letters);

    for (i = 0; i < k; i++) {
        for (l = 0; l < j; l++) {
            if (str[i] == letters[l]) {
                return i;
            }
        }
    }
    return -ENODATA;
}

/*
 Convert 10-base <number> string into the fractional part of a Float This
 algo avoids iteration as GNU lib m uses double rounding and leaves an
 erronios fraction.  I.e. it's broken according to POSIX. To minimize this,
 extra rounding on the final result is also applied.
 */
#ifdef NOTUSED
static Float toDecFrac(const char *numstr)
{
    int firstNonZero(const char *s)
    {
        int i;
        for (i = 0; s[i] == '0'; i++)
            ;
        return i;
    }

    const char *number = &numstr[firstNonZero(numstr)];
    int ndigits = strlen(number);

    Float denominator = powl(10, ndigits);
    Float frac = atoi(number) / denominator;

    return frac;
}
#endif

/*
 Convert arbitrary-base <number> into the fractional part of a Float

 It will have rounding error (see toDecFrac)

 */
static Float toBaseFrac(const char *numstr, unsigned base)
{
    Float tfrac, frac = 0.0L;
    int i, j = strlen(numstr);
    char digit[2] = { 0 }; /* Null terminated digit-str, one gigit at a time */

    for (i = 0; i < j; i++) {
        if (numstr[i] != '0') {
            digit[0] = numstr[i];
            tfrac = strtol(digit, NULL, base) / powl(base, i + 1);
            frac += tfrac;
        }
    }

    return frac;
}

Float read_binary_f(const char *str)
{
    int m = idxInStr("B", str);
    int d = idxInStr(".", str);
    char *int_s, *frac_s, *mant_s;
    unsigned int_i, mant_i;
    Float frac;
    Float pow;
    Float rslt;
    int mneg = 0; /* Negative mantissa */

    ASSERT((m >= 0) && (d >= 0));
    char *buf = strdup(str);

    buf[m] = 0; /* Make separator for next part to parse */
    mant_s = &buf[m + 1];
    if (mant_s[0] == '-') {
        mneg = 1;
        mant_s++;
    }
    mant_i = bins2int(mant_s);

    buf[d] = 0; /* Make separator for next part to parse */
    frac_s = &buf[d + 1];
    frac = toBaseFrac(frac_s, 2);

    int_s = &buf[0];
    int_i = bins2int(int_s);

    pow = powl(2, mant_i);

    free(buf);

    rslt = (Float)int_i + frac;
    if (mneg) {
        rslt = rslt / pow;
    } else {
        rslt = rslt * pow;
    }

    return rslt;
}

Float read_binary_i(const char *str)
{
    int m = idxInStr("B", str);
    char *int_s, *mant_s;
    unsigned int_i, mant_i;
    Float pow;
    Float rslt;
    int mneg = 0; /* Negative mantissa */

    ASSERT(m >= 0);
    char *buf = strdup(str);

    buf[m] = 0; /* Make separator for next part to parse */
    mant_s = &buf[m + 1];
    if (mant_s[0] == '-') {
        mneg = 1;
        mant_s++;
    }
    mant_i = bins2int(mant_s);

    int_s = &buf[0];
    int_i = bins2int(int_s);

    pow = powl(2, mant_i);

    free(buf);

    rslt = (Float)int_i;
    if (mneg) {
        rslt = rslt / pow;
    } else {
        rslt = rslt * pow;
    }

    return rslt;
}

Float read_octal_f(const char *str)
{
    int m = idxInStr("Oo", str);
    int d = idxInStr(".", str);
    char *int_s, *frac_s, *mant_s;
    unsigned int_i, mant_i;
    Float frac;
    Float pow;
    Float rslt;
    int mneg = 0; /* Negative mantissa */

    ASSERT((m >= 0) && (d >= 0));
    char *buf = strdup(str);

    buf[m] = 0; /* Make separator for next part to parse */
    mant_s = &buf[m + 1];
    if (mant_s[0] == '-') {
        mneg = 1;
        mant_s++;
    }
    sscanf(mant_s, "%o", &mant_i);

    buf[d] = 0; /* Make separator for next part to parse */
    frac_s = &buf[d + 1];
    frac = toBaseFrac(frac_s, 8);

    int_s = &buf[0];
    sscanf(int_s, "%o", &int_i);

    pow = powl(8, mant_i);

    free(buf);

    rslt = (Float)int_i + frac;
    if (mneg) {
        rslt = rslt / pow;
    } else {
        rslt = rslt * pow;
    }

    return rslt;
}

Float read_octal_i(const char *str)
{
    int m = idxInStr("Oo", str);
    char *int_s, *mant_s;
    unsigned int_i, mant_i;
    Float pow;
    Float rslt;
    int mneg = 0; /* Negative mantissa */

    ASSERT(m >= 0);
    char *buf = strdup(str);

    buf[m] = 0; /* Make separator for next part to parse */
    mant_s = &buf[m + 1];
    if (mant_s[0] == '-') {
        mneg = 1;
        mant_s++;
    }
    sscanf(mant_s, "%o", &mant_i);

    int_s = &buf[0];
    sscanf(int_s, "%o", &int_i);

    pow = powl(8, mant_i);

    free(buf);

    rslt = (Float)int_i;
    if (mneg) {
        rslt = rslt / pow;
    } else {
        rslt = rslt * pow;
    }

    return rslt;
}

Float read_hex_f(const char *str)
{
    int m = idxInStr("Hh", str);
    int d = idxInStr(".", str);
    char *int_s, *frac_s, *mant_s;
    unsigned int_i, mant_i;
    Float frac;
    Float pow;
    Float rslt;
    int mneg = 0; /* Negative mantissa */

    ASSERT((m >= 0) && (d >= 0));
    char *buf = strdup(str);

    buf[m] = 0; /* Make separator for next part to parse */
    mant_s = &buf[m + 1];
    if (mant_s[0] == '-') {
        mneg = 1;
        mant_s++;
    }
    sscanf(mant_s, "%x", &mant_i);

    buf[d] = 0; /* Make separator for next part to parse */
    frac_s = &buf[d + 1];
    frac = toBaseFrac(frac_s, 16);

    int_s = &buf[0];
    sscanf(int_s, "%x", &int_i);

    pow = powl(16, mant_i);

    free(buf);

    rslt = (Float)int_i + frac;
    if (mneg) {
        rslt = rslt / pow;
    } else {
        rslt = rslt * pow;
    }

    return rslt;
}

Float read_hex_i(const char *str)
{
    int m = idxInStr("Hh", str);
    char *int_s, *mant_s;
    unsigned int_i, mant_i;
    Float pow;
    Float rslt;
    int mneg = 0; /* Negative mantissa */

    ASSERT(m >= 0);
    char *buf = strdup(str);

    buf[m] = 0; /* Make separator for next part to parse */
    mant_s = &buf[m + 1];
    if (mant_s[0] == '-') {
        mneg = 1;
        mant_s++;
    }
    sscanf(mant_s, "%x", &mant_i);

    int_s = &buf[0];
    sscanf(int_s, "%x", &int_i);

    pow = powl(16, mant_i);

    free(buf);

    rslt = (Float)int_i;
    if (mneg) {
        rslt = rslt / pow;
    } else {
        rslt = rslt * pow;
    }

    return rslt;
}

/* Scanf doesnt have a binary format specifier. Read to string and convert */
Int bins2int(char *str)
{
    Int convert(char *string)
    {
        char *sval = &string[0];
        int slen = strlen(sval);
        Int digit;
        Int m = 1;
        Int res = 0;

        PRINTF("%s\n", sval);
        for (int i = 0; i < slen; i++) {
            digit = (sval[(slen - 1) - i]) - '0';
            PRINTF("%2d: %16Lx %16Lx", i + 1, digit, m);
            res += (digit * m);
            m <<= 1;
            PRINTF("%16Lx\n", res);
        }
        return res;
    };

    return convert(str);
}
