#define _GNU_SOURCE
#if __GNUC__ < 10
#    undef _FORTIFY_SOURCE
#    define _FORTIFY_SOURCE 0
#endif
#include <regstring.h>
#include <gensub.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <limits.h>
#include <printf.h>
#include <liblog/log.h>
#include <liblog/assure.h>
#include "misc.h"
#include "main.h"
#include "io.h"
#include "postformat.h"
#include "settings.h"

static char *printf_number(Float number);
static char *printf_integer(unsigned long long number, bool neg);

/* Cast to unsigned integer and printf according to extended formatting
 * info. If neg is true, a sign will be pre-pended. */
char *printf_integer(unsigned long long number, bool neg)
{
    char *nums = NULL;
    char *pfx = NULL;
    char *sgn = NULL;
    char *tmp;
    char *format = get_ofprintf_format();
    char fc;
    size_t ss = 0, sp;

    if (neg) {
        sgn = strdup("-");
        number = -1 * number;
        ss = 1;
    } else {
        sgn = strdup("");
    }

    fc = format_letter();
    if (!ofrmt->grouping) {
        /* If grouping post-formatter, prefix etc will be handled there */
        sp = 2;
        switch (fc) {
        case 'b':
            pfx = strdup("0b");
            break;
        case 'o':
            pfx = strdup("0c");
            break;
        case 'x':
            pfx = strdup("0x");
            break;
        case 'X':
            pfx = strdup("0X");
            break;
        case 'u':
        default:;
            pfx = strdup("");
            sp = 0;
            break;
        }
    } else {
        pfx = strdup("");
        sp = 0;
    }

#ifdef MYCALC_PRINTF_REGISTER
    asprintf(&nums, format, number);
#else
    if (fc == 'b') {
        struct printf_info info = {
            .is_long_double = 1,
        };
        const void *const args[] = {
            [0] = &number,
        };
        asprintf_binary(&nums, &info, (const void *const *)args);
    } else {
        asprintf(&nums, format, number);
    }
#endif
    if (ofrmt->grouping) {
        /* Cleanu-up any leading zeros. Grouping will do it.*/
        tmp = nums;
        nums = gensub("^0+", "", '1', tmp);
    }

    strnprepend(pfx, &nums, sp);
    strnprepend(sgn, &nums, ss);
    strfree(&pfx);
    strfree(&sgn);
    return nums;
}

char *printf_number(Float number)
{
    char *nums = NULL;
    char *format = get_ofprintf_format();
    char fc = format_letter();
    switch (fc) {
        /* Integer prints */
    case 'd': /* decimal - always signed & never prefixed */
    case 'i':
        if (fabsl(number) < LLONG_MAX) {
            asprintf(&nums, format, (Int)number);
        } else {
            fprintf(stderr, "Number to big to print for integer formatter\n");
            nums = strdup("NaN");
        }
        break;
    case 'b': /* Bin */
    case 'o': /* Octal */
    case 'u': /* Decimal */
    case 'x': /* Hex */
    case 'X': /* Hex */
        if (fabsl(number) < ULLONG_MAX) {
            if (number >= 0)
                nums = printf_integer(number, false);
            else
                nums = printf_integer(fabsl(number), true);
        } else {
            fprintf(stderr, "Number to big to print for integer formatter\n");
            nums = strdup("NaN");
        }
        break;

        /* Floating-point prints */
    case 'e':
    case 'E':
    case 'f':
    case 'F':
    case 'g':
    case 'G':
    case 'a':
    case 'A':
        asprintf(&nums, format, number);
        break;

    default:
        LOGE("Can't printf_number: Invalid format detected: %s", format);
    }
    return nums;
}

/*
 * Output number formatter (end-point of evacuating an AST or result)
 */
void printnumber(Float number)
{
    char *buf;
    buf = printf_number(number);
    buf = postformat(buf);
    fprintf(ofrmt->file, "%s", buf);
    free(buf);
}
