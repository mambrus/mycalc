set(MYCALC_PRINTF_FORMAT
    "Lg"
    CACHE STRING
    "Defaut printf output numerical format representation")

set(MYCALC_PRINTF_SCI_MODE
    "NRM"
    CACHE STRING
    "Defaut SCI or NRM printf post-formatter (NRM means none)")
set_property(CACHE MYCALC_PRINTF_SCI_MODE PROPERTY STRINGS
    "SCI" "NRM")

option (MYCALC_PRINTF_REGISTER
    "Register printf-functions (defunct)" OFF)

set(MYCALC_DURING_b
	""
	CACHE STRING
	"Default output format for integer printf temp-setting: b")
set(MYCALC_DURING_d
	""
	CACHE STRING
	"Default output format for integer printf temp-setting: d")
set(MYCALC_DURING_i
	""
	CACHE STRING
	"Default output format for integer printf temp-setting: i")
set(MYCALC_DURING_o
	""
	CACHE STRING
	"Default output format for integer printf temp-setting: o")
set(MYCALC_DURING_u
	""
	CACHE STRING
	"Default output format for integer printf temp-setting: u")
set(MYCALC_DURING_x
	"04"
	CACHE STRING
	"Default output format for integer printf temp-setting: x")
set(MYCALC_DURING_X
	"04"
	CACHE STRING
	"Default output format for integer printf temp-setting: X")
set(MYCALC_DURING_e
	"2.2"
	CACHE STRING
	"Default output format for float scientific printf temp-setting: e")
set(MYCALC_DURING_E
	"2.2"
	CACHE STRING
	"Default output format for float scientific printf temp-setting: E")
set(MYCALC_DURING_f
	"2.2"
	CACHE STRING
	"Default output format for float printf temp-setting: f")
set(MYCALC_DURING_F
	"2.2"
	CACHE STRING
	"Default output format for float printf temp-setting: F")
set(MYCALC_DURING_g
	"2.2"
	CACHE STRING
	"Default output format for float e-form printf temp-setting: g")
set(MYCALC_DURING_G
	"2.2"
	CACHE STRING
    "Default output format for float E-form printf temp-setting: G")
set(MYCALC_DURING_a
	"2.2"
	CACHE STRING
	"Default output format for scientific hexadecimal printf temp-setting: a")
set(MYCALC_DURING_A
	"2.2"
	CACHE STRING
	"Default output format for scientific hexadecimal printf temp-setting: A")
#
# Output post-formatter grouping
#
set(MYCALC_PRX_TYPES
    "FIRST"
    "ALL"
    "NONE"
    "SUFFIX"
)
# ---- HEX ----
set(MYCALC_GROUP_HEX_DIGITS
	"4"
	CACHE STRING
    "Default number of digits grouping HEX")
set(MYCALC_GROUP_HEX_SEP
	" "
	CACHE STRING
    "Default separator grouping HEX")
set(MYCALC_GROUP_HEX_PFX
    "FIRST"
	CACHE STRING
    "Default prefix (FIRST|ALL|NONE|SUFFIX)")
set_property(CACHE MYCALC_GROUP_HEX_PFX PROPERTY STRINGS
    ${MYCALC_PRX_TYPES})

option(MYCALC_GROUP_HEX_ZPAD
    "Pad outermost group(s) with zeros if needed" ON)
# ---- DEC ----
set(MYCALC_GROUP_DEC_DIGITS
	"3"
	CACHE STRING
    "Default number of digits grouping DEC")
set(MYCALC_GROUP_DEC_SEP
	"-"
	CACHE STRING
    "Default separator grouping DEC")
set(MYCALC_GROUP_DEC_PFX
    "NONE"
	CACHE STRING
    "Default prefix (FIRST|ALL|NONE|SUFFIX)")
set_property(CACHE MYCALC_GROUP_DEC_PFX PROPERTY STRINGS
    ${MYCALC_PRX_TYPES})

option(MYCALC_GROUP_DEC_ZPAD
    "Pad outermost group(s) with zeros if needed" OFF)
# ---- OCT ----
set(MYCALC_GROUP_OCT_DIGITS
	"3"
	CACHE STRING
    "Default number of digits grouping OCT")
set(MYCALC_GROUP_OCT_SEP
	" "
	CACHE STRING
    "Default separator grouping OCT")
set(MYCALC_GROUP_OCT_PFX
    "ALL"
	CACHE STRING
    "Default prefix (FIRST|ALL|NONE|SUFFIX)")
set_property(CACHE MYCALC_GROUP_OCT_PFX PROPERTY STRINGS
    ${MYCALC_PRX_TYPES})

option(MYCALC_GROUP_OCT_ZPAD
    "Pad outermost group(s) with zeros if needed" ON)
# ---- BIN ----
set(MYCALC_GROUP_BIN_DIGITS
	"8"
	CACHE STRING
    "Default number of digits grouping BIN")
set(MYCALC_GROUP_BIN_SEP
	" "
	CACHE STRING
    "Default separator grouping BIN")
set(MYCALC_GROUP_BIN_PFX
    "SUFFIX"
	CACHE STRING
    "Default prefix (FIRST|ALL|NONE|SUFFIX)")
set_property(CACHE MYCALC_GROUP_BIN_PFX PROPERTY STRINGS
    ${MYCALC_PRX_TYPES})

option(MYCALC_GROUP_BIN_ZPAD
    "Pad outermost group(s) with zeros if needed" ON)
