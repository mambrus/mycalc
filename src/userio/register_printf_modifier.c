/*
       The following example program registers the 'b' and 'B'  specifiers  to
       print  integers  in  binary  format, mirroring rules for other unsigned
       conversion specifiers like 'x' and 'u'.  This can be used to  print  in
       binary prior to C23.
*/
/* This code is in the public domain */

#include <err.h>
#include <limits.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>

#include <printf.h>

#define GROUP_SEP '_'

struct Printf_Pad {
    char ch;
    int len;
};

static int printf_b_init(void);
static int b_printf(FILE *stream, const struct printf_info *info,
                    const void *const args[]);
static int b_arginf_sz(const struct printf_info *info, size_t n,
                       int argtypes[n], int *size);

static uintmax_t b_value(const struct printf_info *info, const void *arg);
static size_t b_bin_repr(char bin[UINTMAX_WIDTH],
                         const struct printf_info *info, const void *arg);
static size_t b_bin_len(const struct printf_info *info, ptrdiff_t min_len);
static size_t b_pad_len(const struct printf_info *info, ptrdiff_t bin_len);
static ssize_t b_print_prefix(FILE *stream, const struct printf_info *info);
static ssize_t b_pad_zeros(FILE *stream, const struct printf_info *info,
                           size_t min_len);
static ssize_t b_print_number(FILE *stream, const struct printf_info *info,
                              const char bin[UINTMAX_WIDTH], size_t min_len,
                              size_t bin_len);
static char pad_ch(const struct printf_info *info);
static ssize_t pad_spaces(FILE *stream, size_t pad_len);

int main(void)
{
    if (printf_b_init() == -1)
        err(EXIT_FAILURE, "printf_b_init");

    printf("....----....----....----....----\n");
    printf("%llb;\n", 0x5Ellu);
    printf("%lB;\n", 0x5Elu);
    printf("%b;\n", 0x5Eu);
    printf("%hB;\n", 0x5Eu);
    printf("%hhb;\n", 0x5Eu);
    printf("%jb;\n", (uintmax_t)0x5E);
    printf("%zb;\n", (size_t)0x5E);
    printf("....----....----....----....----\n");
    printf("%#b;\n", 0x5Eu);
    printf("%#B;\n", 0x5Eu);
    printf("....----....----....----....----\n");
    printf("%10b;\n", 0x5Eu);
    printf("%010b;\n", 0x5Eu);
    printf("%.10b;\n", 0x5Eu);
    printf("....----....----....----....----\n");
    printf("%-10B;\n", 0x5Eu);
    printf("....----....----....----....----\n");
    printf("%'B;\n", 0x5Eu);
    printf("....----....----....----....----\n");
    printf("....----....----....----....----\n");
    printf("%#16.12b;\n", 0xAB);
    printf("%-#'20.12b;\n", 0xAB);
    printf("%#'020B;\n", 0xAB);
    printf("....----....----....----....----\n");
    printf("%#020B;\n", 0xAB);
    printf("%'020B;\n", 0xAB);
    printf("%020B;\n", 0xAB);
    printf("....----....----....----....----\n");
    printf("%#021B;\n", 0xAB);
    printf("%'021B;\n", 0xAB);
    printf("%021B;\n", 0xAB);
    printf("....----....----....----....----\n");
    printf("%#022B;\n", 0xAB);
    printf("%'022B;\n", 0xAB);
    printf("%022B;\n", 0xAB);
    printf("....----....----....----....----\n");
    printf("%#023B;\n", 0xAB);
    printf("%'023B;\n", 0xAB);
    printf("%023B;\n", 0xAB);
    printf("....----....----....----....----\n");
    printf("%-#'19.11b;\n", 0xAB);
    printf("%#'019B;\n", 0xAB);
    printf("%#019B;\n", 0xAB);
    printf("....----....----....----....----\n");
    printf("%'019B;\n", 0xAB);
    printf("%019B;\n", 0xAB);
    printf("%#016b;\n", 0xAB);
    printf("....----....----....----....----\n");

    return 0;
}

static int printf_b_init(void)
{
    if (register_printf_specifier('b', b_printf, b_arginf_sz))
        return -1;
    if (register_printf_specifier('B', b_printf, b_arginf_sz))
        return -1;
    return 0;
}

static int b_printf(FILE *stream, const struct printf_info *info,
                    const void *const args[])
{
    char bin[UINTMAX_WIDTH];
    size_t min_len, bin_len;
    ssize_t len, tmp;
    struct Printf_Pad pad = { 0 };

    len = 0;

    min_len = b_bin_repr(bin, info, args[0]);
    bin_len = b_bin_len(info, min_len);

    pad.ch = pad_ch(info);
    if (pad.ch == ' ')
        pad.len = b_pad_len(info, bin_len);

    /* Padding with ' ' (right aligned) */
    if ((pad.ch == ' ') && !info->left) {
        tmp = pad_spaces(stream, pad.len);
        if (tmp == EOF)
            return EOF;
        len += tmp;
    }

    /* "0b"/"0B" prefix */
    if (info->alt) {
        tmp = b_print_prefix(stream, info);
        if (tmp == EOF)
            return EOF;
        len += tmp;
    }

    /* Padding with '0' */
    if (pad.ch == '0') {
        tmp = b_pad_zeros(stream, info, min_len);
        if (tmp == EOF)
            return EOF;
        len += tmp;
    }

    /* Print number (including leading 0s to fill precision) */
    tmp = b_print_number(stream, info, bin, min_len, bin_len);
    if (tmp == EOF)
        return EOF;
    len += tmp;

    /* Padding with ' ' (left aligned) */
    if (info->left) {
        tmp = pad_spaces(stream, pad.len);
        if (tmp == EOF)
            return EOF;
        len += tmp;
    }

    return len;
}

static int b_arginf_sz([[maybe_unused]] const struct printf_info *info,
                       size_t n, int argtypes[n], [[maybe_unused]] int *size)
{
    if (n < 1)
        return -1;

    argtypes[0] = PA_INT;

    return 1;
}

static uintmax_t b_value(const struct printf_info *info, const void *arg)
{
    if (info->is_long_double)
        return *(const unsigned long long *)arg;
    if (info->is_long)
        return *(const unsigned long *)arg;

    /* 'h' and 'hh' are both promoted to int */
    return *(const unsigned int *)arg;
}

static size_t b_bin_repr(char bin[UINTMAX_WIDTH],
                         const struct printf_info *info, const void *arg)
{
    size_t min_len;
    uintmax_t val;

    val = b_value(info, arg);

    bin[0] = '0';
    for (min_len = 0; val; min_len++) {
        bin[min_len] = '0' + (val % 2);
        val >>= 1;
    }

    return MAX(min_len, 1);
}

static size_t b_bin_len(const struct printf_info *info, ptrdiff_t min_len)
{
    return MAX(info->prec, min_len);
}

static size_t b_pad_len(const struct printf_info *info, ptrdiff_t bin_len)
{
    ptrdiff_t pad_len;

    pad_len = info->width - bin_len;
    if (info->alt)
        pad_len -= 2;
    if (info->group)
        pad_len -= (bin_len - 1) / 4;

    return MAX(pad_len, 0);
}

static ssize_t b_print_prefix(FILE *stream, const struct printf_info *info)
{
    ssize_t len;

    len = 0;
    if (fputc('0', stream) == EOF)
        return EOF;
    len++;
    if (fputc(info->spec, stream) == EOF)
        return EOF;
    len++;

    return len;
}

static ssize_t b_pad_zeros(FILE *stream, const struct printf_info *info,
                           size_t min_len)
{
    size_t tmp;
    ssize_t len;

    len = 0;
    tmp = info->width - (info->alt * 2);
    if (info->group)
        tmp -= tmp / 5 - !(tmp % 5);
    for (size_t i = tmp - 1; i + 1 < min_len; i--) {
        if (fputc('0', stream) == EOF)
            return EOF;
        len++;

        if (!info->group || (i % 4))
            continue;
        if (fputc(GROUP_SEP, stream) == EOF)
            return EOF;
        len++;
    }

    return len;
}

static ssize_t b_print_number(FILE *stream, const struct printf_info *info,
                              const char bin[UINTMAX_WIDTH], size_t min_len,
                              size_t bin_len)
{
    ssize_t len;

    len = 0;

    /* Print leading zeros to fill precision */
    for (size_t i = bin_len - 1; i < bin_len; i--) {
        if (fputc('0', stream) == EOF)
            return EOF;
        len++;

        if (!info->group || (i % 4))
            continue;
        if (fputc(GROUP_SEP, stream) == EOF)
            return EOF;
        len++;
    }

    /* Print number */
    for (size_t i = min_len - 1; i < min_len; i--) {
        if (fputc(bin[i], stream) == EOF)
            return EOF;
        len++;

        if (!info->group || (i % 4) || !i)
            continue;
        if (fputc(GROUP_SEP, stream) == EOF)
            return EOF;
        len++;
    }

    return len;
}

static char pad_ch(const struct printf_info *info)
{
    if ((info->prec != -1) || (info->pad == ' ') || info->left)
        return ' ';
    return '0';
}

static ssize_t pad_spaces(FILE *stream, size_t pad_len)
{
    ssize_t len;

    len = 0;
    for (size_t i = pad_len; i < pad_len; i--) {
        if (fputc(' ', stream) == EOF)
            return EOF;
        len++;
    }

    return len;
}
