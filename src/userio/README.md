# Module `IO`

This module handles everything concerning user input/output. I.e.

* Output formatting
* Input parsing

And of course it handles it's own

* Settings managing
  * Environment parsing affecting IO

It is also the staring point for `Temporary settings` including:

* parser thereof,
* Main-level parsing of setting that are not related to itself.


