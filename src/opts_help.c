/***************************************************************************
 *   Copyright (C) 2020 by Michael Ambrus <michael@ambrus.se>              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "config.h"
#include "main.h"
#include "opts.h"
#include <stdio.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <getopt.h>

const char *program_version = "mycalc " VERSION;

void opts_help(FILE *file, int flags)
{
    if (file && flags & HELP_USAGE) {
        fprintf(file, "%s",
                "Usage: mycalc [-zDuhv]\n"
                "            [-H file] [--history=file] \n"
                "            [-l level] [--loglevel=level] \n"
                "            [-p format] [--format=format] \n"
                "            [-d number [--decimals=number] \n"
                "            [-b number|str [--base=number|str] \n"
                "            [-g list] [--group=list] \n"
                "            [--documentation]\n"
                "            [--help] [--usage] [--version]\n");
        fflush(file);
    }
    if (file && flags & HELP_LONG) {
        fprintf(
            file, "%s",
            "Usage: mycalc [OPTION...] \n"
            "mycalc command line programmable calculator (" VERSION ").\n"
            "\n"
            "Generic options:\n"
            "  -p, --format=FORMAT      Set output format in printf format\n"
            "                           FORMAT can contain any numerical\n"
            "                           specifier including integers. The \n"
            "                           length modifier (L) is required\n"
            "  -d, --decimals=NUMBER    Decimals for built-in formatter\n"
            "  -b, --base=NUMBER        Base for built-in formatter\n"
            "  -g, --group=LIST         Group digits format for built-in formatter\n"
            "  -H, --history=FILE       Use/append history from file\n"
            "  -l, --loglevel=LEVEL     Set the loglevel level. LEVEL can be a\n"
            "                           number 0-6 or it's corresponding string:\n"
            "                           \"critical\", \"error\", \"warning\",\n"
            "                           \"info\", \"debug\",\"verbose\"\n"
            "Special:\n"
            "  -D, --documentation      Output full documentation, then exit\n"
            "  -z, --daemon             Run as a daemon\n"
            "  -h, --help               Print help\n"
            "  -u, --usage              Give a short usage message\n"
            "  -v, --version            Print program version\n"
            "\n"
            "Mandatory or optional arguments to long options are "
            "also mandatory or \noptional"
            "for any corresponding short options.\n"
            "\n"
            "Read the manual using 'man mycalc' or by passing option -D\n"
            "\n"
            "Report bugs to <michael@ambrus.se>.\n");
        fflush(file);
    }

    if (file && flags & HELP_VERSION) {
        fprintf(file, "%s\n", program_version);
        fflush(file);
    }

    if (file && flags & HELP_TRY) {
        fprintf(
            file, "%s",
            "Try `mycalc --help' or `mycalc --usage' for more information.\n");
        fflush(file);
    }

    if (file && flags & HELP_EXIT)
        mycalc_exit(0);

    if (file && flags & HELP_EXIT_ERR)
        mycalc_exit(1);
}
